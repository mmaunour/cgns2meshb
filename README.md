A tool to convert CGNS files to Gamma Mesh Format developped by GAMMA Project Team at Inria

- [Motivation](#motivation)
- [Installation](#installation)
  * [Prerequisite: install CGNS and HDF5](#prerequisite-install-cgns-and-hdf5)
    + [Install HDF5](#install-hdf5)
    + [Install CGNS with HDF5](#install-cgns-with-hdf5)
  * [Install cgns2meshb](#install-cgns2meshb)
- [How to use](#how-to-use)
- [Contact](#contact)

# Motivation

The goal of this tool is to convert CGNS files to Gamma Mesh Format thanks to the [libMeshb](https://github.com/LoicMarechal/libMeshb). \
Meshes and Solutions in the libMeshb format can be visualized with [ViZiR 4](http://vizir.inria.fr/).

Note that ViZiR 4 is able to read cgns file. However, it will be more efficient in meshb/solb format (in terms of I/O and all operations).

# Installation

CGNS with HDF5 are needed as library

## Prerequisite: install CGNS and HDF5 

### Install HDF5

- `git clone https://github.com/HDFGroup/hdf5.git`
- `cd hdf5/`
- `mkdir build`
- `cd build`
- `cmake ..`
- `make`
- `sudo make install`

### Install CGNS with HDF5

- `git clone https://github.com/CGNS/CGNS.git`
- `cd CGNS/`
- `mkdir build`
- `cd build`
- `cmake ..`
- `make`
- `sudo make install`

You have to check that CGNS_ENABLE_HDF5 is set to ON

## Install cgns2meshb
Simply follow these steps:
- Clone the repo
- Go to cgns2meshb
- `mkdir build`
- `cd build`
- `cmake ..`
- `make`
- `sudo make install`

If CMake cannot find libMeshb library (with find_package), you may need to add the path where it has been set, for instance:
-  `cmake .. -DCMAKE_PREFIX_PATH=~/cmakebuilds/`

# How to use
The format is: cgns2meshb xxx.cgns yyy.cgns zzz.cgns (-ascii) 
- several cgns files can be provided
- -ascii is optional to define ascii output (binary by default otherwise)

# Contact 
[Matthieu Maunoury](https://pages.saclay.inria.fr/matthieu.maunoury/) (matthieu.maunoury@inria.fr)
