#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
using namespace std;

#include <string>

extern "C" {
#include "msh.h"
}

static inline int isParameter(char* str)
{
  if (str == NULL)
    return 0;
  if (str[0] == '-')
    return 0;
  return 1;
}

int main(int argc, char** argv)
{

  printf(" \n");
  printf(" ----------------------------------------------------------------------- \n");
  printf("|                                                                       |\n");
  printf("|                             cgns2meshb                                |\n");
  printf("|                                                                       |\n");
  printf(" -----------------------------------------------------------------------\n");
  printf(" \n");

  int          nbrVer, ierro;
  char         InpMshNam[1024], noMshNam[1024], noSolNam[1024], OutMshNam[1024], OutSolNam[1024];
  bool         solexist, ascii = false;
  VizObject*   Obj_surf = NULL;
  VizIntStatus status;

  double t0 = GetWallClock();

  if (argc < 2) {
    cout << "  ERROR: missing arguments" << endl;
    goto errormessage;

  errormessage: //-- error message
    printf("\n");
    cout << "  The format is: cgns2meshb xxx.cgns yyy.cgns zzz.cgns (-ascii)" << endl;
    cout << "        several cgns files can be provided" << endl;
    cout << "        -ascii is optional to define ascii output (binary by default otherwise)" << endl;
    return 0;
  }

  //-- read parameters
  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-ascii") == 0)
      ascii = true;
  }

  for (int i = 1; i < argc; i++) {
    solexist = false;
    if ((isParameter(argv[i]) == 1)) {
      sprintf(InpMshNam, "%s", argv[i]);
      sprintf(noMshNam, "%s", InpMshNam);
      if (strstr(InpMshNam, ".cgns") != NULL) {
        //-- cgns file found
        printf("Case of cgns file: %s\n", argv[i]);
        noMshNam[strstr(InpMshNam, ".cgns") - InpMshNam] = '\0'; // check if it has the extension .cgns[b] and remove it if it is the case

        sprintf(OutMshNam, "%s", noMshNam);
        sprintf(OutSolNam, "%s", noMshNam);

        if (ascii)
          strcat(OutMshNam, ".mesh");
        else
          strcat(OutMshNam, ".meshb");
        if (ascii)
          strcat(OutSolNam, ".sol");
        else
          strcat(OutSolNam, ".solb");

        ierro = viz_NewInterface(&Obj_surf);
        if (ierro != 0)
          printf(" viz_NewInterface  status %d \n", ierro);

        ierro = viz_OpenMesh(Obj_surf, InpMshNam, &status);
        if (ierro != VIZINT_SUCCESS) {
          printf(" viz_OpenMesh failed error %3d : %s\n", ierro, status.ErrMsg);
          continue;
        }
        else
          printf("  %%%% %s OPENED\n", status.InfoMsg);

        viz_PrintMeshInfo(Obj_surf, &status);

        ierro = viz_OpenSolution(Obj_surf, InpMshNam, &status);
        if (ierro != VIZINT_SUCCESS)
          printf(" viz_OpenSolution failed error %3d : %s\n", ierro, status.ErrMsg);
        else {
          printf("  %%%% %s OPENED\n", status.InfoMsg);
          solexist = true;
        }
        viz_PrintSolutionInfo(Obj_surf, &status);

        if (viz_GetNumberOfVertices(Obj_surf, &nbrVer, &status) == VIZINT_SUCCESS) {
          if (nbrVer <= 0)
            continue;
        }

        ierro = viz_WriteMesh(Obj_surf, OutMshNam, &status);
        if (ierro != VIZINT_SUCCESS)
          printf(" viz_WriteMesh failed error %3d : %s\n", ierro, status.ErrMsg);
        else
          printf("  %%%% Mesh written in %s\n", status.InfoMsg);
        if (solexist) {
          sprintf(OutSolNam, "%s", OutMshNam);
          if (strstr(OutMshNam, ".mesh") != NULL)
            OutSolNam[strstr(OutMshNam, ".mesh") - OutMshNam] = '\0'; // check if it has the extension .mesh[b] and remove it if it is the case``

          //-- add format solb if no format sol or solb is given
          if (strstr(OutSolNam, ".sol") == NULL) {
            strcat(OutSolNam, ".solb");
          }

          ierro = viz_WriteSolution(Obj_surf, OutSolNam, -1, &status);

          if (ierro != VIZINT_SUCCESS)
            printf(" viz_WriteSolution failed error %3d : %s\n", ierro, status.ErrMsg);
          else
            printf("  %%%% Solution written in %s\n", status.InfoMsg);
        }
      }
      printf("\n");
    }
  }

  double t2 = GetWallClock();
  printf(" Total cpu time %lg (s) \n", t2 - t0);

  cout << "\n  Thank you for using cgns2meshb" << endl;

  return 0;
}
