#ifdef WIN32
#else
#include <sys/time.h>
#endif

#include <datastruct.h>
#include <msh.h>

// To Read and Write  .mesh and .meshb file we use the
// library LibMeshb developed by Loic Maréchal - Gamma team INRIA
#include "libmeshb7.h"

static int typeOf[]    = { 0, 0, 0, 0, GmfInt, 0, 0, 0, GmfLong };
static int typeOfTab[] = { 0, 0, 0, 0, GmfIntTab, 0, 0, 0, GmfLongTab };

#ifndef min
#define min(a, b) ((a) < (b) ? (a) : (b))
#endif
#ifndef max
#define max(a, b) ((a) > (b) ? (a) : (b))
#endif

static void viz_writestatus(VizIntStatus* status, char* msg, ...)
{
  va_list arg;
  if (status == NULL)
    return;
  va_start(arg, msg);
  vsnprintf(status->ErrMsg, 512, msg, arg);
  va_end(arg);
}

static void viz_writeinfo(VizIntStatus* status, char* msg, ...)
{
  va_list arg;
  if (status == NULL)
    return;
  va_start(arg, msg);
  vsnprintf(status->InfoMsg, 512, msg, arg);
  va_end(arg);
}

//-- error codes libmeshb
void readMshErr(int end)
{
  fprintf(stderr, "\n  ## ERROR WHILE READING MESH DATA. PLEASE CHECK YOUR DATA\n");
  printf("  Thank you for using VIZIR 4\n");
  exit(1);
}

void readSolErr(int end)
{
  fprintf(stderr, "\n  ## ERROR WHILE READING SOLUTION DATA. PLEASE CHECK YOUR DATA\n");
  printf("  Thank you for using VIZIR 4\n");
  exit(1);
}

static int viz_IsSolKwd(int SolAtEnt)
{
  if (SolAtEnt == GmfSolAtVertices || SolAtEnt == GmfSolAtEdges || SolAtEnt == GmfSolAtTriangles || SolAtEnt == GmfSolAtQuadrilaterals || SolAtEnt == GmfSolAtTetrahedra || SolAtEnt == GmfSolAtPyramids || SolAtEnt == GmfSolAtPrisms || SolAtEnt == GmfSolAtHexahedra)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP1 || SolAtEnt == GmfHOSolAtTrianglesP1 || SolAtEnt == GmfHOSolAtQuadrilateralsQ1 || SolAtEnt == GmfHOSolAtTetrahedraP1 || SolAtEnt == GmfHOSolAtPyramidsP1 || SolAtEnt == GmfHOSolAtPrismsP1 || SolAtEnt == GmfHOSolAtHexahedraQ1)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP2 || SolAtEnt == GmfHOSolAtTrianglesP2 || SolAtEnt == GmfHOSolAtQuadrilateralsQ2 || SolAtEnt == GmfHOSolAtTetrahedraP2 || SolAtEnt == GmfHOSolAtPyramidsP2 || SolAtEnt == GmfHOSolAtPrismsP2 || SolAtEnt == GmfHOSolAtHexahedraQ2)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP3 || SolAtEnt == GmfHOSolAtTrianglesP3 || SolAtEnt == GmfHOSolAtQuadrilateralsQ3 || SolAtEnt == GmfHOSolAtTetrahedraP3 || SolAtEnt == GmfHOSolAtPyramidsP3 || SolAtEnt == GmfHOSolAtPrismsP3 || SolAtEnt == GmfHOSolAtHexahedraQ3)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP4 || SolAtEnt == GmfHOSolAtTrianglesP4 || SolAtEnt == GmfHOSolAtQuadrilateralsQ4 || SolAtEnt == GmfHOSolAtTetrahedraP4 || SolAtEnt == GmfHOSolAtPyramidsP4 || SolAtEnt == GmfHOSolAtPrismsP4 || SolAtEnt == GmfHOSolAtHexahedraQ4)
    return 1;
  return 0;
}

int viz_NewInterface(VizObject** iObj)
{
  VizObject* Obj = (VizObject*)malloc(sizeof(VizObject));
  if (Obj == NULL)
    return 2;

  viz_IniObject(Obj);

  *iObj = Obj;

  return 0;
}

int viz_FreeInterface(VizObject** Obj)
{

  if (!Obj)
    return 2;
  if (!(*Obj))
    return 2;

  viz_FreeObject(*Obj);

  free(*Obj);

  *Obj = NULL;

  return 0;
}

static solution* viz_ReadSolutionLibMeshb(int64_t InpSol, solution* sol, int FilVer, int SolAt, int NbrFld, int SolSiz, int* SolTyp, int NbrEnt, int Deg, int NbrNod, int Dim, int Ite, double Tim, VizIntStatus* status)
{
  int iSol, i, j, iEnt, idx, BufSiz;

  if (SolSiz <= 0) {
    printf("Warning: the size of the solution is invalid \n");
    printf("Please check your solution file \n");
    printf("In particular, check that your type of each solution is good \n");
    printf("1 for a scalar, 2 for a vector, 3 for symmetric matrix and 4 for a full matrix");
    printf(" \n");
    return NULL;
  }

  solution** Sol = malloc(sizeof(solution*) * NbrFld);
  for (iSol = 0; iSol < NbrFld; iSol++) {
    Sol[iSol] = NULL;
  }

  double* solBuf = malloc(sizeof(double) * (NbrEnt + 1) * (SolSiz));
  int*    sizFld = malloc(sizeof(int) * (NbrFld));

  //-- check parameters
  BufSiz = 0;
  for (iSol = 0; iSol < NbrFld; iSol++) {
    switch (SolTyp[iSol]) {
      case GmfSca:
        sizFld[iSol] = 1;
        break;
      case GmfVec:
        sizFld[iSol] = (Dim == 2) ? 2 : 3;
        break;
      case GmfSymMat:
        sizFld[iSol] = (Dim == 2) ? 3 : 6;
        break;
      case GmfMat:
        sizFld[iSol] = (Dim == 2) ? 4 : 9;
        break;
      default:
        printf("Warning: the following solution type is unkown   %d \n", SolTyp[iSol]);
        break;
    }
    BufSiz += sizFld[iSol];
  }
  BufSiz *= NbrNod;
  if (BufSiz != SolSiz) {
    viz_writestatus(status, "Invalid size of solution for Type %s : %d and it should be %d \n", GmfKwdFmt[SolAt][0], BufSiz, SolSiz);
    return NULL;
  }

  for (iSol = 0; iSol < NbrFld; iSol++) {
    //-- for each field, alloc one solution
    Sol[iSol]           = viz_addsol(sol);
    Sol[iSol]->Dim      = Dim;
    Sol[iSol]->NbrSol   = NbrEnt;
    Sol[iSol]->SolTyp   = SolTyp[iSol];
    Sol[iSol]->Nam[0]   = '\0';
    Sol[iSol]->Sol      = malloc(NbrNod * sizFld[iSol] * (NbrEnt + 1) * sizeof(double));
    Sol[iSol]->Ite      = Ite;
    Sol[iSol]->Tim      = Tim;
    Sol[iSol]->Deg      = Deg;
    Sol[iSol]->NbrNod   = NbrNod;
    Sol[iSol]->vizAlloc = 1;
    sol                 = Sol[iSol];
  }

  GmfGetBlock(InpSol, SolAt, 1, NbrEnt, 0, NULL, NULL, GmfDoubleVec, SolSiz, &solBuf[1 * SolSiz], &solBuf[NbrEnt * SolSiz]);

  for (iEnt = 1; iEnt <= NbrEnt; iEnt++) {
    // copy solution in buffer (split)
    idx = 0;
    for (iSol = 0; iSol < NbrFld; iSol++) {
      for (i = 0; i < NbrNod; i++) {
        for (j = 0; j < sizFld[iSol]; j++)
          Sol[iSol]->Sol[iEnt * sizFld[iSol] * NbrNod + i * sizFld[iSol] + j] = solBuf[iEnt * SolSiz + i * SolSiz / NbrNod + j + idx];
      }
      idx += sizFld[iSol];
    }
  }

  // free buffers
  if (solBuf) {
    free(solBuf);
    solBuf = NULL;
  }

  solution* out_sol = sol;

  out_sol = Sol[0];

  // free the contigus array of solution
  free(Sol);
  Sol = NULL;

  return out_sol;
}

int viz_ReadSolutionsStrings(int64_t InpSol, VizObject* iObj)
{
  int  NbrStr, iStr, iFld, FldKwd;
  char FldNam[4096];

  NbrStr = GmfStatKwd(InpSol, GmfReferenceStrings);
  GmfGotoKwd(InpSol, GmfReferenceStrings);
  for (iStr = 0; iStr < NbrStr; iStr++) {
    GmfGetLin(InpSol, GmfReferenceStrings, &FldKwd, &iFld, &FldNam);
    if (viz_IsSolKwd(FldKwd)) {
      FldNam[strcspn(FldNam, "\n")] = 0; //-- remove end of line
      viz_AttachSolutionName(iObj, FldKwd, iFld, FldNam);
    }
  }

  return VIZINT_SUCCESS;
}

int viz_AttachSolutionName(VizObject* iObj, int LibMshKwd, int iSol, char* Nam)
{
  solution* sol = NULL;
  int       idxSol;

  switch (LibMshKwd) {
    case GmfSolAtVertices:
      sol = iObj->CrdSol;
      break;

    case GmfSolAtEdges:
    case GmfHOSolAtEdgesP1:
      sol = iObj->EdgSol;
      break;

    case GmfHOSolAtEdgesP2:
      sol = iObj->P2EdgSol;
      break;

    case GmfHOSolAtEdgesP3:
      sol = iObj->P3EdgSol;
      break;

    case GmfHOSolAtEdgesP4:
      sol = iObj->P4EdgSol;
      break;

    case GmfSolAtTriangles:
    case GmfHOSolAtTrianglesP1:
      sol = iObj->TriSol;
      break;

    case GmfHOSolAtTrianglesP2:
      sol = iObj->P2TriSol;
      break;

    case GmfHOSolAtTrianglesP3:
      sol = iObj->P3TriSol;
      break;

    case GmfHOSolAtTrianglesP4:
      sol = iObj->P4TriSol;
      break;

    case GmfSolAtQuadrilaterals:
    case GmfHOSolAtQuadrilateralsQ1:
      sol = iObj->QuaSol;
      break;

    case GmfHOSolAtQuadrilateralsQ2:
      sol = iObj->Q2QuaSol;
      break;

    case GmfHOSolAtQuadrilateralsQ3:
      sol = iObj->Q3QuaSol;
      break;

    case GmfHOSolAtQuadrilateralsQ4:
      sol = iObj->Q4QuaSol;
      break;

    case GmfSolAtTetrahedra:
    case GmfHOSolAtTetrahedraP1:
      sol = iObj->TetSol;
      break;

    case GmfHOSolAtTetrahedraP2:
      sol = iObj->P2TetSol;
      break;

    case GmfHOSolAtTetrahedraP3:
      sol = iObj->P3TetSol;
      break;

    case GmfHOSolAtTetrahedraP4:
      sol = iObj->P4TetSol;
      break;

    case GmfSolAtPrisms:
    case GmfHOSolAtPrismsP1:
      sol = iObj->PriSol;
      break;

    case GmfHOSolAtPrismsP2:
      sol = iObj->P2PriSol;
      break;

    case GmfHOSolAtPrismsP3:
      sol = iObj->P3PriSol;
      break;

    case GmfHOSolAtPrismsP4:
      sol = iObj->P4PriSol;
      break;

    case GmfSolAtPyramids:
    case GmfHOSolAtPyramidsP1:
      sol = iObj->PyrSol;
      break;

    case GmfHOSolAtPyramidsP2:
      sol = iObj->P2PyrSol;
      break;

    case GmfHOSolAtPyramidsP3:
      sol = iObj->P3PyrSol;
      break;

    case GmfHOSolAtPyramidsP4:
      sol = iObj->P4PyrSol;
      break;

    case GmfSolAtHexahedra:
    case GmfHOSolAtHexahedraQ1:
      sol = iObj->HexSol;
      break;

    case GmfHOSolAtHexahedraQ2:
      sol = iObj->Q2HexSol;
      break;

    case GmfHOSolAtHexahedraQ3:
      sol = iObj->Q3HexSol;
      break;

    case GmfHOSolAtHexahedraQ4:
      sol = iObj->Q4HexSol;
      break;

    default:
      printf(" ERROR: Keyword %s (= %d) is not of solution type\n", GmfKwdFmt[LibMshKwd][0], LibMshKwd);
      return VIZINT_ERROR;
  }

  idxSol = 0;
  while (sol) {
    idxSol++;
    if (idxSol == iSol)
      break;
    else
      sol = sol->nxt;
  }
  if (idxSol == iSol) {
    sprintf(sol->Nam, "%s", Nam);
  }
  else {
    printf(" ERROR: Solution # %d corresponding to keyword %s (= %d) does not exists\n", iSol, GmfKwdFmt[LibMshKwd][0], LibMshKwd);
    return VIZINT_ERROR;
  }
  return VIZINT_SUCCESS;
}

void viz_ConvertHOSolutionInVizirBasis(int64_t InpSol, int SolAtNodesPositions, solution* sol)
{
#if defined(INC_F77)

  int     CrdNodRefSiz, iSol, iNod, jNod, i, j, k, n, sizFld;
  double *CrdNodRef, *CrdNodUsr;
  double *Mat = NULL, *Mat2 = NULL, *solBuf = NULL, *solBuf2 = NULL;
  int     info, lda, lwork;
  double* work;
  int*    ipiv;

  work = NULL;
  ipiv = NULL;

  solution* curSol = NULL;

  CrdNodRef = NULL;

  if (sol->Deg <= 0 || sol->Deg > 10)
    return;

  CrdNodRefSiz = GetCrdNodRefSiz(SolAtNodesPositions);
  CrdNodUsr    = malloc(CrdNodRefSiz * sol->NbrNod * sizeof(double));

  if (CrdNodRefSiz == 2) {
    GmfGetBlock(InpSol, SolAtNodesPositions, 1, sol->NbrNod, 0, NULL, NULL,
        GmfDouble, &(CrdNodUsr[0]), &(CrdNodUsr[2 * (sol->NbrNod - 1) + 0]),
        GmfDouble, &(CrdNodUsr[1]), &(CrdNodUsr[2 * (sol->NbrNod - 1) + 1]));
  }
  else if (CrdNodRefSiz == 3) {
    GmfGetBlock(InpSol, SolAtNodesPositions, 1, sol->NbrNod, 0, NULL, NULL,
        GmfDouble, &(CrdNodUsr[0]), &(CrdNodUsr[3 * (sol->NbrNod - 1) + 0]),
        GmfDouble, &(CrdNodUsr[1]), &(CrdNodUsr[3 * (sol->NbrNod - 1) + 1]),
        GmfDouble, &(CrdNodUsr[2]), &(CrdNodUsr[3 * (sol->NbrNod - 1) + 2]));
  }
  else if (CrdNodRefSiz == 4) {
    GmfGetBlock(InpSol, SolAtNodesPositions, 1, sol->NbrNod, 0, NULL, NULL,
        GmfDouble, &(CrdNodUsr[0]), &(CrdNodUsr[4 * (sol->NbrNod - 1) + 0]),
        GmfDouble, &(CrdNodUsr[1]), &(CrdNodUsr[4 * (sol->NbrNod - 1) + 1]),
        GmfDouble, &(CrdNodUsr[2]), &(CrdNodUsr[4 * (sol->NbrNod - 1) + 2]),
        GmfDouble, &(CrdNodUsr[3]), &(CrdNodUsr[4 * (sol->NbrNod - 1) + 3]));
  }
  else {
    printf("Wrong keyword for HO solution Nodes positions conversion\n");
    free(CrdNodUsr);
    CrdNodUsr = NULL;
    return;
  }

  //-- pyramid is special -> we do not really have the shape functions but only the "monomial" basis proposed by Bergot, Cohen and Durufle in 2010
  if (SolAtNodesPositions == GmfHOSolAtPyramidsP1NodesPositions || SolAtNodesPositions == GmfHOSolAtPyramidsP2NodesPositions || SolAtNodesPositions == GmfHOSolAtPyramidsP3NodesPositions || SolAtNodesPositions == GmfHOSolAtPyramidsP4NodesPositions) {

    CrdNodRef = GetCrdNodRef(SolAtNodesPositions, sol->Deg);

    //---  Vandermonde matrices of "monomials" in user and ref basis
    Mat  = (double*)malloc(sol->NbrNod * sol->NbrNod * sizeof(double));
    Mat2 = (double*)malloc(sol->NbrNod * sol->NbrNod * sizeof(double));

    //-- fill in matrices
    for (iNod = 0; iNod < sol->NbrNod; iNod++) {   //--lines
      for (jNod = 0; jNod < sol->NbrNod; jNod++) { //-- column
        i                               = ((int*)PyramidsOrderingPtr[sol->Deg])[3 * jNod + 0];
        j                               = ((int*)PyramidsOrderingPtr[sol->Deg])[3 * jNod + 1];
        k                               = ((int*)PyramidsOrderingPtr[sol->Deg])[3 * jNod + 2];
        Mat[iNod * sol->NbrNod + jNod]  = pow((CrdNodUsr[3 * iNod]) / (1. - CrdNodUsr[3 * iNod + 2]), i) * pow((CrdNodUsr[3 * iNod + 1]) / (1. - CrdNodUsr[3 * iNod + 2]), j) * pow((1. - CrdNodUsr[3 * iNod + 2]), k);
        Mat2[iNod * sol->NbrNod + jNod] = pow((CrdNodRef[3 * iNod]) / (1. - CrdNodRef[3 * iNod + 2]), i) * pow((CrdNodRef[3 * iNod + 1]) / (1. - CrdNodRef[3 * iNod + 2]), j) * pow((1. - CrdNodRef[3 * iNod + 2]), k);
      }
    }

    //-- set variables to call lapack
    n     = sol->NbrNod;
    info  = 0;
    lda   = n;
    lwork = n;
    work  = malloc(lwork * sizeof(double));
    ipiv  = malloc(n * sizeof(double));

    memset(ipiv, 0, n * sizeof(int));

    //-- get LU decomp from LAPACK
    call(dgetrf)(&n, &n, Mat, &lda, ipiv, &info);

    //-- inverse matrix after LU decomp using LAPACK
    call(dgetri)(&n, Mat, &lda, ipiv, work, &lwork, &info);

    free(work);
    free(ipiv);
    work = NULL;
    ipiv = NULL;

    //--- check if it succeeded
    if (info != 0) {
      printf("HO solution Nodes positions conversion failed for pyramid as the matrix cannot be inverted. Display of the nodes in vizir uniform nodes distribution \n");
      free(CrdNodUsr);
      CrdNodUsr = NULL;
      free(CrdNodRef);
      CrdNodRef = NULL;
      free(Mat);
      Mat = NULL;
      free(Mat2);
      Mat2 = NULL;
      return;
    }

    //-- temporary solutions.
    solBuf  = malloc(sol->NbrNod * sizeof(double));
    solBuf2 = malloc(sol->NbrNod * sizeof(double));

    // inversion of the mapping succeed, now let us convert the solution
    curSol = sol;
    while (curSol != NULL) {                                   //--- loop over the fields
      sizFld = viz_GetSolSize(curSol->Dim, curSol->SolTyp, 1); //-- get the size of the field <=> size of the total solution size for field with one node
      for (i = 0; i < sizFld; i++) {                           //-- loop over each component of the field
        for (iSol = 1; iSol <= curSol->NbrSol; iSol++) {       //-- loop over all the mesh entities
          for (iNod = 0; iNod < curSol->NbrNod; iNod++)        //-- store the current sol at element in a buffer array
            solBuf[iNod] = curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i];

          //-- perform two matrix vector product to compute the new solution
          //-- get coefficients in Bergot's basis
          for (iNod = 0; iNod < curSol->NbrNod; iNod++) {
            solBuf2[iNod] = 0.;
            for (jNod = 0; jNod < curSol->NbrNod; jNod++)
              solBuf2[iNod] += Mat[iNod * curSol->NbrNod + jNod] * solBuf[jNod];
          }
          //-- get the nodes in the uniform basis
          for (iNod = 0; iNod < curSol->NbrNod; iNod++) {
            curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i] = 0.;
            for (jNod = 0; jNod < curSol->NbrNod; jNod++)
              curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i] += Mat2[iNod * curSol->NbrNod + jNod] * solBuf2[jNod];
          }
        }
      }
      curSol = curSol->nxt;
    }

    free(CrdNodUsr);
    free(CrdNodRef);
    CrdNodUsr = NULL;
    CrdNodRef = NULL;
    free(solBuf);
    solBuf = NULL;
    free(solBuf2);
    solBuf2 = NULL;
    free(Mat);
    Mat = NULL;
    free(Mat2);
    Mat2 = NULL;
  }
  else {

    //-- Assembling of the matrix that will be inverted
    //-- fill in matrix with uniform shape functions evaluated in user's node points
    Mat = (double*)malloc(sol->NbrNod * sol->NbrNod * sizeof(double));
    for (iNod = 0; iNod < sol->NbrNod; iNod++) { //--lines
      for (jNod = 0; jNod < sol->NbrNod; jNod++) //-- columns
        Mat[sol->NbrNod * iNod + jNod] = viz_UniformShapeFunctionEvaluation(SolAtNodesPositions, sol->Deg, jNod, (double*)&CrdNodUsr[CrdNodRefSiz * iNod]);
    }

    //-- set variables to call lapack
    n     = sol->NbrNod;
    info  = 0;
    lda   = n;
    lwork = n;
    work  = malloc(lwork * sizeof(double));
    ipiv  = malloc(n * sizeof(double));

    memset(ipiv, 0, n * sizeof(int));

    //-- get LU decomp from LAPACK
    call(dgetrf)(&n, &n, Mat, &lda, ipiv, &info);

    //-- inverse matrix after LU decomp using LAPACK
    call(dgetri)(&n, Mat, &lda, ipiv, work, &lwork, &info);

    free(work);
    free(ipiv);
    work = NULL;
    ipiv = NULL;

    //--- check if it succeeded
    if (info != 0) {
      printf("HO solution Nodes positions conversion failed as the matrix cannot be inverted. Display of the nodes in vizir uniform nodes distribution \n");
      free(CrdNodUsr);
      CrdNodUsr = NULL;
      free(Mat);
      Mat = NULL;
      return;
    }

    solBuf = malloc(sol->NbrNod * sizeof(double));

    // inversion of the mapping succeed, now let us convert the solution
    curSol = sol;
    while (curSol != NULL) {                                   //--- loop over the fields
      sizFld = viz_GetSolSize(curSol->Dim, curSol->SolTyp, 1); //-- get the size of the field <=> size of the total solution size for field with one node
      for (i = 0; i < sizFld; i++) {                           //-- loop over each component of the field
        for (iSol = 1; iSol <= curSol->NbrSol; iSol++) {       //-- loop over all the mesh entities
          for (iNod = 0; iNod < curSol->NbrNod; iNod++)        //-- store the current sol at element in a buffer array
            solBuf[iNod] = curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i];

          for (iNod = 0; iNod < curSol->NbrNod; iNod++) { //-- perform matrix vector product to coimpute the new solution
            curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i] = 0.;
            for (jNod = 0; jNod < curSol->NbrNod; jNod++)
              curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i] += Mat[iNod * curSol->NbrNod + jNod] * solBuf[jNod];
          }
        }
      }
      curSol = curSol->nxt;
    }

    free(CrdNodUsr);
    CrdNodUsr = NULL;
    free(solBuf);
    solBuf = NULL;
    free(Mat);
    Mat = NULL;
  }

#else
  printf("HO solution Nodes positions conversion not supported as F77 libraries are not included in the project\n");
#endif
  return;
}

int viz_OpenMesh(VizObject* iObj, const char* MshFile, VizIntStatus* status)
{
  int ierr;
  if ((strstr(MshFile, ".cgns") != NULL) || (strstr(MshFile, ".hdf") != NULL))
    return viz_OpenMeshCGNS(iObj, MshFile, status);
  else {
    //--- redirect error signals
#if defined(__linux__) || defined(__APPLE__)
    signal(SIGILL, readMshErr);
    signal(SIGFPE, readMshErr);
    signal(SIGBUS, readMshErr);
    signal(SIGSEGV, readMshErr);
    signal(SIGQUIT, readMshErr);
#endif
    ierr = viz_OpenMeshLibMeshb(iObj, MshFile, status);
//--- reset default error signals
#if defined(__linux__) || defined(__APPLE__)
    signal(SIGILL, SIG_DFL);
    signal(SIGFPE, SIG_DFL);
    signal(SIGBUS, SIG_DFL);
    signal(SIGSEGV, SIG_DFL);
    signal(SIGQUIT, SIG_DFL);
#endif
    return ierr;
  }
}

int viz_OpenMeshLibMeshb(VizObject* Obj, const char* MshFile, VizIntStatus* status)
{

  int1 iVer;

  //--- HO numbering arrays
  int1 P2EdgFilOrd[3], P3EdgFilOrd[4], P4EdgFilOrd[5], P1TriFilOrd[3][3], P2TriFilOrd[6][3], P3TriFilOrd[10][3], P4TriFilOrd[15][3], Q2QuaFilOrd[9][2], Q3QuaFilOrd[16][2], Q4QuaFilOrd[25][2];
  int1 P2TetFilOrd[10][4], P3TetFilOrd[20][4], P4TetFilOrd[35][4], Q2HexFilOrd[27][3], Q3HexFilOrd[64][3], Q4HexFilOrd[125][3], P2PyrFilOrd[14][3], P3PyrFilOrd[30][3], P4PyrFilOrd[55][3], P2PriFilOrd[18][4], P3PriFilOrd[40][4], P4PriFilOrd[75][4];

  char bufNam[1024];
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!MshFile) {
    viz_writestatus(status, "NULL (MshFile)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  int     FilVer, DimRead;
  int64_t InpMsh = GmfOpenMesh(MshFile, GmfRead, &FilVer, &DimRead);
  if (InpMsh <= 0) {
    if (strstr(MshFile, ".mesh") == NULL) {
      sprintf(bufNam, "%s.meshb", MshFile);
      InpMsh = GmfOpenMesh(bufNam, GmfRead, &FilVer, &DimRead);
      if (InpMsh <= 0) {
        sprintf(bufNam, "%s.mesh", MshFile);
        InpMsh = GmfOpenMesh(bufNam, GmfRead, &FilVer, &DimRead);
        if (InpMsh <= 0) {
          viz_writestatus(status, "Cannot open mesh file %s.[mesh[b]]\n", MshFile);
          return VIZINT_ERROR;
        }
      }
      viz_writeinfo(status, "%s", bufNam);
    }
    else {
      viz_writestatus(status, "Cannot open mesh file %s\n", MshFile);
      return VIZINT_ERROR;
    }
  }
  else
    viz_writeinfo(status, "%s", MshFile);

  /* free previous mesh */
  viz_FreeObject(Obj);

  Obj->Dim = DimRead;

  //--- read HO numbering arrays if any
  if (GmfStatKwd(InpMsh, GmfEdgesP2Ordering)) {
    GmfGetBlock(InpMsh, GmfEdgesP2Ordering, 1, 3, 0, NULL, NULL, typeOf[sizeof(int1)], &(P2EdgFilOrd[0]), &(P2EdgFilOrd[2]));
    GmfSetHONodesOrdering(InpMsh, GmfEdgesP2, (int*)P2EdgesOrdering, (int*)P2EdgFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfEdgesP3Ordering)) {
    GmfGetBlock(InpMsh, GmfEdgesP3Ordering, 1, 4, 0, NULL, NULL, typeOf[sizeof(int1)], &(P3EdgFilOrd[0]), &(P3EdgFilOrd[3]));
    GmfSetHONodesOrdering(InpMsh, GmfEdgesP3, (int*)P3EdgesOrdering, (int*)P3EdgFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfEdgesP4Ordering)) {
    GmfGetBlock(InpMsh, GmfEdgesP4Ordering, 1, 5, 0, NULL, NULL, typeOf[sizeof(int1)], &(P4EdgFilOrd[0]), &(P4EdgFilOrd[4]));
    GmfSetHONodesOrdering(InpMsh, GmfEdgesP4, (int*)P4EdgesOrdering, (int*)P4EdgFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTrianglesP1Ordering)) {
    GmfGetBlock(InpMsh, GmfTrianglesP1Ordering, 1, 3, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P1TriFilOrd[0], P1TriFilOrd[2]);
    GmfSetHONodesOrdering(InpMsh, GmfTriangles, (int*)P1TrianglesOrdering, (int*)P1TriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTrianglesP2Ordering)) {
    GmfGetBlock(InpMsh, GmfTrianglesP2Ordering, 1, 6, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P2TriFilOrd[0], P2TriFilOrd[5]);
    GmfSetHONodesOrdering(InpMsh, GmfTrianglesP2, (int*)P2TrianglesOrdering, (int*)P2TriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTrianglesP3Ordering)) {
    GmfGetBlock(InpMsh, GmfTrianglesP3Ordering, 1, 10, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P3TriFilOrd[0], P3TriFilOrd[9]);
    GmfSetHONodesOrdering(InpMsh, GmfTrianglesP3, (int*)P3TrianglesOrdering, (int*)P3TriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTrianglesP4Ordering)) {
    GmfGetBlock(InpMsh, GmfTrianglesP4Ordering, 1, 15, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P4TriFilOrd[0], P4TriFilOrd[14]);
    GmfSetHONodesOrdering(InpMsh, GmfTrianglesP4, (int*)P4TrianglesOrdering, (int*)P4TriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfQuadrilateralsQ2Ordering)) {
    GmfGetBlock(InpMsh, GmfQuadrilateralsQ2Ordering, 1, 9, 0, NULL, NULL, typeOfTab[sizeof(int1)], 2, Q2QuaFilOrd[0], Q2QuaFilOrd[8]);
    GmfSetHONodesOrdering(InpMsh, GmfQuadrilateralsQ2, (int*)Q2QuadrilateralsOrdering, (int*)Q2QuaFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfQuadrilateralsQ3Ordering)) {
    GmfGetBlock(InpMsh, GmfQuadrilateralsQ3Ordering, 1, 16, 0, NULL, NULL, typeOfTab[sizeof(int1)], 2, Q3QuaFilOrd[0], Q3QuaFilOrd[15]);
    GmfSetHONodesOrdering(InpMsh, GmfQuadrilateralsQ3, (int*)Q3QuadrilateralsOrdering, (int*)Q3QuaFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfQuadrilateralsQ4Ordering)) {
    GmfGetBlock(InpMsh, GmfQuadrilateralsQ4Ordering, 1, 25, 0, NULL, NULL, typeOfTab[sizeof(int1)], 2, Q4QuaFilOrd[0], Q4QuaFilOrd[24]);
    GmfSetHONodesOrdering(InpMsh, GmfQuadrilateralsQ4, (int*)Q4QuadrilateralsOrdering, (int*)Q4QuaFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTetrahedraP2Ordering)) {
    GmfGetBlock(InpMsh, GmfTetrahedraP2Ordering, 1, 10, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P2TetFilOrd[0], P2TetFilOrd[9]);
    GmfSetHONodesOrdering(InpMsh, GmfTetrahedraP2, (int*)P2TetrahedraOrdering, (int*)P2TetFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTetrahedraP3Ordering)) {
    GmfGetBlock(InpMsh, GmfTetrahedraP3Ordering, 1, 20, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P3TetFilOrd[0], P3TetFilOrd[19]);
    GmfSetHONodesOrdering(InpMsh, GmfTetrahedraP3, (int*)P3TetrahedraOrdering, (int*)P3TetFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTetrahedraP4Ordering)) {
    GmfGetBlock(InpMsh, GmfTetrahedraP4Ordering, 1, 35, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P4TetFilOrd[0], P4TetFilOrd[34]);
    GmfSetHONodesOrdering(InpMsh, GmfTetrahedraP4, (int*)P4TetrahedraOrdering, (int*)P4TetFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPyramidsP2Ordering)) {
    GmfGetBlock(InpMsh, GmfPyramidsP2Ordering, 1, 14, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P2PyrFilOrd[0], P2PyrFilOrd[13]);
    GmfSetHONodesOrdering(InpMsh, GmfPyramidsP2, (int*)P2PyramidsOrdering, (int*)P2PyrFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPyramidsP3Ordering)) {
    GmfGetBlock(InpMsh, GmfPyramidsP3Ordering, 1, 30, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P3PyrFilOrd[0], P3PyrFilOrd[29]);
    GmfSetHONodesOrdering(InpMsh, GmfPyramidsP3, (int*)P3PyramidsOrdering, (int*)P3PyrFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPyramidsP4Ordering)) {
    GmfGetBlock(InpMsh, GmfPyramidsP4Ordering, 1, 55, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P4PyrFilOrd[0], P4PyrFilOrd[54]);
    GmfSetHONodesOrdering(InpMsh, GmfPyramidsP4, (int*)P4PyramidsOrdering, (int*)P4PyrFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPrismsP2Ordering)) {
    GmfGetBlock(InpMsh, GmfPrismsP2Ordering, 1, 18, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P2PriFilOrd[0], P2PriFilOrd[17]);
    GmfSetHONodesOrdering(InpMsh, GmfPrismsP2, (int*)P2PrismsOrdering, (int*)P2PriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPrismsP3Ordering)) {
    GmfGetBlock(InpMsh, GmfPrismsP3Ordering, 1, 40, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P3PriFilOrd[0], P3PriFilOrd[39]);
    GmfSetHONodesOrdering(InpMsh, GmfPrismsP3, (int*)P3PrismsOrdering, (int*)P3PriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPrismsP4Ordering)) {
    GmfGetBlock(InpMsh, GmfPrismsP4Ordering, 1, 75, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P4PriFilOrd[0], P4PriFilOrd[74]);
    GmfSetHONodesOrdering(InpMsh, GmfPrismsP4, (int*)P4PrismsOrdering, (int*)P4PriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfHexahedraQ2Ordering)) {
    GmfGetBlock(InpMsh, GmfHexahedraQ2Ordering, 1, 27, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, Q2HexFilOrd[0], Q2HexFilOrd[26]);
    GmfSetHONodesOrdering(InpMsh, GmfHexahedraQ2, (int*)Q2HexahedraOrdering, (int*)Q2HexFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfHexahedraQ3Ordering)) {
    GmfGetBlock(InpMsh, GmfHexahedraQ3Ordering, 1, 64, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, Q3HexFilOrd[0], Q3HexFilOrd[63]);
    GmfSetHONodesOrdering(InpMsh, GmfHexahedraQ3, (int*)Q3HexahedraOrdering, (int*)Q3HexFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfHexahedraQ4Ordering)) {
    GmfGetBlock(InpMsh, GmfHexahedraQ4Ordering, 1, 125, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, Q4HexFilOrd[0], Q4HexFilOrd[124]);
    GmfSetHONodesOrdering(InpMsh, GmfHexahedraQ4, (int*)Q4HexahedraOrdering, (int*)Q4HexFilOrd);
  }

  /* Reading vertices */
  Obj->NbrVer = GmfStatKwd(InpMsh, GmfVertices);
  Obj->Crd    = viz_reallocate(sizeof(double3) * (Obj->NbrVer + 1), Obj->Crd);
  if (!Obj->Crd) {
    viz_writestatus(status, "Allocation failed for vertex-arrays, size %lg Mb\n", sizeof(double3) * (Obj->NbrVer + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->CrdRef = viz_reallocate(sizeof(int1) * (Obj->NbrVer + 1), Obj->CrdRef);
  if (!Obj->CrdRef) {
    viz_writestatus(status, "Allocation failed for vertex-arrays, size %lg Mb\n", sizeof(double3) * (Obj->NbrVer + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }

  /* Reading vertices */
  if (DimRead == 3) {
    Obj->NbrVerMax = Obj->NbrVer;
    // GmfGotoKwd(InpMsh, GmfVertices);
    GmfGetBlock(InpMsh, GmfVertices, 1, Obj->NbrVer, 0, NULL, NULL,
        GmfDouble, &(Obj->Crd[1][0]), &(Obj->Crd[Obj->NbrVer][0]),
        GmfDouble, &(Obj->Crd[1][1]), &(Obj->Crd[Obj->NbrVer][1]),
        GmfDouble, &(Obj->Crd[1][2]), &(Obj->Crd[Obj->NbrVer][2]),
        typeOf[sizeof(int1)], &Obj->CrdRef[1], &Obj->CrdRef[Obj->NbrVer]);
  }
  else {
    Obj->NbrVerMax = Obj->NbrVer;
    // GmfGotoKwd(InpMsh, GmfVertices);
    GmfGetBlock(InpMsh, GmfVertices, 1, Obj->NbrVer, 0, NULL, NULL,
        GmfDouble, &(Obj->Crd[1][0]), &(Obj->Crd[Obj->NbrVer][0]),
        GmfDouble, &(Obj->Crd[1][1]), &(Obj->Crd[Obj->NbrVer][1]),
        typeOf[sizeof(int1)], &Obj->CrdRef[1], &Obj->CrdRef[Obj->NbrVer]);
    for (iVer = 1; iVer <= Obj->NbrVer; iVer++)
      Obj->Crd[iVer][2] = 0.0;
  }

  /* Reading edges */
  Obj->NbrEdg = GmfStatKwd(InpMsh, GmfEdges);
  Obj->Edg    = viz_reallocate(sizeof(int2) * (Obj->NbrEdg + 1), Obj->Edg);
  Obj->EdgRef = viz_reallocate(sizeof(int1) * (Obj->NbrEdg + 1), Obj->EdgRef);
  if (!Obj->Edg || !Obj->EdgRef) {
    viz_writestatus(status, "Allocation failed for Edge-arrays, size %lg Mb\n", sizeof(int2) * (Obj->NbrEdg + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrEdgMax = Obj->NbrEdg;
  // GmfGotoKwd(InpMsh, GmfEdges);
  GmfGetBlock(InpMsh, GmfEdges, 1, Obj->NbrEdg, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 2, &Obj->Edg[1], &Obj->Edg[Obj->NbrEdg],
      typeOf[sizeof(int1)], &Obj->EdgRef[1], &Obj->EdgRef[Obj->NbrEdg]);

  /* Reading corners */
  Obj->NbrCrn = GmfStatKwd(InpMsh, GmfCorners);
  Obj->Crn    = viz_reallocate(sizeof(int1) * (Obj->NbrCrn + 1), Obj->Crn);
  Obj->CrnRef = viz_reallocate(sizeof(int1) * (Obj->NbrCrn + 1), Obj->CrnRef);
  if (!Obj->Crn) {
    viz_writestatus(status, "Allocation failed for corners-arrays, size %lg Mb\n", sizeof(int) * (Obj->NbrCrn + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrCrnMax = Obj->NbrCrn;
  // GmfGotoKwd(InpMsh, GmfCorners);
  GmfGetBlock(InpMsh, GmfCorners, 1, Obj->NbrCrn, 0, NULL, NULL,
      typeOf[sizeof(int1)], &Obj->Crn[1], &Obj->Crn[Obj->NbrCrn]);

  for (iVer = 1; iVer <= Obj->NbrCrn; iVer++) {
    if (Obj->Crn[iVer] < Obj->NbrVer)
      Obj->CrnRef[iVer] = Obj->CrdRef[Obj->Crn[iVer]];
    else
      Obj->CrnRef[iVer] = 0;
  }

  /* Reading triangles */
  Obj->NbrTri = GmfStatKwd(InpMsh, GmfTriangles);
  Obj->Tri    = viz_reallocate(sizeof(int3) * (Obj->NbrTri + 1), Obj->Tri);
  Obj->TriRef = viz_reallocate(sizeof(int1) * (Obj->NbrTri + 1), Obj->TriRef);
  if (!Obj->Tri || !Obj->TriRef) {
    viz_writestatus(status, "Allocation failed for triangle-arrays, size %lg Mb\n", sizeof(int3) * (Obj->NbrTri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrTriMax = Obj->NbrTri;
  // GmfGotoKwd(InpMsh, GmfTriangles);
  GmfGetBlock(InpMsh, GmfTriangles, 1, Obj->NbrTri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 3, &Obj->Tri[1], &Obj->Tri[Obj->NbrTri],
      typeOf[sizeof(int1)], &Obj->TriRef[1], &Obj->TriRef[Obj->NbrTri]);

  /* Reading quads */
  Obj->NbrQua = GmfStatKwd(InpMsh, GmfQuadrilaterals);
  Obj->Qua    = viz_reallocate(sizeof(int4) * (Obj->NbrQua + 1), Obj->Qua);
  Obj->QuaRef = viz_reallocate(sizeof(int1) * (Obj->NbrQua + 1), Obj->QuaRef);
  if (!Obj->Qua || !Obj->QuaRef) {
    viz_writestatus(status, "Allocation failed for Quad-arrays, size %lg Mb\n", sizeof(int4) * (Obj->NbrQua + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQuaMax = Obj->NbrQua;
  // GmfGotoKwd(InpMsh, GmfQuadrilaterals);
  GmfGetBlock(InpMsh, GmfQuadrilaterals, 1, Obj->NbrQua, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 4, &Obj->Qua[1], &Obj->Qua[Obj->NbrQua],
      typeOf[sizeof(int1)], &Obj->QuaRef[1], &Obj->QuaRef[Obj->NbrQua]);

  /* Reading tets */
  Obj->NbrTet = GmfStatKwd(InpMsh, GmfTetrahedra);
  Obj->Tet    = viz_reallocate(sizeof(int4) * (Obj->NbrTet + 1), Obj->Tet);
  Obj->TetRef = viz_reallocate(sizeof(int1) * (Obj->NbrTet + 1), Obj->TetRef);
  if (!Obj->Tet || !Obj->TetRef) {
    viz_writestatus(status, "Allocation failed for Tet-arrays, size %lg Mb\n", sizeof(int4) * (Obj->NbrTet + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrTetMax = Obj->NbrTet;
  // GmfGotoKwd(InpMsh, GmfTetrahedra);
  GmfGetBlock(InpMsh, GmfTetrahedra, 1, Obj->NbrTet, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 4, &Obj->Tet[1], &Obj->Tet[Obj->NbrTet],
      typeOf[sizeof(int1)], &Obj->TetRef[1], &Obj->TetRef[Obj->NbrTet]);

  /* Reading Pyramids */
  Obj->NbrPyr = GmfStatKwd(InpMsh, GmfPyramids);
  Obj->Pyr    = viz_reallocate(sizeof(int5) * (Obj->NbrPyr + 1), Obj->Pyr);
  Obj->PyrRef = viz_reallocate(sizeof(int1) * (Obj->NbrPyr + 1), Obj->PyrRef);
  if (!Obj->Pyr || !Obj->PyrRef) {
    viz_writestatus(status, "Allocation failed for Pyramid-arrays, size %lg Mb\n", sizeof(int5) * (Obj->NbrPyr + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrPyrMax = Obj->NbrPyr;
  // GmfGotoKwd(InpMsh,  GmfPyramids);
  GmfGetBlock(InpMsh, GmfPyramids, 1, Obj->NbrPyr, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 5, &Obj->Pyr[1], &Obj->Pyr[Obj->NbrPyr],
      typeOf[sizeof(int1)], &Obj->PyrRef[1], &Obj->PyrRef[Obj->NbrPyr]);

  /* Reading Prisms */
  Obj->NbrPri = GmfStatKwd(InpMsh, GmfPrisms);
  Obj->Pri    = viz_reallocate(sizeof(int6) * (Obj->NbrPri + 1), Obj->Pri);
  Obj->PriRef = viz_reallocate(sizeof(int1) * (Obj->NbrPri + 1), Obj->PriRef);
  if (!Obj->Pri || !Obj->PriRef) {
    viz_writestatus(status, "Allocation failed for Prism-arrays, size %lg Mb\n", sizeof(int6) * (Obj->NbrPri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrPriMax = Obj->NbrPri;
  // GmfGotoKwd(InpMsh,  GmfPrisms);
  GmfGetBlock(InpMsh, GmfPrisms, 1, Obj->NbrPri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 6, &Obj->Pri[1], &Obj->Pri[Obj->NbrPri],
      typeOf[sizeof(int1)], &Obj->PriRef[1], &Obj->PriRef[Obj->NbrPri]);

  /* Reading hexahedra */
  Obj->NbrHex = GmfStatKwd(InpMsh, GmfHexahedra);
  Obj->Hex    = viz_reallocate(sizeof(int8) * (Obj->NbrHex + 1), Obj->Hex);
  Obj->HexRef = viz_reallocate(sizeof(int1) * (Obj->NbrHex + 1), Obj->HexRef);
  if (!Obj->Hex || !Obj->HexRef) {
    viz_writestatus(status, "Allocation failed for Hex-arrays, size %lg Mb\n", sizeof(int8) * (Obj->NbrHex + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrHexMax = Obj->NbrHex;
  // GmfGotoKwd(InpMsh,  GmfHexahedra);
  GmfGetBlock(InpMsh, GmfHexahedra, 1, Obj->NbrHex, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 8, &Obj->Hex[1], &Obj->Hex[Obj->NbrHex],
      typeOf[sizeof(int1)], &Obj->HexRef[1], &Obj->HexRef[Obj->NbrHex]);

  /* Read High order entities if any */

  /* reading P2 edges */
  Obj->NbrP2Edg = GmfStatKwd(InpMsh, GmfEdgesP2);
  Obj->P2Edg    = viz_reallocate(sizeof(int3) * (Obj->NbrP2Edg + 1), Obj->P2Edg);
  Obj->P2EdgRef = viz_reallocate(sizeof(int1) * (Obj->NbrP2Edg + 1), Obj->P2EdgRef);
  if (!Obj->P2Edg || !Obj->P2EdgRef) {
    viz_writestatus(status, "Allocation failed for P2Edge-arrays, size %lg Mb\n", sizeof(int3) * (Obj->NbrP2Edg + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP2EdgMax = Obj->NbrP2Edg;
  // GmfGotoKwd(InpMsh, GmfEdgesP2);
  GmfGetBlock(InpMsh, GmfEdgesP2, 1, Obj->NbrP2Edg, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 3, &Obj->P2Edg[1], &Obj->P2Edg[Obj->NbrP2Edg],
      typeOf[sizeof(int1)], &Obj->P2EdgRef[1], &Obj->P2EdgRef[Obj->NbrP2Edg]);

  /* reading P3 edges */
  Obj->NbrP3Edg = GmfStatKwd(InpMsh, GmfEdgesP3);
  Obj->P3Edg    = viz_reallocate(sizeof(int4) * (Obj->NbrP3Edg + 1), Obj->P3Edg);
  Obj->P3EdgRef = viz_reallocate(sizeof(int1) * (Obj->NbrP3Edg + 1), Obj->P3EdgRef);
  if (!Obj->P3Edg || !Obj->P3EdgRef) {
    viz_writestatus(status, "Allocation failed for P3Edge-arrays, size %lg Mb\n", sizeof(int4) * (Obj->NbrP3Edg + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP3EdgMax = Obj->NbrP3Edg;
  // GmfGotoKwd(InpMsh, GmfEdgesP3);
  GmfGetBlock(InpMsh, GmfEdgesP3, 1, Obj->NbrP3Edg, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 4, &Obj->P3Edg[1], &Obj->P3Edg[Obj->NbrP3Edg],
      typeOf[sizeof(int1)], &Obj->P3EdgRef[1], &Obj->P3EdgRef[Obj->NbrP3Edg]);

  /* reading P4 edges */
  Obj->NbrP4Edg = GmfStatKwd(InpMsh, GmfEdgesP4);
  Obj->P4Edg    = viz_reallocate(sizeof(int5) * (Obj->NbrP4Edg + 1), Obj->P4Edg);
  Obj->P4EdgRef = viz_reallocate(sizeof(int1) * (Obj->NbrP4Edg + 1), Obj->P4EdgRef);
  if (!Obj->P4Edg || !Obj->P4EdgRef) {
    viz_writestatus(status, "Allocation failed for P4Edge-arrays, size %lg Mb\n", sizeof(int5) * (Obj->NbrP4Edg + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP4EdgMax = Obj->NbrP4Edg;
  // GmfGotoKwd(InpMsh, GmfEdgesP4);
  GmfGetBlock(InpMsh, GmfEdgesP4, 1, Obj->NbrP4Edg, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 5, &Obj->P4Edg[1], &Obj->P4Edg[Obj->NbrP4Edg],
      typeOf[sizeof(int1)], &Obj->P4EdgRef[1], &Obj->P4EdgRef[Obj->NbrP4Edg]);

  /* Reading P2 triangles */
  Obj->NbrP2Tri = GmfStatKwd(InpMsh, GmfTrianglesP2);
  Obj->P2Tri    = viz_reallocate(sizeof(int6) * (Obj->NbrP2Tri + 1), Obj->P2Tri);
  Obj->P2TriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP2Tri + 1), Obj->P2TriRef);
  if (!Obj->P2Tri || !Obj->P2TriRef) {
    viz_writestatus(status, "Allocation failed for P2Triangle-arrays, size %lg Mb\n", sizeof(int6) * (Obj->NbrP2Tri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP2TriMax = Obj->NbrP2Tri;
  // GmfGotoKwd(InpMsh, GmfTrianglesP2);
  GmfGetBlock(InpMsh, GmfTrianglesP2, 1, Obj->NbrP2Tri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 6, &Obj->P2Tri[1], &Obj->P2Tri[Obj->NbrP2Tri],
      typeOf[sizeof(int1)], &Obj->P2TriRef[1], &Obj->P2TriRef[Obj->NbrP2Tri]);

  /* Reading P3 triangles */
  Obj->NbrP3Tri = GmfStatKwd(InpMsh, GmfTrianglesP3);
  Obj->P3Tri    = viz_reallocate(sizeof(int10) * (Obj->NbrP3Tri + 1), Obj->P3Tri);
  Obj->P3TriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP3Tri + 1), Obj->P3TriRef);
  if (!Obj->P3Tri || !Obj->P3TriRef) {
    viz_writestatus(status, "Allocation failed for P3Triangle-arrays, size %lg Mb\n", sizeof(int10) * (Obj->NbrP3Tri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP3TriMax = Obj->NbrP3Tri;
  // GmfGotoKwd(InpMsh, GmfTrianglesP3);
  GmfGetBlock(InpMsh, GmfTrianglesP3, 1, Obj->NbrP3Tri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 10, &Obj->P3Tri[1], &Obj->P3Tri[Obj->NbrP3Tri],
      typeOf[sizeof(int1)], &Obj->P3TriRef[1], &Obj->P3TriRef[Obj->NbrP3Tri]);

  /* Reading P4 triangles */
  Obj->NbrP4Tri = GmfStatKwd(InpMsh, GmfTrianglesP4);
  Obj->P4Tri    = viz_reallocate(sizeof(int15) * (Obj->NbrP4Tri + 1), Obj->P4Tri);
  Obj->P4TriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP4Tri + 1), Obj->P4TriRef);
  if (!Obj->P4Tri || !Obj->P4TriRef) {
    viz_writestatus(status, "Allocation failed for P4Triangle-arrays, size %lg Mb\n", sizeof(int15) * (Obj->NbrP4Tri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP4TriMax = Obj->NbrP4Tri;
  // GmfGotoKwd(InpMsh, GmfTrianglesP4);
  GmfGetBlock(InpMsh, GmfTrianglesP4, 1, Obj->NbrP4Tri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 15, &Obj->P4Tri[1], &Obj->P4Tri[Obj->NbrP4Tri],
      typeOf[sizeof(int1)], &Obj->P4TriRef[1], &Obj->P4TriRef[Obj->NbrP4Tri]);

  /* Reading Q2 quads */
  Obj->NbrQ2Qua = GmfStatKwd(InpMsh, GmfQuadrilateralsQ2);
  Obj->Q2Qua    = viz_reallocate(sizeof(int9) * (Obj->NbrQ2Qua + 1), Obj->Q2Qua);
  Obj->Q2QuaRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ2Qua + 1), Obj->Q2QuaRef);
  if (!Obj->Q2Qua || !Obj->Q2QuaRef) {
    viz_writestatus(status, "Allocation failed for Q2Quad-arrays, size %lg Mb\n", sizeof(int9) * (Obj->NbrQ2Qua + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ2QuaMax = Obj->NbrQ2Qua;
  // GmfGotoKwd(InpMsh, GmfQuadrilateralsQ2);
  GmfGetBlock(InpMsh, GmfQuadrilateralsQ2, 1, Obj->NbrQ2Qua, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 9, &Obj->Q2Qua[1], &Obj->Q2Qua[Obj->NbrQ2Qua],
      typeOf[sizeof(int1)], &Obj->Q2QuaRef[1], &Obj->Q2QuaRef[Obj->NbrQ2Qua]);

  /* Reading Q3 quads */
  Obj->NbrQ3Qua = GmfStatKwd(InpMsh, GmfQuadrilateralsQ3);
  Obj->Q3Qua    = viz_reallocate(sizeof(int16) * (Obj->NbrQ3Qua + 1), Obj->Q3Qua);
  Obj->Q3QuaRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ3Qua + 1), Obj->Q3QuaRef);
  if (!Obj->Q3Qua || !Obj->Q3QuaRef) {
    viz_writestatus(status, "Allocation failed for Q3Quad-arrays, size %lg Mb\n", sizeof(int16) * (Obj->NbrQ3Qua + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ3QuaMax = Obj->NbrQ3Qua;
  // GmfGotoKwd(InpMsh, GmfQuadrilateralsQ3);
  GmfGetBlock(InpMsh, GmfQuadrilateralsQ3, 1, Obj->NbrQ3Qua, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 16, &Obj->Q3Qua[1], &Obj->Q3Qua[Obj->NbrQ3Qua],
      typeOf[sizeof(int1)], &Obj->Q3QuaRef[1], &Obj->Q3QuaRef[Obj->NbrQ3Qua]);

  /* Reading Q4 quads */
  Obj->NbrQ4Qua = GmfStatKwd(InpMsh, GmfQuadrilateralsQ4);
  Obj->Q4Qua    = viz_reallocate(sizeof(int25) * (Obj->NbrQ4Qua + 1), Obj->Q4Qua);
  Obj->Q4QuaRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ4Qua + 1), Obj->Q4QuaRef);
  if (!Obj->Q4Qua || !Obj->Q4QuaRef) {
    viz_writestatus(status, "Allocation failed for Q4Quad-arrays, size %lg Mb\n", sizeof(int25) * (Obj->NbrQ4Qua + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ4QuaMax = Obj->NbrQ4Qua;
  // GmfGotoKwd(InpMsh, GmfQuadrilateralsQ4);
  GmfGetBlock(InpMsh, GmfQuadrilateralsQ4, 1, Obj->NbrQ4Qua, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 25, &Obj->Q4Qua[1], &Obj->Q4Qua[Obj->NbrQ4Qua],
      typeOf[sizeof(int1)], &Obj->Q4QuaRef[1], &Obj->Q4QuaRef[Obj->NbrQ4Qua]);

  /* Reading P2 Tets */
  Obj->NbrP2Tet = GmfStatKwd(InpMsh, GmfTetrahedraP2);
  Obj->P2Tet    = viz_reallocate(sizeof(int10) * (Obj->NbrP2Tet + 1), Obj->P2Tet);
  Obj->P2TetRef = viz_reallocate(sizeof(int1) * (Obj->NbrP2Tet + 1), Obj->P2TetRef);
  if (!Obj->P2Tet || !Obj->P2TetRef) {
    viz_writestatus(status, "Allocation failed for P2Tet-arrays, size %lg Mb\n", sizeof(int10) * (Obj->NbrP2Tet + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP2TetMax = Obj->NbrP2Tet;
  // GmfGotoKwd(InpMsh, GmfTetrahedraP2);
  GmfGetBlock(InpMsh, GmfTetrahedraP2, 1, Obj->NbrP2Tet, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 10, &Obj->P2Tet[1], &Obj->P2Tet[Obj->NbrP2Tet],
      typeOf[sizeof(int1)], &Obj->P2TetRef[1], &Obj->P2TetRef[Obj->NbrP2Tet]);

  /* Reading P3 Tets */
  Obj->NbrP3Tet = GmfStatKwd(InpMsh, GmfTetrahedraP3);
  Obj->P3Tet    = viz_reallocate(sizeof(int20) * (Obj->NbrP3Tet + 1), Obj->P3Tet);
  Obj->P3TetRef = viz_reallocate(sizeof(int1) * (Obj->NbrP3Tet + 1), Obj->P3TetRef);
  if (!Obj->P3Tet || !Obj->P3TetRef) {
    viz_writestatus(status, "Allocation failed for P3Tet-arrays, size %lg Mb\n", sizeof(int20) * (Obj->NbrP3Tet + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP3TetMax = Obj->NbrP3Tet;
  // GmfGotoKwd(InpMsh, GmfTetrahedraP3);
  GmfGetBlock(InpMsh, GmfTetrahedraP3, 1, Obj->NbrP3Tet, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 20, &Obj->P3Tet[1], &Obj->P3Tet[Obj->NbrP3Tet],
      typeOf[sizeof(int1)], &Obj->P3TetRef[1], &Obj->P3TetRef[Obj->NbrP3Tet]);

  /* Reading P4 Tets */
  Obj->NbrP4Tet = GmfStatKwd(InpMsh, GmfTetrahedraP4);
  Obj->P4Tet    = viz_reallocate(sizeof(int35) * (Obj->NbrP4Tet + 1), Obj->P4Tet);
  Obj->P4TetRef = viz_reallocate(sizeof(int1) * (Obj->NbrP4Tet + 1), Obj->P4TetRef);
  if (!Obj->P4Tet || !Obj->P4TetRef) {
    viz_writestatus(status, "Allocation failed for P4Tet-arrays, size %lg Mb\n", sizeof(int35) * (Obj->NbrP4Tet + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP4TetMax = Obj->NbrP4Tet;
  // GmfGotoKwd(InpMsh, GmfTetrahedraP4);
  GmfGetBlock(InpMsh, GmfTetrahedraP4, 1, Obj->NbrP4Tet, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 35, &Obj->P4Tet[1], &Obj->P4Tet[Obj->NbrP4Tet],
      typeOf[sizeof(int1)], &Obj->P4TetRef[1], &Obj->P4TetRef[Obj->NbrP4Tet]);

  /* Reading Q2 hexahedra */
  Obj->NbrQ2Hex = GmfStatKwd(InpMsh, GmfHexahedraQ2);
  Obj->Q2Hex    = viz_reallocate(sizeof(int27) * (Obj->NbrQ2Hex + 1), Obj->Q2Hex);
  Obj->Q2HexRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ2Hex + 1), Obj->Q2HexRef);
  if (!Obj->Q2Hex || !Obj->Q2HexRef) {
    viz_writestatus(status, "Allocation failed for Q2Hex-arrays, size %lg Mb\n", sizeof(int27) * (Obj->NbrQ2Hex + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ2HexMax = Obj->NbrQ2Hex;
  // GmfGotoKwd(InpMsh,  GmfHexahedraQ2);
  GmfGetBlock(InpMsh, GmfHexahedraQ2, 1, Obj->NbrQ2Hex, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 27, &Obj->Q2Hex[1], &Obj->Q2Hex[Obj->NbrQ2Hex],
      typeOf[sizeof(int1)], &Obj->Q2HexRef[1], &Obj->Q2HexRef[Obj->NbrQ2Hex]);

  /* Reading Q3 hexahedra */
  Obj->NbrQ3Hex = GmfStatKwd(InpMsh, GmfHexahedraQ3);
  Obj->Q3Hex    = viz_reallocate(sizeof(int64) * (Obj->NbrQ3Hex + 1), Obj->Q3Hex);
  Obj->Q3HexRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ3Hex + 1), Obj->Q3HexRef);
  if (!Obj->Q3Hex || !Obj->Q3HexRef) {
    viz_writestatus(status, "Allocation failed for Q3Hex-arrays, size %lg Mb\n", sizeof(int64) * (Obj->NbrQ3Hex + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ3HexMax = Obj->NbrQ3Hex;
  // GmfGotoKwd(InpMsh,  GmfHexahedraQ3);
  GmfGetBlock(InpMsh, GmfHexahedraQ3, 1, Obj->NbrQ3Hex, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 64, &Obj->Q3Hex[1], &Obj->Q3Hex[Obj->NbrQ3Hex],
      typeOf[sizeof(int1)], &Obj->Q3HexRef[1], &Obj->Q3HexRef[Obj->NbrQ3Hex]);

  /* Reading Q4 hexahedra */
  Obj->NbrQ4Hex = GmfStatKwd(InpMsh, GmfHexahedraQ4);
  Obj->Q4Hex    = viz_reallocate(sizeof(int125) * (Obj->NbrQ4Hex + 1), Obj->Q4Hex);
  Obj->Q4HexRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ4Hex + 1), Obj->Q4HexRef);
  if (!Obj->Q4Hex || !Obj->Q4HexRef) {
    viz_writestatus(status, "Allocation failed for Q4Hex-arrays, size %lg Mb\n", sizeof(int125) * (Obj->NbrQ4Hex + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ4HexMax = Obj->NbrQ4Hex;
  // GmfGotoKwd(InpMsh,  GmfHexahedraQ4);
  GmfGetBlock(InpMsh, GmfHexahedraQ4, 1, Obj->NbrQ4Hex, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 125, &Obj->Q4Hex[1], &Obj->Q4Hex[Obj->NbrQ4Hex],
      typeOf[sizeof(int1)], &Obj->Q4HexRef[1], &Obj->Q4HexRef[Obj->NbrQ4Hex]);

  /* Reading P2 Pyramids */
  Obj->NbrP2Pyr = GmfStatKwd(InpMsh, GmfPyramidsP2);
  Obj->P2Pyr    = viz_reallocate(sizeof(int14) * (Obj->NbrP2Pyr + 1), Obj->P2Pyr);
  Obj->P2PyrRef = viz_reallocate(sizeof(int1) * (Obj->NbrP2Pyr + 1), Obj->P2PyrRef);
  if (!Obj->P2Pyr || !Obj->P2PyrRef) {
    viz_writestatus(status, "Allocation failed for P2Pyramid-arrays, size %lg Mb\n", sizeof(int14) * (Obj->NbrP2Pyr + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP2PyrMax = Obj->NbrP2Pyr;
  // GmfGotoKwd(InpMsh,  GmfPyramidsP2);
  GmfGetBlock(InpMsh, GmfPyramidsP2, 1, Obj->NbrP2Pyr, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 14, &Obj->P2Pyr[1], &Obj->P2Pyr[Obj->NbrP2Pyr],
      typeOf[sizeof(int1)], &Obj->P2PyrRef[1], &Obj->P2PyrRef[Obj->NbrP2Pyr]);

  /* Reading P3 Pyramids */
  Obj->NbrP3Pyr = GmfStatKwd(InpMsh, GmfPyramidsP3);
  Obj->P3Pyr    = viz_reallocate(sizeof(int30) * (Obj->NbrP3Pyr + 1), Obj->P3Pyr);
  Obj->P3PyrRef = viz_reallocate(sizeof(int1) * (Obj->NbrP3Pyr + 1), Obj->P3PyrRef);
  if (!Obj->P3Pyr || !Obj->P3PyrRef) {
    viz_writestatus(status, "Allocation failed for P3Pyramid-arrays, size %lg Mb\n", sizeof(int30) * (Obj->NbrP3Pyr + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP3PyrMax = Obj->NbrP3Pyr;
  // GmfGotoKwd(InpMsh,  GmfPyramidsP3);
  GmfGetBlock(InpMsh, GmfPyramidsP3, 1, Obj->NbrP3Pyr, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 30, &Obj->P3Pyr[1], &Obj->P3Pyr[Obj->NbrP3Pyr],
      typeOf[sizeof(int1)], &Obj->P3PyrRef[1], &Obj->P3PyrRef[Obj->NbrP3Pyr]);

  /* Reading P4 Pyramids */
  Obj->NbrP4Pyr = GmfStatKwd(InpMsh, GmfPyramidsP4);
  Obj->P4Pyr    = viz_reallocate(sizeof(int55) * (Obj->NbrP4Pyr + 1), Obj->P4Pyr);
  Obj->P4PyrRef = viz_reallocate(sizeof(int1) * (Obj->NbrP4Pyr + 1), Obj->P4PyrRef);
  if (!Obj->P4Pyr || !Obj->P4PyrRef) {
    viz_writestatus(status, "Allocation failed for P4Pyramid-arrays, size %lg Mb\n", sizeof(int55) * (Obj->NbrP4Pyr + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP4PyrMax = Obj->NbrP4Pyr;
  // GmfGotoKwd(InpMsh,  GmfPyramidsP4);
  GmfGetBlock(InpMsh, GmfPyramidsP4, 1, Obj->NbrP4Pyr, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 55, &Obj->P4Pyr[1], &Obj->P4Pyr[Obj->NbrP4Pyr],
      typeOf[sizeof(int1)], &Obj->P4PyrRef[1], &Obj->P4PyrRef[Obj->NbrP4Pyr]);

  /* Reading P2 Prisms */
  Obj->NbrP2Pri = GmfStatKwd(InpMsh, GmfPrismsP2);
  Obj->P2Pri    = viz_reallocate(sizeof(int18) * (Obj->NbrP2Pri + 1), Obj->P2Pri);
  Obj->P2PriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP2Pri + 1), Obj->P2PriRef);
  if (!Obj->P2Pri || !Obj->P2PriRef) {
    viz_writestatus(status, "Allocation failed for P2Prism-arrays, size %lg Mb\n", sizeof(int18) * (Obj->NbrP2Pri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP2PriMax = Obj->NbrP2Pri;
  // GmfGotoKwd(InpMsh,  GmfPrismsP2);
  GmfGetBlock(InpMsh, GmfPrismsP2, 1, Obj->NbrP2Pri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 18, &Obj->P2Pri[1], &Obj->P2Pri[Obj->NbrP2Pri],
      typeOf[sizeof(int1)], &Obj->P2PriRef[1], &Obj->P2PriRef[Obj->NbrP2Pri]);

  /* Reading P3 Prisms */
  Obj->NbrP3Pri = GmfStatKwd(InpMsh, GmfPrismsP3);
  Obj->P3Pri    = viz_reallocate(sizeof(int40) * (Obj->NbrP3Pri + 1), Obj->P3Pri);
  Obj->P3PriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP3Pri + 1), Obj->P3PriRef);
  if (!Obj->P3Pri || !Obj->P3PriRef) {
    viz_writestatus(status, "Allocation failed for P3Prism-arrays, size %lg Mb\n", sizeof(int40) * (Obj->NbrP3Pri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP3PriMax = Obj->NbrP3Pri;
  // GmfGotoKwd(InpMsh,  GmfPrismsP3);
  GmfGetBlock(InpMsh, GmfPrismsP3, 1, Obj->NbrP3Pri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 40, &Obj->P3Pri[1], &Obj->P3Pri[Obj->NbrP3Pri],
      typeOf[sizeof(int1)], &Obj->P3PriRef[1], &Obj->P3PriRef[Obj->NbrP3Pri]);

  /* Reading P4 Prisms */
  Obj->NbrP4Pri = GmfStatKwd(InpMsh, GmfPrismsP4);
  Obj->P4Pri    = viz_reallocate(sizeof(int75) * (Obj->NbrP4Pri + 1), Obj->P4Pri);
  Obj->P4PriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP4Pri + 1), Obj->P4PriRef);
  if (!Obj->P4Pri || !Obj->P4PriRef) {
    viz_writestatus(status, "Allocation failed for P4Prism-arrays, size %lg Mb\n", sizeof(int75) * (Obj->NbrP4Pri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP4PriMax = Obj->NbrP4Pri;
  // GmfGotoKwd(InpMsh,  GmfPrismsP4);
  GmfGetBlock(InpMsh, GmfPrismsP4, 1, Obj->NbrP4Pri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 75, &Obj->P4Pri[1], &Obj->P4Pri[Obj->NbrP4Pri],
      typeOf[sizeof(int1)], &Obj->P4PriRef[1], &Obj->P4PriRef[Obj->NbrP4Pri]);

  /* Close current mesh */
  GmfCloseMesh(InpMsh);

  return VIZINT_SUCCESS;
}

int1* uti_String2Array(const char* str, int* size)
{
  int        i;
  const char delimiters[] = " ,";

  *size = 0;

  // printf(" string: %s \n",str);
  if (strlen(str) <= 0)
    return NULL;

  char* token = NULL;
  char* sub   = NULL;

  UtiTuple* tuple = uti_NewTuple();

  char* string = strdup(str);
  token        = mystrsep(&string, delimiters);

  while (token != NULL) {
    // printf(" token %s %d\n",token,strlen(token));
    if (strlen(token) > 0) {
      int beg = strtol(token, &sub, 10);
      int end = beg;
      if (sub != NULL) {
        if (sub[0] != '\0' && strlen(sub) >= 1) {
          // printf(" sub %s \n",sub);
          end = strtol(sub + 1, NULL, 10);
        }
      }
      // printf(" Add beg-end %d %d \n",beg,end);
      for (i = beg; i <= end; i++)
        tuple = uti_AddTuple(i, tuple);
    }
    token = mystrsep(&string, delimiters);
  }

  // printf(" end \n");

  int1* array = uti_GetTupleArray(tuple, size);

  tuple = uti_FreeTuple(tuple);
  free(string);

  // for(i=0; i<*size; i++) printf(" %d ",array[i]);
  // printf("\n");

  return array;
}

UtiTuple* uti_NewTuple()
{
  UtiTuple* tup = malloc(sizeof(UtiTuple));
  if (!tup)
    return NULL;
  tup->size = 0;
  tup->nxt  = NULL;
  return tup;
}

/* add en entity and return the tuple in which the object has been added */
UtiTuple* uti_AddTuple(int1 iEnt, UtiTuple* head)
{
  UtiTuple* tup = NULL;
  if (head == NULL) {
    tup                   = uti_NewTuple();
    tup->tuple[tup->size] = iEnt;
    tup->size++;
    tup->nxt = NULL;
    return tup;
  }
  else if (head->size >= UTI_TUPLE_MAX_SIZE) {
    tup                   = uti_NewTuple();
    tup->tuple[tup->size] = iEnt;
    tup->size++;
    tup->nxt = head;
    return tup;
  }
  else {
    head->tuple[head->size] = iEnt;
    head->size++;
    return head;
  }
}

UtiTuple* uti_FreeTuple(UtiTuple* head)
{
  UtiTuple* tup = head;
  UtiTuple* nxt = NULL;
  while (tup != NULL) {
    nxt = tup->nxt;
    free(tup);
    tup = NULL;
    tup = nxt;
  }
  return NULL;
}

int uti_GetTupleSize(UtiTuple* head)
{
  int       size = 0;
  UtiTuple* tup  = head;
  while (tup != NULL) {
    size += tup->size;
    tup = tup->nxt;
  }
  return size;
}

int1* uti_GetTupleArray(UtiTuple* head, int* size)
{
  int i;
  *size = uti_GetTupleSize(head);
  if (*size <= 0)
    return NULL;

  int1*     lst = malloc(sizeof(int1) * (*size));
  UtiTuple* tup = head;
  int       idx = *size - 1;
  while (tup != NULL) {
    for (i = tup->size - 1; i >= 0; i--) {
      lst[idx--] = tup->tuple[i];
    }
    tup = tup->nxt;
  }

  return lst;
}

char* mystrsep(char** stringp, const char* delim)
{
  char* start = *stringp;
  char* p;

  p = (start != NULL) ? strpbrk(start, delim) : NULL;

  if (p == NULL) {
    *stringp = NULL;
  }
  else {
    *p       = '\0';
    *stringp = p + 1;
  }

  return start;
}

int viz_OpenSolution(VizObject* iObj, const char* SolFile, VizIntStatus* status)
{
  int ierr;
  if ((strstr(SolFile, ".cgns") != NULL) || (strstr(SolFile, ".hdf") != NULL))
    return viz_OpenSolutionCGNS(iObj, SolFile, status);
  else {
    //--- redirect error signals
#if defined(__linux__) || defined(__APPLE__)
    signal(SIGILL, readSolErr);
    signal(SIGFPE, readSolErr);
    signal(SIGBUS, readSolErr);
    signal(SIGSEGV, readSolErr);
    signal(SIGQUIT, readSolErr);
#endif

    ierr = viz_OpenSolutionLibMeshb(iObj, SolFile, status);
    //--- reset default error signals
#if defined(__linux__) || defined(__APPLE__)
    signal(SIGILL, SIG_DFL);
    signal(SIGFPE, SIG_DFL);
    signal(SIGBUS, SIG_DFL);
    signal(SIGSEGV, SIG_DFL);
    signal(SIGQUIT, SIG_DFL);
#endif
    return ierr;
  }
}

int viz_OpenSolutionLibMeshb(VizObject* Obj, const char* SolFile, VizIntStatus* status)
{
  char    bufNam[1024], noMshNam[1024];
  int     SolSiz, NbrTyp, TypTab[GmfMaxTyp + 1], Dim, NbrSol, Ite, Deg, NbrNod, NbrNodRef;
  double  Tim;
  float   flt;
  int     FilVer;
  int64_t InpSol;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!SolFile) {
    viz_writestatus(status, "NULL (Solution Name) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status) parameter on input\n");
    return VIZINT_ERROR;
  }

  sprintf(noMshNam, "%s", SolFile);
  if (strstr(SolFile, ".mesh") != NULL)
    noMshNam[strstr(SolFile, ".mesh") - SolFile] = '\0'; // check if it has the extension .mesh[b] and remove it if it is the case

  InpSol = GmfOpenMesh(noMshNam, GmfRead, &FilVer, &Dim);

  if (InpSol <= 0) {
    if (strstr(noMshNam, ".sol") == NULL) {
      sprintf(bufNam, "%s.solb", noMshNam);
      InpSol = GmfOpenMesh(bufNam, GmfRead, &FilVer, &Dim);
      if (InpSol <= 0) {
        sprintf(bufNam, "%s.sol", noMshNam);
        InpSol = GmfOpenMesh(bufNam, GmfRead, &FilVer, &Dim);
        if (InpSol <= 0) {
          viz_writestatus(status, "Cannot open solution file %s.[sol[b]]\n", noMshNam);
          return VIZINT_ERROR;
        }
      }
      viz_writeinfo(status, "%s", bufNam);
    }
    else {
      viz_writestatus(status, "Cannot open solution file %s.[sol[b]]\n", noMshNam);
      return VIZINT_ERROR;
    }
  }
  else
    viz_writeinfo(status, "%s", noMshNam);

  if (InpSol == 0) {
    viz_writestatus(status, "Cannot open solution file %s\n", noMshNam);
    return VIZINT_ERROR;
  }
  if (Dim != Obj->Dim) {
    viz_writestatus(status, "Invalid solution dimension %d should be %d \n", Dim, Obj->Dim);
    return VIZINT_ERROR;
  }

  if (GmfStatKwd(InpSol, GmfTime)) {
    GmfGotoKwd(InpSol, GmfTime);
    if (FilVer == GmfFloat) { // read 32 bits float
      GmfGetLin(InpSol, GmfTime, &flt);
      Tim = (double)flt;
    }
    else if (FilVer == GmfDouble) // read 64 bits float
      GmfGetLin(InpSol, GmfTime, &Tim);
  }
  else
    Tim = 0.;

  if (GmfStatKwd(InpSol, GmfIterations)) {
    GmfGotoKwd(InpSol, GmfIterations);
    GmfGetLin(InpSol, GmfIterations, &Ite);
  }
  else
    Ite = 0;

  // Reading sol at vertices
  NbrSol = GmfStatKwd(InpSol, GmfSolAtVertices, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrVer) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtVertices \n", NbrSol, Obj->NbrVer);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) { //--- by convention solatvertices is of degree 0
    Obj->CrdSol = viz_ReadSolutionLibMeshb(InpSol, Obj->CrdSol, FilVer, GmfSolAtVertices, NbrTyp, SolSiz, TypTab, Obj->NbrVer, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  // Reading sol at Edges
  NbrSol = GmfStatKwd(InpSol, GmfSolAtEdges, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrTet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtEdges \n", NbrSol, Obj->NbrTet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->EdgSol = viz_ReadSolutionLibMeshb(InpSol, Obj->EdgSol, FilVer, GmfSolAtEdges, NbrTyp, SolSiz, TypTab, Obj->NbrEdg, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtEdgesP1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrEdg) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtEdgesP1 \n", NbrSol, Obj->NbrEdg);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->EdgSol = viz_ReadSolutionLibMeshb(InpSol, Obj->EdgSol, FilVer, GmfHOSolAtEdgesP1, NbrTyp, SolSiz, TypTab, Obj->NbrEdg, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtEdgesP1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtEdgesP1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtEdgesP1NodesPositions, Obj->EdgSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtEdgesP2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP2Edg) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtEdgesP2 \n", NbrSol, Obj->NbrP2Edg);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P2EdgSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P2EdgSol, FilVer, GmfHOSolAtEdgesP2, NbrTyp, SolSiz, TypTab, Obj->NbrP2Edg, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtEdgesP2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtEdgesP2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtEdgesP2NodesPositions, Obj->P2EdgSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtEdgesP3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP3Edg) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtEdgesP3 \n", NbrSol, Obj->NbrP3Edg);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P3EdgSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P3EdgSol, FilVer, GmfHOSolAtEdgesP3, NbrTyp, SolSiz, TypTab, Obj->NbrP3Edg, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtEdgesP3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtEdgesP3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtEdgesP3NodesPositions, Obj->P3EdgSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtEdgesP4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP4Edg) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtEdgesP4 \n", NbrSol, Obj->NbrP4Edg);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P4EdgSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P4EdgSol, FilVer, GmfHOSolAtEdgesP4, NbrTyp, SolSiz, TypTab, Obj->NbrP4Edg, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtEdgesP4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtEdgesP4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtEdgesP4NodesPositions, Obj->P4EdgSol);
  }

  // Reading sol at triangles
  NbrSol = GmfStatKwd(InpSol, GmfSolAtTriangles, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrTri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtTriangles \n", NbrSol, Obj->NbrTri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->TriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->TriSol, FilVer, GmfSolAtTriangles, NbrTyp, SolSiz, TypTab, Obj->NbrTri, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrTri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTrianglesP1 \n", NbrSol, Obj->NbrTri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->TriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->TriSol, FilVer, GmfHOSolAtTrianglesP1, NbrTyp, SolSiz, TypTab, Obj->NbrTri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTrianglesP1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTrianglesP1NodesPositions, Obj->TriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP2Tri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTrianglesP2 \n", NbrSol, Obj->NbrP2Tri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P2TriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P2TriSol, FilVer, GmfHOSolAtTrianglesP2, NbrTyp, SolSiz, TypTab, Obj->NbrP2Tri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTrianglesP2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTrianglesP2NodesPositions, Obj->P2TriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP3Tri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTrianglesP3 \n", NbrSol, Obj->NbrP3Tri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P3TriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P3TriSol, FilVer, GmfHOSolAtTrianglesP3, NbrTyp, SolSiz, TypTab, Obj->NbrP3Tri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTrianglesP3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTrianglesP3NodesPositions, Obj->P3TriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP4Tri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTrianglesP4 \n", NbrSol, Obj->NbrP4Tri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P4TriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P4TriSol, FilVer, GmfHOSolAtTrianglesP4, NbrTyp, SolSiz, TypTab, Obj->NbrP4Tri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTrianglesP4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTrianglesP4NodesPositions, Obj->P4TriSol);
  }

  // Reading sol at Quadrilaterals
  NbrSol = GmfStatKwd(InpSol, GmfSolAtQuadrilaterals, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQua) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtQuadrilaterals \n", NbrSol, Obj->NbrQua);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->QuaSol = viz_ReadSolutionLibMeshb(InpSol, Obj->QuaSol, FilVer, GmfSolAtQuadrilaterals, NbrTyp, SolSiz, TypTab, Obj->NbrQua, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQua) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtQuadrilateralsQ1 \n", NbrSol, Obj->NbrQua);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->QuaSol = viz_ReadSolutionLibMeshb(InpSol, Obj->QuaSol, FilVer, GmfHOSolAtQuadrilateralsQ1, NbrTyp, SolSiz, TypTab, Obj->NbrQua, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtQuadrilateralsQ1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtQuadrilateralsQ1NodesPositions, Obj->QuaSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ2Qua) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtQuadrilateralsQ2 \n", NbrSol, Obj->NbrQ2Qua);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q2QuaSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q2QuaSol, FilVer, GmfHOSolAtQuadrilateralsQ2, NbrTyp, SolSiz, TypTab, Obj->NbrQ2Qua, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtQuadrilateralsQ2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtQuadrilateralsQ2NodesPositions, Obj->Q2QuaSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ3Qua) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtQuadrilateralsQ3 \n", NbrSol, Obj->NbrQ3Qua);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q3QuaSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q3QuaSol, FilVer, GmfHOSolAtQuadrilateralsQ3, NbrTyp, SolSiz, TypTab, Obj->NbrQ3Qua, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtQuadrilateralsQ3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtQuadrilateralsQ3NodesPositions, Obj->Q3QuaSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ4Qua) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtQuadrilateralsQ4 \n", NbrSol, Obj->NbrQ4Qua);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q4QuaSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q4QuaSol, FilVer, GmfHOSolAtQuadrilateralsQ4, NbrTyp, SolSiz, TypTab, Obj->NbrQ4Qua, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtQuadrilateralsQ4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtQuadrilateralsQ4NodesPositions, Obj->Q4QuaSol);
  }

  // Reading sol at tetrahedra
  NbrSol = GmfStatKwd(InpSol, GmfSolAtTetrahedra, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrTet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtTetrahedra \n", NbrSol, Obj->NbrTet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->TetSol = viz_ReadSolutionLibMeshb(InpSol, Obj->TetSol, FilVer, GmfSolAtTetrahedra, NbrTyp, SolSiz, TypTab, Obj->NbrTet, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrTet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTetrahedraP1 \n", NbrSol, Obj->NbrTet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->TetSol = viz_ReadSolutionLibMeshb(InpSol, Obj->TetSol, FilVer, GmfHOSolAtTetrahedraP1, NbrTyp, SolSiz, TypTab, Obj->NbrTet, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTetrahedraP1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTetrahedraP1NodesPositions, Obj->TetSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP2Tet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTetrahedraP2 \n", NbrSol, Obj->NbrP2Tet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P2TetSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P2TetSol, FilVer, GmfHOSolAtTetrahedraP2, NbrTyp, SolSiz, TypTab, Obj->NbrP2Tet, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTetrahedraP2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTetrahedraP2NodesPositions, Obj->P2TetSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP3Tet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTetrahedraP3 \n", NbrSol, Obj->NbrP3Tet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P3TetSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P3TetSol, FilVer, GmfHOSolAtTetrahedraP3, NbrTyp, SolSiz, TypTab, Obj->NbrP3Tet, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTetrahedraP3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTetrahedraP3NodesPositions, Obj->P3TetSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP4Tet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTetrahedraP4 \n", NbrSol, Obj->NbrP4Tet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P4TetSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P4TetSol, FilVer, GmfHOSolAtTetrahedraP4, NbrTyp, SolSiz, TypTab, Obj->NbrP4Tet, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTetrahedraP4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTetrahedraP4NodesPositions, Obj->P4TetSol);
  }

  // Reading sol at Pyramids
  NbrSol = GmfStatKwd(InpSol, GmfSolAtPyramids, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrPyr) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtPyramids \n", NbrSol, Obj->NbrPyr);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->PyrSol = viz_ReadSolutionLibMeshb(InpSol, Obj->PyrSol, FilVer, GmfSolAtPyramids, NbrTyp, SolSiz, TypTab, Obj->NbrPyr, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrPyr) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPyramidsP1 \n", NbrSol, Obj->NbrPyr);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->PyrSol = viz_ReadSolutionLibMeshb(InpSol, Obj->PyrSol, FilVer, GmfHOSolAtPyramidsP1, NbrTyp, SolSiz, TypTab, Obj->NbrPyr, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPyramidsP1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPyramidsP1NodesPositions, Obj->PyrSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP2Pyr) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPyramidsP2 \n", NbrSol, Obj->NbrP2Pyr);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P2PyrSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P2PyrSol, FilVer, GmfHOSolAtPyramidsP2, NbrTyp, SolSiz, TypTab, Obj->NbrP2Pyr, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPyramidsP2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPyramidsP2NodesPositions, Obj->P2PyrSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP3Pyr) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPyramidsP3 \n", NbrSol, Obj->NbrP3Pyr);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P3PyrSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P3PyrSol, FilVer, GmfHOSolAtPyramidsP3, NbrTyp, SolSiz, TypTab, Obj->NbrP3Pyr, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPyramidsP3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPyramidsP3NodesPositions, Obj->P3PyrSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP4Pyr) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPyramidsP4 \n", NbrSol, Obj->NbrP4Pyr);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P4PyrSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P4PyrSol, FilVer, GmfHOSolAtPyramidsP4, NbrTyp, SolSiz, TypTab, Obj->NbrP4Pyr, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPyramidsP4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPyramidsP4NodesPositions, Obj->P4PyrSol);
  }

  // Reading sol at Prisms
  NbrSol = GmfStatKwd(InpSol, GmfSolAtPrisms, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrPri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtPrisms \n", NbrSol, Obj->NbrPri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->PriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->PriSol, FilVer, GmfSolAtPrisms, NbrTyp, SolSiz, TypTab, Obj->NbrPri, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPrismsP1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrPri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPrismsP1 \n", NbrSol, Obj->NbrPri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->PriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->PriSol, FilVer, GmfHOSolAtPrismsP1, NbrTyp, SolSiz, TypTab, Obj->NbrPri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPrismsP1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPrismsP1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPrismsP1NodesPositions, Obj->PriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPrismsP2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP2Pri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPrismsP2 \n", NbrSol, Obj->NbrP2Pri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P2PriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P2PriSol, FilVer, GmfHOSolAtPrismsP2, NbrTyp, SolSiz, TypTab, Obj->NbrP2Pri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPrismsP2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPrismsP2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPrismsP2NodesPositions, Obj->P2PriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPrismsP3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP3Pri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPrismsP3 \n", NbrSol, Obj->NbrP3Pri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P3PriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P3PriSol, FilVer, GmfHOSolAtPrismsP3, NbrTyp, SolSiz, TypTab, Obj->NbrP3Pri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPrismsP3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPrismsP3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPrismsP3NodesPositions, Obj->P3PriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPrismsP4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP4Pri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPrismsP4 \n", NbrSol, Obj->NbrP4Pri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P4PriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P4PriSol, FilVer, GmfHOSolAtPrismsP4, NbrTyp, SolSiz, TypTab, Obj->NbrP4Pri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPrismsP4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPrismsP4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPrismsP4NodesPositions, Obj->P4PriSol);
  }

  // Reading sol at Hexahedra
  NbrSol = GmfStatKwd(InpSol, GmfSolAtHexahedra, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrHex) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtHexahedra \n", NbrSol, Obj->NbrHex);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->HexSol = viz_ReadSolutionLibMeshb(InpSol, Obj->HexSol, FilVer, GmfSolAtHexahedra, NbrTyp, SolSiz, TypTab, Obj->NbrHex, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrHex) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtHexahedraQ1 \n", NbrSol, Obj->NbrHex);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->HexSol = viz_ReadSolutionLibMeshb(InpSol, Obj->HexSol, FilVer, GmfHOSolAtHexahedraQ1, NbrTyp, SolSiz, TypTab, Obj->NbrHex, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtHexahedraQ1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtHexahedraQ1NodesPositions, Obj->HexSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ2Hex) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtHexahedraQ2 \n", NbrSol, Obj->NbrQ2Hex);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q2HexSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q2HexSol, FilVer, GmfHOSolAtHexahedraQ2, NbrTyp, SolSiz, TypTab, Obj->NbrQ2Hex, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtHexahedraQ2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtHexahedraQ2NodesPositions, Obj->Q2HexSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ3Hex) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtHexahedraQ3 \n", NbrSol, Obj->NbrQ3Hex);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q3HexSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q3HexSol, FilVer, GmfHOSolAtHexahedraQ3, NbrTyp, SolSiz, TypTab, Obj->NbrQ3Hex, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtHexahedraQ3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtHexahedraQ3NodesPositions, Obj->Q3HexSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ4Hex) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtHexahedraQ4 \n", NbrSol, Obj->NbrQ4Hex);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q4HexSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q4HexSol, FilVer, GmfHOSolAtHexahedraQ4, NbrTyp, SolSiz, TypTab, Obj->NbrQ4Hex, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }

  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtHexahedraQ4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtHexahedraQ4NodesPositions, Obj->Q4HexSol);
  }

  viz_ReadSolutionsStrings(InpSol, Obj);

  // Close current mesh
  GmfCloseMesh(InpSol);

  return VIZINT_SUCCESS;
}

int viz_getIthSolInfo(solution* sol, int ith, int1* nbrEnt, int* SolAtEntTyp, int1* iter, double* tim, int1* deg, int1* nbrNod, double** solp, VizIntStatus* status)
{
  int       i    = 1;
  solution* csol = sol;
  if (!csol) {
    // printf("Invalid request to get a solution\n");
    return 2;
  }

  while (ith != i) {
    if (csol) {
      csol = csol->nxt;
      i++;
    }
    else {
      printf(" warning : unsucessfully attempting to access to solution %d\n", ith);
      break;
    }
  }
  if (csol) {
    *nbrEnt      = csol->NbrSol;
    *SolAtEntTyp = csol->SolTyp;
    *tim         = csol->Tim;
    *iter        = csol->Ite;
    *nbrNod      = csol->NbrNod;
    *deg         = csol->Deg;
    *solp        = csol->Sol;
    return 0;
  }
  else {
    // printf("Invalid request to get a solution\n");
    return 2;
  }
}

double* viz_GetSolutionsArray(VizObject* Obj, int SolTyp, int iSol, int1* nbrEnt, int1* SolAtEntTyp, int1* iter, double* tim, int1* deg, int1* nbrNod, VizIntStatus* status)
{

  *nbrEnt      = 0;
  *SolAtEntTyp = 0;
  *tim         = 0.;
  *deg         = 0;
  *nbrNod      = 0;

  //-- sol info
  double* sol = NULL;

  switch (SolTyp) {
    case VizAny:
      return NULL;
    case VizVerSol:
      viz_getIthSolInfo(Obj->CrdSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizTriSol:
      viz_getIthSolInfo(Obj->TriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQuaSol:
      viz_getIthSolInfo(Obj->QuaSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizTetSol:
      viz_getIthSolInfo(Obj->TetSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizPyrSol:
      viz_getIthSolInfo(Obj->PyrSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizPriSol:
      viz_getIthSolInfo(Obj->PriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizHexSol:
      viz_getIthSolInfo(Obj->HexSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP2TriSol:
      viz_getIthSolInfo(Obj->P2TriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ2QuaSol:
      viz_getIthSolInfo(Obj->Q2QuaSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP2TetSol:
      viz_getIthSolInfo(Obj->P2TetSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP2PyrSol:
      viz_getIthSolInfo(Obj->P2PyrSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP2PriSol:
      viz_getIthSolInfo(Obj->P2PriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ2HexSol:
      viz_getIthSolInfo(Obj->Q2HexSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP3TriSol:
      viz_getIthSolInfo(Obj->P3TriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ3QuaSol:
      viz_getIthSolInfo(Obj->Q3QuaSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP3TetSol:
      viz_getIthSolInfo(Obj->P3TetSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP3PyrSol:
      viz_getIthSolInfo(Obj->P3PyrSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP3PriSol:
      viz_getIthSolInfo(Obj->P3PriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ3HexSol:
      viz_getIthSolInfo(Obj->Q3HexSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP4TriSol:
      viz_getIthSolInfo(Obj->P4TriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ4QuaSol:
      viz_getIthSolInfo(Obj->Q4QuaSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP4TetSol:
      viz_getIthSolInfo(Obj->P4TetSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP4PyrSol:
      viz_getIthSolInfo(Obj->P4PyrSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP4PriSol:
      viz_getIthSolInfo(Obj->P4PriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ4HexSol:
      viz_getIthSolInfo(Obj->Q4HexSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    default:
      printf(" GetSolutionArray is undefined for Typ %d !\n", SolTyp);
      return NULL;
  }
}

int viz_GetNumberOfFields(VizObject* Obj, const int SolTyp)
{
  solution* sol;

  int NbrFld = 0;

  switch (SolTyp) {
    case VizEdgSol:
      if (Obj->EdgSol) {
        sol = Obj->EdgSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP2EdgSol:
      if (Obj->P2EdgSol) {
        sol = Obj->P2EdgSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP3EdgSol:
      if (Obj->P3EdgSol) {
        sol = Obj->P3EdgSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP4EdgSol:
      if (Obj->P4EdgSol) {
        sol = Obj->P4EdgSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizTriSol:
      if (Obj->TriSol) {
        sol = Obj->TriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP2TriSol:
      if (Obj->P2TriSol) {
        sol = Obj->P2TriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP3TriSol:
      if (Obj->P3TriSol) {
        sol = Obj->P3TriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP4TriSol:
      if (Obj->P4TriSol) {
        sol = Obj->P4TriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQuaSol:
      if (Obj->QuaSol) {
        sol = Obj->QuaSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ2QuaSol:
      if (Obj->Q2QuaSol) {
        sol = Obj->Q2QuaSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ3QuaSol:
      if (Obj->Q3QuaSol) {
        sol = Obj->Q3QuaSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ4QuaSol:
      if (Obj->Q4QuaSol) {
        sol = Obj->Q4QuaSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizTetSol:
      if (Obj->TetSol) {
        sol = Obj->TetSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP2TetSol:
      if (Obj->P2TetSol) {
        sol = Obj->P2TetSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP3TetSol:
      if (Obj->P3TetSol) {
        sol = Obj->P3TetSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP4TetSol:
      if (Obj->P4TetSol) {
        sol = Obj->P4TetSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizPyrSol:
      if (Obj->PyrSol) {
        sol = Obj->PyrSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP2PyrSol:
      if (Obj->P2PyrSol) {
        sol = Obj->P2PyrSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP3PyrSol:
      if (Obj->P3PyrSol) {
        sol = Obj->P3PyrSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP4PyrSol:
      if (Obj->P4PyrSol) {
        sol = Obj->P4PyrSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizPriSol:
      if (Obj->PriSol) {
        sol = Obj->PriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP2PriSol:
      if (Obj->P2PriSol) {
        sol = Obj->P2PriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP3PriSol:
      if (Obj->P3PriSol) {
        sol = Obj->P3PriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP4PriSol:
      if (Obj->P4PriSol) {
        sol = Obj->P4PriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizHexSol:
      if (Obj->HexSol) {
        sol = Obj->HexSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ2HexSol:
      if (Obj->Q2HexSol) {
        sol = Obj->Q2HexSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ3HexSol:
      if (Obj->Q3HexSol) {
        sol = Obj->Q3HexSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ4HexSol:
      if (Obj->Q4HexSol) {
        sol = Obj->Q4HexSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    default:
      printf("Warining: viz_GetNumberOfFields: case not considered");
      break;
  }

  return NbrFld;
}

int viz_PrintMeshInfo(VizObject* Obj, VizIntStatus* status)
{
  double bbox[6];
  int1   nbrEnt;
  int    dim;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  if (viz_GetDimension(Obj, &dim, status) == VIZINT_SUCCESS)
    printf("  Dimension %d  \n", dim);
  if (viz_GetNumberOfVertices(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Vertices           %12d \n", nbrEnt);
  if (viz_GetNumberOfCorners(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Corners            %12d \n", nbrEnt);
  if (viz_GetNumberOfEdges(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Edges              %12d \n", nbrEnt);
  if (viz_GetNumberOfP2Edges(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P2-Edges           %12d \n", nbrEnt);
  if (viz_GetNumberOfP3Edges(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P3-Edges           %12d \n", nbrEnt);
  if (viz_GetNumberOfP4Edges(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P4-Edges           %12d \n", nbrEnt);
  if (viz_GetNumberOfTriangles(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Triangles          %12d \n", nbrEnt);
  if (viz_GetNumberOfP2Triangles(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P2-Triangles       %12d \n", nbrEnt);
  if (viz_GetNumberOfP3Triangles(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P3-Triangles       %12d \n", nbrEnt);
  if (viz_GetNumberOfP4Triangles(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P4-Triangles       %12d \n", nbrEnt);
  if (viz_GetNumberOfQuadrilaterals(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Quadrilaterals     %12d \n", nbrEnt);
  if (viz_GetNumberOfQ2Quadrilaterals(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q2-Quadrilaterals  %12d \n", nbrEnt);
  if (viz_GetNumberOfQ3Quadrilaterals(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q3-Quadrilaterals  %12d \n", nbrEnt);
  if (viz_GetNumberOfQ4Quadrilaterals(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q4-Quadrilaterals  %12d \n", nbrEnt);
  if (viz_GetNumberOfTetrahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Tetrahedra         %12d \n", nbrEnt);
  if (viz_GetNumberOfP2Tetrahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P2-Tetrahedra      %12d \n", nbrEnt);
  if (viz_GetNumberOfP3Tetrahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P3-Tetrahedra      %12d \n", nbrEnt);
  if (viz_GetNumberOfP4Tetrahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P4-Tetrahedra      %12d \n", nbrEnt);
  if (viz_GetNumberOfPrisms(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Prisms             %12d \n", nbrEnt);
  if (viz_GetNumberOfP2Prisms(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P2-Prisms          %12d \n", nbrEnt);
  if (viz_GetNumberOfP3Prisms(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P3-Prisms          %12d \n", nbrEnt);
  if (viz_GetNumberOfP4Prisms(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P4-Prisms          %12d \n", nbrEnt);
  if (viz_GetNumberOfHexahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Hexahedra          %12d \n", nbrEnt);
  if (viz_GetNumberOfQ2Hexahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q2-Hexahedra       %12d \n", nbrEnt);
  if (viz_GetNumberOfQ3Hexahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q3-Hexahedra       %12d \n", nbrEnt);
  if (viz_GetNumberOfQ4Hexahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q4-Hexahedra       %12d \n", nbrEnt);
  if (viz_GetNumberOfPyramids(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Pyramids           %12d \n", nbrEnt);
  if (viz_GetNumberOfP2Pyramids(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P2-Pyramids        %12d \n", nbrEnt);
  if (viz_GetNumberOfP3Pyramids(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P3-Pyramids        %12d \n", nbrEnt);
  if (viz_GetNumberOfP4Pyramids(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P4-Pyramids        %12d \n", nbrEnt);
  if (viz_GetMeshBoundingBox(Obj, bbox, status) == VIZINT_SUCCESS)
    printf("  x:[%lg %lg] y:[%lg %lg] z:[%lg %lg]\n", bbox[0], bbox[1], bbox[2], bbox[3], bbox[4], bbox[5]);

  printf("\n");
  return VIZINT_SUCCESS;
}

int viz_PrintSolutionInfo(VizObject* Obj, VizIntStatus* status)
{
  solution* sol;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  if (Obj->EdgSol) {
    sol = Obj->EdgSol;
    printf("  Degree of HOSolAtEdgesP1          %7d \n", sol->Deg);
  }

  if (Obj->P2EdgSol) {
    sol = Obj->P2EdgSol;
    printf("  Degree of HOSolAtEdgesP2          %7d \n", sol->Deg);
  }

  if (Obj->P3EdgSol) {
    sol = Obj->P3EdgSol;
    printf("  Degree of HOSolAtEdgesP3          %7d \n", sol->Deg);
  }

  if (Obj->P4EdgSol) {
    sol = Obj->P4EdgSol;
    printf("  Degree of HOSolAtEdgesP4          %7d \n", sol->Deg);
  }

  if (Obj->TriSol) {
    sol = Obj->TriSol;
    printf("  Degree of HOSolAtTrianglesP1      %7d \n", sol->Deg);
  }

  if (Obj->P2TriSol) {
    sol = Obj->P2TriSol;
    printf("  Degree of HOSolAtTrianglesP2      %7d \n", sol->Deg);
  }

  if (Obj->P3TriSol) {
    sol = Obj->P3TriSol;
    printf("  Degree of HOSolAtTrianglesP3      %7d \n", sol->Deg);
  }

  if (Obj->P4TriSol) {
    sol = Obj->P4TriSol;
    printf("  Degree of HOSolAtTrianglesP4      %7d \n", sol->Deg);
  }

  if (Obj->QuaSol) {
    sol = Obj->QuaSol;
    printf("  Degree of HOSolAtQuadrilateralsQ1 %7d \n", sol->Deg);
  }

  if (Obj->Q2QuaSol) {
    sol = Obj->Q2QuaSol;
    printf("  Degree of HOSolAtQuadrilateralsQ2 %7d \n", sol->Deg);
  }

  if (Obj->Q3QuaSol) {
    sol = Obj->Q3QuaSol;
    printf("  Degree of HOSolAtQuadrilateralsQ3 %7d \n", sol->Deg);
  }

  if (Obj->Q4QuaSol) {
    sol = Obj->Q4QuaSol;
    printf("  Degree of HOSolAtQuadrilateralsQ4 %7d \n", sol->Deg);
  }

  if (Obj->TetSol) {
    sol = Obj->TetSol;
    printf("  Degree of HOSolAtTetrahedraP1     %7d \n", sol->Deg);
  }

  if (Obj->P2TetSol) {
    sol = Obj->P2TetSol;
    printf("  Degree of HOSolAtTetrahedraP2     %7d \n", sol->Deg);
  }

  if (Obj->P3TetSol) {
    sol = Obj->P3TetSol;
    printf("  Degree of HOSolAtTetrahedraP3     %7d \n", sol->Deg);
  }

  if (Obj->P4TetSol) {
    sol = Obj->P4TetSol;
    printf("  Degree of HOSolAtTetrahedraP4     %7d \n", sol->Deg);
  }

  if (Obj->PyrSol) {
    sol = Obj->PyrSol;
    printf("  Degree of HOSolAtPyramidsP1       %7d \n", sol->Deg);
  }

  if (Obj->P2PyrSol) {
    sol = Obj->P2PyrSol;
    printf("  Degree of HOSolAtPyramidsP2       %7d \n", sol->Deg);
  }

  if (Obj->P3PyrSol) {
    sol = Obj->P3PyrSol;
    printf("  Degree of HOSolAtPyramidsP3       %7d \n", sol->Deg);
  }

  if (Obj->P4PyrSol) {
    sol = Obj->P4PyrSol;
    printf("  Degree of HOSolAtPyramidsP4       %7d \n", sol->Deg);
  }

  if (Obj->PriSol) {
    sol = Obj->PriSol;
    printf("  Degree of HOSolAtPrismsP1         %7d \n", sol->Deg);
  }

  if (Obj->P2PriSol) {
    sol = Obj->P2PriSol;
    printf("  Degree of HOSolAtPrismsP2         %7d \n", sol->Deg);
  }

  if (Obj->P3PriSol) {
    sol = Obj->P3PriSol;
    printf("  Degree of HOSolAtPrismsP3         %7d \n", sol->Deg);
  }

  if (Obj->P4PriSol) {
    sol = Obj->P4PriSol;
    printf("  Degree of HOSolAtPrismsP4         %7d \n", sol->Deg);
  }

  if (Obj->HexSol) {
    sol = Obj->HexSol;
    printf("  Degree of HOSolAtHexahedraQ1      %7d \n", sol->Deg);
  }

  if (Obj->Q2HexSol) {
    sol = Obj->Q2HexSol;
    printf("  Degree of HOSolAtHexahedraQ2      %7d \n", sol->Deg);
  }

  if (Obj->Q3HexSol) {
    sol = Obj->Q3HexSol;
    printf("  Degree of HOSolAtHexahedraQ3      %7d \n", sol->Deg);
  }

  if (Obj->Q4HexSol) {
    sol = Obj->Q4HexSol;
    printf("  Degree of HOSolAtHexahedraQ4      %7d \n", sol->Deg);
  }

  printf("\n");
  return VIZINT_SUCCESS;
}

int viz_GetDimension(VizObject* Obj, int1* dim, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!dim) {
    viz_writestatus(status, "NULL (dim)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *dim = Obj->Dim;
  return VIZINT_SUCCESS;
}

int viz_GetMeshBoundingBox(VizObject* Obj, double* bbox, VizIntStatus* status)
{
  int iVer;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!bbox) {
    viz_writestatus(status, "NULL (bbox)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  if (Obj->NbrVer <= 0) {
    bbox[0] = bbox[1] = 0.0;
    bbox[2] = bbox[3] = 0.0;
    bbox[4] = bbox[5] = 0.0;
    return VIZINT_SUCCESS;
  }

  if (Obj->Crd) {
    bbox[0] = bbox[1] = Obj->Crd[1][0];
    bbox[2] = bbox[3] = Obj->Crd[1][1];
    bbox[4] = bbox[5] = Obj->Crd[1][2];

    for (iVer = 2; iVer <= Obj->NbrVer; iVer++) {
      bbox[0] = fmin(bbox[0], Obj->Crd[iVer][0]);
      bbox[1] = fmax(bbox[1], Obj->Crd[iVer][0]);
      bbox[2] = fmin(bbox[2], Obj->Crd[iVer][1]);
      bbox[3] = fmax(bbox[3], Obj->Crd[iVer][1]);
      bbox[4] = fmin(bbox[4], Obj->Crd[iVer][2]);
      bbox[5] = fmax(bbox[5], Obj->Crd[iVer][2]);
    }
  }
  else if (Obj->Crd_f) {
    bbox[0] = bbox[1] = Obj->Crd_f[1][0];
    bbox[2] = bbox[3] = Obj->Crd_f[1][1];
    bbox[4] = bbox[5] = Obj->Crd_f[1][2];

    for (iVer = 2; iVer <= Obj->NbrVer; iVer++) {
      bbox[0] = fmin(bbox[0], Obj->Crd_f[iVer][0]);
      bbox[1] = fmax(bbox[1], Obj->Crd_f[iVer][0]);
      bbox[2] = fmin(bbox[2], Obj->Crd_f[iVer][1]);
      bbox[3] = fmax(bbox[3], Obj->Crd_f[iVer][1]);
      bbox[4] = fmin(bbox[4], Obj->Crd_f[iVer][2]);
      bbox[5] = fmax(bbox[5], Obj->Crd_f[iVer][2]);
    }
  }

  return VIZINT_SUCCESS;
}

int viz_GetNumberOfVertices(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrVer;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfCorners(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrCrn;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfNormals(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrNor;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfEdges(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrEdg;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP2Edges(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP2Edg;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP3Edges(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP3Edg;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP4Edges(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP4Edg;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfTriangles(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrTri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP2Triangles(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP2Tri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP3Triangles(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP3Tri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP4Triangles(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP4Tri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQuadrilaterals(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQua;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ2Quadrilaterals(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ2Qua;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ3Quadrilaterals(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ3Qua;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ4Quadrilaterals(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ4Qua;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfTetrahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrTet;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP2Tetrahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP2Tet;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP3Tetrahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP3Tet;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP4Tetrahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP4Tet;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfPrisms(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrPri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP2Prisms(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP2Pri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP3Prisms(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP3Pri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP4Prisms(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP4Pri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfHexahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrHex;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ2Hexahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ2Hex;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ3Hexahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ3Hex;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ4Hexahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ4Hex;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfPyramids(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrPyr;
  return VIZINT_SUCCESS;
}

int viz_GetNumberOfP2Pyramids(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP2Pyr;
  return VIZINT_SUCCESS;
}

int viz_GetNumberOfP3Pyramids(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP3Pyr;
  return VIZINT_SUCCESS;
}

int viz_GetNumberOfP4Pyramids(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP4Pyr;
  return VIZINT_SUCCESS;
}

int viz_GetNumberOfEntities(VizObject* Obj, int Typ, int1* nbrEnt, VizIntStatus* status)
{
  *nbrEnt = 0;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  switch (Typ) {
    case VizAny:
      return VIZINT_SUCCESS;
    case VizVer:
      return viz_GetNumberOfVertices(Obj, nbrEnt, status);
    case VizCrn:
      return viz_GetNumberOfCorners(Obj, nbrEnt, status);
    case VizEdg:
      return viz_GetNumberOfEdges(Obj, nbrEnt, status);
    case VizTri:
      return viz_GetNumberOfTriangles(Obj, nbrEnt, status);
    case VizQua:
      return viz_GetNumberOfQuadrilaterals(Obj, nbrEnt, status);
    case VizTet:
      return viz_GetNumberOfTetrahedra(Obj, nbrEnt, status);
    case VizPyr:
      return viz_GetNumberOfPyramids(Obj, nbrEnt, status);
    case VizPri:
      return viz_GetNumberOfPrisms(Obj, nbrEnt, status);
    case VizHex:
      return viz_GetNumberOfHexahedra(Obj, nbrEnt, status);
    case VizP2Edg:
      return viz_GetNumberOfP2Edges(Obj, nbrEnt, status);
    case VizP3Edg:
      return viz_GetNumberOfP3Edges(Obj, nbrEnt, status);
    case VizP4Edg:
      return viz_GetNumberOfP4Edges(Obj, nbrEnt, status);
    case VizP2Tri:
      return viz_GetNumberOfP2Triangles(Obj, nbrEnt, status);
    case VizP3Tri:
      return viz_GetNumberOfP3Triangles(Obj, nbrEnt, status);
    case VizP4Tri:
      return viz_GetNumberOfP4Triangles(Obj, nbrEnt, status);
    case VizQ2Qua:
      return viz_GetNumberOfQ2Quadrilaterals(Obj, nbrEnt, status);
    case VizQ3Qua:
      return viz_GetNumberOfQ3Quadrilaterals(Obj, nbrEnt, status);
    case VizQ4Qua:
      return viz_GetNumberOfQ4Quadrilaterals(Obj, nbrEnt, status);
    case VizP2Tet:
      return viz_GetNumberOfP2Tetrahedra(Obj, nbrEnt, status);
    case VizP3Tet:
      return viz_GetNumberOfP3Tetrahedra(Obj, nbrEnt, status);
    case VizP4Tet:
      return viz_GetNumberOfP4Tetrahedra(Obj, nbrEnt, status);
    case VizQ2Hex:
      return viz_GetNumberOfQ2Hexahedra(Obj, nbrEnt, status);
    case VizQ3Hex:
      return viz_GetNumberOfQ3Hexahedra(Obj, nbrEnt, status);
    case VizQ4Hex:
      return viz_GetNumberOfQ4Hexahedra(Obj, nbrEnt, status);
    case VizP2Pyr:
      return viz_GetNumberOfP2Pyramids(Obj, nbrEnt, status);
    case VizP3Pyr:
      return viz_GetNumberOfP3Pyramids(Obj, nbrEnt, status);
    case VizP4Pyr:
      return viz_GetNumberOfP4Pyramids(Obj, nbrEnt, status);
    case VizP2Pri:
      return viz_GetNumberOfP2Prisms(Obj, nbrEnt, status);
    case VizP3Pri:
      return viz_GetNumberOfP3Prisms(Obj, nbrEnt, status);
    case VizP4Pri:
      return viz_GetNumberOfP4Prisms(Obj, nbrEnt, status);
    default:
      viz_writestatus(status, "viz_GetNumberOfEntities is undefined for Typ %d !\n", Typ);
      return VIZINT_ERROR;
  }
}

int1* viz_GetReferenceArray(VizObject* Obj, int Typ, VizIntStatus* status)
{

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return NULL;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return NULL;
  }

  switch (Typ) {
    case VizAny:
      return NULL;
    case VizVer:
      return Obj->CrdRef;
    case VizCrn:
      return Obj->CrnRef;
    case VizEdg:
      return Obj->EdgRef;
    case VizTri:
      return Obj->TriRef;
    case VizQua:
      return Obj->QuaRef;
    case VizTet:
      return Obj->TetRef;
    case VizPyr:
      return Obj->PyrRef;
    case VizPri:
      return Obj->PriRef;
    case VizHex:
      return Obj->HexRef;
    case VizP2Edg:
      return Obj->P2EdgRef;
    case VizP3Edg:
      return Obj->P3EdgRef;
    case VizP4Edg:
      return Obj->P4EdgRef;
    case VizP2Tri:
      return Obj->P2TriRef;
    case VizP3Tri:
      return Obj->P3TriRef;
    case VizP4Tri:
      return Obj->P4TriRef;
    case VizQ2Qua:
      return Obj->Q2QuaRef;
    case VizQ3Qua:
      return Obj->Q3QuaRef;
    case VizQ4Qua:
      return Obj->Q4QuaRef;
    case VizP2Tet:
      return Obj->P2TetRef;
    case VizP3Tet:
      return Obj->P3TetRef;
    case VizP4Tet:
      return Obj->P4TetRef;
    case VizQ2Hex:
      return Obj->Q2HexRef;
    case VizQ3Hex:
      return Obj->Q3HexRef;
    case VizQ4Hex:
      return Obj->Q4HexRef;
    case VizP2Pyr:
      return Obj->P2PyrRef;
    case VizP3Pyr:
      return Obj->P3PyrRef;
    case VizP4Pyr:
      return Obj->P4PyrRef;
    case VizP2Pri:
      return Obj->P2PriRef;
    case VizP3Pri:
      return Obj->P3PriRef;
    case VizP4Pri:
      return Obj->P4PriRef;
    default:
      viz_writestatus(status, " viz_GetReferenceArray is undefined for Typ %d !\n", Typ);
      return NULL;
  }
}

int1* viz_GetIndicesArray(VizObject* Obj, int Typ, int1* nbrEnt, int1* sizEnt, VizIntStatus* status)
{

  *nbrEnt = 0;
  *sizEnt = 0;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return NULL;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return NULL;
  }

  switch (Typ) {
    case VizAny:
      return NULL;
    case VizVer:
      *nbrEnt = Obj->NbrVer;
      *sizEnt = 1;
      // int1* index = NULL;
      // int1* index = malloc((Obj->NbrVer + 1) * sizeof(int));
      // for (int i = 1; i <= Obj->NbrVer; i++) {
      //   index[i] = i;
      //   printf("index[%d] %d \n", i, index[i]);
      // }
      return (Obj->Crd) ? (int1*)Obj->Crd : (int1*)Obj->Crd_f;
    case VizCrn:
      *nbrEnt = Obj->NbrCrn;
      *sizEnt = 1;
      return (int1*)Obj->Crn;
    case VizNor:
      *nbrEnt = Obj->NbrNor;
      *sizEnt = 1;
      return (Obj->Nor_f) ? (int1*)Obj->Nor_f : (int1*)NULL;
    case VizEdg:
      *nbrEnt = Obj->NbrEdg;
      *sizEnt = 2;
      return (int1*)Obj->Edg;
    case VizTri:
      *nbrEnt = Obj->NbrTri;
      *sizEnt = 3;
      return (int1*)Obj->Tri;
    case VizQua:
      *nbrEnt = Obj->NbrQua;
      *sizEnt = 4;
      return (int1*)Obj->Qua;
    case VizTet:
      *nbrEnt = Obj->NbrTet;
      *sizEnt = 4;
      return (int1*)Obj->Tet;
    case VizPyr:
      *nbrEnt = Obj->NbrPyr;
      *sizEnt = 5;
      return (int1*)Obj->Pyr;
    case VizPri:
      *nbrEnt = Obj->NbrPri;
      *sizEnt = 6;
      return (int1*)Obj->Pri;
    case VizHex:
      *nbrEnt = Obj->NbrHex;
      *sizEnt = 8;
      return (int1*)Obj->Hex;
    case VizP2Edg:
      *nbrEnt = Obj->NbrP2Edg;
      *sizEnt = 3;
      return (int1*)Obj->P2Edg;
    case VizP3Edg:
      *nbrEnt = Obj->NbrP3Edg;
      *sizEnt = 4;
      return (int1*)Obj->P3Edg;
    case VizP4Edg:
      *nbrEnt = Obj->NbrP4Edg;
      *sizEnt = 5;
      return (int1*)Obj->P4Edg;
    case VizP2Tri:
      *nbrEnt = Obj->NbrP2Tri;
      *sizEnt = 6;
      return (int1*)Obj->P2Tri;
    case VizP3Tri:
      *nbrEnt = Obj->NbrP3Tri;
      *sizEnt = 10;
      return (int1*)Obj->P3Tri;
    case VizP4Tri:
      *nbrEnt = Obj->NbrP4Tri;
      *sizEnt = 15;
      return (int1*)Obj->P4Tri;
    case VizQ2Qua:
      *nbrEnt = Obj->NbrQ2Qua;
      *sizEnt = 9;
      return (int1*)Obj->Q2Qua;
    case VizQ3Qua:
      *nbrEnt = Obj->NbrQ3Qua;
      *sizEnt = 16;
      return (int1*)Obj->Q3Qua;
    case VizQ4Qua:
      *nbrEnt = Obj->NbrQ4Qua;
      *sizEnt = 25;
      return (int1*)Obj->Q4Qua;
    case VizP2Tet:
      *nbrEnt = Obj->NbrP2Tet;
      *sizEnt = 10;
      return (int1*)Obj->P2Tet;
    case VizP3Tet:
      *nbrEnt = Obj->NbrP3Tet;
      *sizEnt = 20;
      return (int1*)Obj->P3Tet;
    case VizP4Tet:
      *nbrEnt = Obj->NbrP4Tet;
      *sizEnt = 35;
      return (int1*)Obj->P4Tet;
    case VizQ2Hex:
      *nbrEnt = Obj->NbrQ2Hex;
      *sizEnt = 27;
      return (int1*)Obj->Q2Hex;
    case VizQ3Hex:
      *nbrEnt = Obj->NbrQ3Hex;
      *sizEnt = 64;
      return (int1*)Obj->Q3Hex;
    case VizQ4Hex:
      *nbrEnt = Obj->NbrQ4Hex;
      *sizEnt = 125;
      return (int1*)Obj->Q4Hex;
    case VizP2Pyr:
      *nbrEnt = Obj->NbrP2Pyr;
      *sizEnt = 14;
      return (int1*)Obj->P2Pyr;
    case VizP3Pyr:
      *nbrEnt = Obj->NbrP3Pyr;
      *sizEnt = 30;
      return (int1*)Obj->P3Pyr;
    case VizP4Pyr:
      *nbrEnt = Obj->NbrP4Pyr;
      *sizEnt = 55;
      return (int1*)Obj->P4Pyr;
    case VizP2Pri:
      *nbrEnt = Obj->NbrP2Pri;
      *sizEnt = 18;
      return (int1*)Obj->P2Pri;
    case VizP3Pri:
      *nbrEnt = Obj->NbrP3Pri;
      *sizEnt = 40;
      return (int1*)Obj->P3Pri;
    case VizP4Pri:
      *nbrEnt = Obj->NbrP4Pri;
      *sizEnt = 75;
      return (int1*)Obj->P4Pri;
    default:
      viz_writestatus(status, " viz_GetIndicesArray is undefined for Typ %d !\n", Typ);
      return NULL;
  }
}

int viz_WriteMesh(VizObject* iObj, const char* MshFile, VizIntStatus* status)
{
  return viz_WriteMeshLibMeshb(iObj, MshFile, status);
}

int viz_WriteMeshLibMeshb(VizObject* Obj, const char* MshFile, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!MshFile) {
    viz_writestatus(status, "NULL (MshFile)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  int FilVer = 3;
  if (sizeof(int1) == 8)
    FilVer = 4;

  int64_t OutMsh = GmfOpenMesh(MshFile, GmfWrite, FilVer, Obj->Dim);
  if (OutMsh == 0) {
    viz_writestatus(status, "Cannot open mesh file %s\n", MshFile);
    return VIZINT_ERROR;
  }
  else
    viz_writeinfo(status, "%s", MshFile);

  // set vertices
  GmfSetKwd(OutMsh, GmfVertices, Obj->NbrVer);
  GmfSetBlock(OutMsh, GmfVertices, 1, Obj->NbrVer, 0, NULL, NULL,
      GmfDouble, &(Obj->Crd[1][0]), &(Obj->Crd[Obj->NbrVer][0]),
      GmfDouble, &(Obj->Crd[1][1]), &(Obj->Crd[Obj->NbrVer][1]),
      GmfDouble, &(Obj->Crd[1][2]), &(Obj->Crd[Obj->NbrVer][2]),
      typeOf[sizeof(int1)], &Obj->CrdRef[1], &Obj->CrdRef[Obj->NbrVer]);

  // set edges
  if (Obj->NbrEdg) {
    GmfSetKwd(OutMsh, GmfEdges, Obj->NbrEdg);
    GmfSetBlock(OutMsh, GmfEdges, 1, Obj->NbrEdg, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &Obj->Edg[1], &Obj->Edg[Obj->NbrEdg],
        typeOf[sizeof(int1)], &Obj->EdgRef[1], &Obj->EdgRef[Obj->NbrEdg]);

    GmfSetKwd(OutMsh, GmfEdgesP1Ordering, 2);
    GmfSetBlock(OutMsh, GmfEdgesP1Ordering, 1, 2, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &P1EdgesOrdering[0], &P1EdgesOrdering[1]);
  }

  if (Obj->NbrP2Edg) {
    GmfSetKwd(OutMsh, GmfEdgesP2, Obj->NbrP2Edg);
    GmfSetBlock(OutMsh, GmfEdgesP2, 1, Obj->NbrP2Edg, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Obj->P2Edg[1], &Obj->P2Edg[Obj->NbrP2Edg],
        typeOf[sizeof(int1)], &Obj->P2EdgRef[1], &Obj->P2EdgRef[Obj->NbrP2Edg]);

    GmfSetKwd(OutMsh, GmfEdgesP2Ordering, 3);
    GmfSetBlock(OutMsh, GmfEdgesP2Ordering, 1, 3, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &P2EdgesOrdering[0], &P2EdgesOrdering[2]);
  }

  if (Obj->NbrP3Edg) {
    GmfSetKwd(OutMsh, GmfEdgesP3, Obj->NbrP3Edg);
    GmfSetBlock(OutMsh, GmfEdgesP3, 1, Obj->NbrP3Edg, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &Obj->P3Edg[1], &Obj->P3Edg[Obj->NbrP3Edg],
        typeOf[sizeof(int1)], &Obj->P3EdgRef[1], &Obj->P3EdgRef[Obj->NbrP3Edg]);

    GmfSetKwd(OutMsh, GmfEdgesP3Ordering, 4);
    GmfSetBlock(OutMsh, GmfEdgesP3Ordering, 1, 4, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &P3EdgesOrdering[0], &P3EdgesOrdering[3]);
  }

  if (Obj->NbrP4Edg) {
    GmfSetKwd(OutMsh, GmfEdgesP4, Obj->NbrP4Edg);
    GmfSetBlock(OutMsh, GmfEdgesP4, 1, Obj->NbrP4Edg, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 5, &Obj->P4Edg[1], &Obj->P4Edg[Obj->NbrP4Edg],
        typeOf[sizeof(int1)], &Obj->P4EdgRef[1], &Obj->P4EdgRef[Obj->NbrP4Edg]);

    GmfSetKwd(OutMsh, GmfEdgesP4Ordering, 5);
    GmfSetBlock(OutMsh, GmfEdgesP4Ordering, 1, 5, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &P4EdgesOrdering[0], &P4EdgesOrdering[4]);
  }

  // set triangles
  if (Obj->NbrTri) {
    GmfSetKwd(OutMsh, GmfTriangles, Obj->NbrTri);
    GmfSetBlock(OutMsh, GmfTriangles, 1, Obj->NbrTri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Obj->Tri[1], &Obj->Tri[Obj->NbrTri],
        typeOf[sizeof(int1)], &Obj->TriRef[1], &Obj->TriRef[Obj->NbrTri]);

    GmfSetKwd(OutMsh, GmfTrianglesP1Ordering, 3);
    GmfSetBlock(OutMsh, GmfTrianglesP1Ordering, 1, 3, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P1TrianglesOrdering[0], &P1TrianglesOrdering[2]);
  }

  if (Obj->NbrP2Tri) {
    GmfSetKwd(OutMsh, GmfTrianglesP2, Obj->NbrP2Tri);
    GmfSetBlock(OutMsh, GmfTrianglesP2, 1, Obj->NbrP2Tri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 6, &Obj->P2Tri[1], &Obj->P2Tri[Obj->NbrP2Tri],
        typeOf[sizeof(int1)], &Obj->P2TriRef[1], &Obj->P2TriRef[Obj->NbrP2Tri]);

    GmfSetKwd(OutMsh, GmfTrianglesP2Ordering, 6);
    GmfSetBlock(OutMsh, GmfTrianglesP2Ordering, 1, 6, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P2TrianglesOrdering[0], &P2TrianglesOrdering[5]);
  }

  if (Obj->NbrP3Tri) {
    GmfSetKwd(OutMsh, GmfTrianglesP3, Obj->NbrP3Tri);
    GmfSetBlock(OutMsh, GmfTrianglesP3, 1, Obj->NbrP3Tri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 10, &Obj->P3Tri[1], &Obj->P3Tri[Obj->NbrP3Tri],
        typeOf[sizeof(int1)], &Obj->P3TriRef[1], &Obj->P3TriRef[Obj->NbrP3Tri]);

    GmfSetKwd(OutMsh, GmfTrianglesP3Ordering, 10);
    GmfSetBlock(OutMsh, GmfTrianglesP3Ordering, 1, 10, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P3TrianglesOrdering[0], &P3TrianglesOrdering[9]);
  }

  if (Obj->NbrP4Tri) {
    GmfSetKwd(OutMsh, GmfTrianglesP4, Obj->NbrP4Tri);
    GmfSetBlock(OutMsh, GmfTrianglesP4, 1, Obj->NbrP4Tri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 15, &Obj->P4Tri[1], &Obj->P4Tri[Obj->NbrP4Tri],
        typeOf[sizeof(int1)], &Obj->P4TriRef[1], &Obj->P4TriRef[Obj->NbrP4Tri]);

    GmfSetKwd(OutMsh, GmfTrianglesP4Ordering, 15);
    GmfSetBlock(OutMsh, GmfTrianglesP4Ordering, 1, 15, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P4TrianglesOrdering[0], &P4TrianglesOrdering[14]);
  }

  // set quads
  if (Obj->NbrQua) {
    GmfSetKwd(OutMsh, GmfQuadrilaterals, Obj->NbrQua);
    GmfSetBlock(OutMsh, GmfQuadrilaterals, 1, Obj->NbrQua, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &Obj->Qua[1], &Obj->Qua[Obj->NbrQua],
        typeOf[sizeof(int1)], &Obj->QuaRef[1], &Obj->QuaRef[Obj->NbrQua]);

    GmfSetKwd(OutMsh, GmfQuadrilateralsQ1Ordering, 4);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ1Ordering, 1, 4, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &Q1QuadrilateralsOrdering[0], &Q1QuadrilateralsOrdering[3]);
  }

  if (Obj->NbrQ2Qua) {
    GmfSetKwd(OutMsh, GmfQuadrilateralsQ2, Obj->NbrQ2Qua);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ2, 1, Obj->NbrQ2Qua, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 9, &Obj->Q2Qua[1], &Obj->Q2Qua[Obj->NbrQ2Qua],
        typeOf[sizeof(int1)], &Obj->Q2QuaRef[1], &Obj->Q2QuaRef[Obj->NbrQ2Qua]);

    GmfSetKwd(OutMsh, GmfQuadrilateralsQ2Ordering, 9);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ2Ordering, 1, 9, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &Q2QuadrilateralsOrdering[0], &Q2QuadrilateralsOrdering[8]);
  }

  if (Obj->NbrQ3Qua) {
    GmfSetKwd(OutMsh, GmfQuadrilateralsQ3, Obj->NbrQ3Qua);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ3, 1, Obj->NbrQ3Qua, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 16, &Obj->Q3Qua[1], &Obj->Q3Qua[Obj->NbrQ3Qua],
        typeOf[sizeof(int1)], &Obj->Q3QuaRef[1], &Obj->Q3QuaRef[Obj->NbrQ3Qua]);

    GmfSetKwd(OutMsh, GmfQuadrilateralsQ3Ordering, 16);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ3Ordering, 1, 16, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &Q3QuadrilateralsOrdering[0], &Q3QuadrilateralsOrdering[15]);
  }

  if (Obj->NbrQ4Qua) {
    GmfSetKwd(OutMsh, GmfQuadrilateralsQ4, Obj->NbrQ4Qua);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ4, 1, Obj->NbrQ4Qua, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 25, &Obj->Q4Qua[1], &Obj->Q4Qua[Obj->NbrQ4Qua],
        typeOf[sizeof(int1)], &Obj->Q4QuaRef[1], &Obj->Q4QuaRef[Obj->NbrQ4Qua]);

    GmfSetKwd(OutMsh, GmfQuadrilateralsQ4Ordering, 25);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ4Ordering, 1, 25, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &Q4QuadrilateralsOrdering[0], &Q4QuadrilateralsOrdering[24]);
  }

  // set tets
  if (Obj->NbrTet) {
    GmfSetKwd(OutMsh, GmfTetrahedra, Obj->NbrTet);
    GmfSetBlock(OutMsh, GmfTetrahedra, 1, Obj->NbrTet, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &Obj->Tet[1], &Obj->Tet[Obj->NbrTet],
        typeOf[sizeof(int1)], &Obj->TetRef[1], &Obj->TetRef[Obj->NbrTet]);

    GmfSetKwd(OutMsh, GmfTetrahedraP1Ordering, 4);
    GmfSetBlock(OutMsh, GmfTetrahedraP1Ordering, 1, 4, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P1TetrahedraOrdering[0], &P1TetrahedraOrdering[3]);
  }

  if (Obj->NbrP2Tet) {
    GmfSetKwd(OutMsh, GmfTetrahedraP2, Obj->NbrP2Tet);
    GmfSetBlock(OutMsh, GmfTetrahedraP2, 1, Obj->NbrP2Tet, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 10, &Obj->P2Tet[1], &Obj->P2Tet[Obj->NbrP2Tet],
        typeOf[sizeof(int1)], &Obj->P2TetRef[1], &Obj->P2TetRef[Obj->NbrP2Tet]);

    GmfSetKwd(OutMsh, GmfTetrahedraP2Ordering, 10);
    GmfSetBlock(OutMsh, GmfTetrahedraP2Ordering, 1, 10, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P2TetrahedraOrdering[0], &P2TetrahedraOrdering[9]);
  }

  if (Obj->NbrP3Tet) {
    GmfSetKwd(OutMsh, GmfTetrahedraP3, Obj->NbrP3Tet);
    GmfSetBlock(OutMsh, GmfTetrahedraP3, 1, Obj->NbrP3Tet, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 20, &Obj->P3Tet[1], &Obj->P3Tet[Obj->NbrP3Tet],
        typeOf[sizeof(int1)], &Obj->P3TetRef[1], &Obj->P3TetRef[Obj->NbrP3Tet]);

    GmfSetKwd(OutMsh, GmfTetrahedraP3Ordering, 20);
    GmfSetBlock(OutMsh, GmfTetrahedraP3Ordering, 1, 20, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P3TetrahedraOrdering[0], &P3TetrahedraOrdering[19]);
  }

  if (Obj->NbrP4Tet) {
    GmfSetKwd(OutMsh, GmfTetrahedraP4, Obj->NbrP4Tet);
    GmfSetBlock(OutMsh, GmfTetrahedraP4, 1, Obj->NbrP4Tet, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 35, &Obj->P4Tet[1], &Obj->P4Tet[Obj->NbrP4Tet],
        typeOf[sizeof(int1)], &Obj->P4TetRef[1], &Obj->P4TetRef[Obj->NbrP4Tet]);

    GmfSetKwd(OutMsh, GmfTetrahedraP4Ordering, 35);
    GmfSetBlock(OutMsh, GmfTetrahedraP4Ordering, 1, 35, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P4TetrahedraOrdering[0], &P4TetrahedraOrdering[34]);
  }

  // set pyrs
  if (Obj->NbrPyr) {
    GmfSetKwd(OutMsh, GmfPyramids, Obj->NbrPyr);
    GmfSetBlock(OutMsh, GmfPyramids, 1, Obj->NbrPyr, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 5, &Obj->Pyr[1], &Obj->Pyr[Obj->NbrPyr],
        typeOf[sizeof(int1)], &Obj->PyrRef[1], &Obj->PyrRef[Obj->NbrPyr]);

    GmfSetKwd(OutMsh, GmfPyramidsP1Ordering, 5);
    GmfSetBlock(OutMsh, GmfPyramidsP1Ordering, 1, 5, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P1PyramidsOrdering[0], &P1PyramidsOrdering[4]);
  }

  if (Obj->NbrP2Pyr) {
    GmfSetKwd(OutMsh, GmfPyramidsP2, Obj->NbrP2Pyr);
    GmfSetBlock(OutMsh, GmfPyramidsP2, 1, Obj->NbrP2Pyr, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 14, &Obj->P2Pyr[1], &Obj->P2Pyr[Obj->NbrP2Pyr],
        typeOf[sizeof(int1)], &Obj->P2PyrRef[1], &Obj->P2PyrRef[Obj->NbrP2Pyr]);

    GmfSetKwd(OutMsh, GmfPyramidsP2Ordering, 14);
    GmfSetBlock(OutMsh, GmfPyramidsP2Ordering, 1, 14, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P2PyramidsOrdering[0], &P2PyramidsOrdering[13]);
  }

  if (Obj->NbrP3Pyr) {
    GmfSetKwd(OutMsh, GmfPyramidsP3, Obj->NbrP3Pyr);
    GmfSetBlock(OutMsh, GmfPyramidsP3, 1, Obj->NbrP3Pyr, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 30, &Obj->P3Pyr[1], &Obj->P3Pyr[Obj->NbrP3Pyr],
        typeOf[sizeof(int1)], &Obj->P3PyrRef[1], &Obj->P3PyrRef[Obj->NbrP3Pyr]);

    GmfSetKwd(OutMsh, GmfPyramidsP3Ordering, 30);
    GmfSetBlock(OutMsh, GmfPyramidsP3Ordering, 1, 30, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P3PyramidsOrdering[0], &P3PyramidsOrdering[29]);
  }

  if (Obj->NbrP4Pyr) {
    GmfSetKwd(OutMsh, GmfPyramidsP4, Obj->NbrP4Pyr);
    GmfSetBlock(OutMsh, GmfPyramidsP4, 1, Obj->NbrP4Pyr, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 55, &Obj->P4Pyr[1], &Obj->P4Pyr[Obj->NbrP4Pyr],
        typeOf[sizeof(int1)], &Obj->P4PyrRef[1], &Obj->P4PyrRef[Obj->NbrP4Pyr]);

    GmfSetKwd(OutMsh, GmfPyramidsP4Ordering, 55);
    GmfSetBlock(OutMsh, GmfPyramidsP4Ordering, 1, 55, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P4PyramidsOrdering[0], &P4PyramidsOrdering[54]);
  }

  // set prims
  if (Obj->NbrPri) {
    GmfSetKwd(OutMsh, GmfPrisms, Obj->NbrPri);
    GmfSetBlock(OutMsh, GmfPrisms, 1, Obj->NbrPri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 6, &Obj->Pri[1], &Obj->Pri[Obj->NbrPri],
        typeOf[sizeof(int1)], &Obj->PriRef[1], &Obj->PriRef[Obj->NbrPri]);

    GmfSetKwd(OutMsh, GmfPrismsP1Ordering, 6);
    GmfSetBlock(OutMsh, GmfPrismsP1Ordering, 1, 6, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P1PrismsOrdering[0], &P1PrismsOrdering[5]);
  }

  if (Obj->NbrP2Pri) {
    GmfSetKwd(OutMsh, GmfPrismsP2, Obj->NbrP2Pri);
    GmfSetBlock(OutMsh, GmfPrismsP2, 1, Obj->NbrP2Pri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 18, &Obj->P2Pri[1], &Obj->P2Pri[Obj->NbrP2Pri],
        typeOf[sizeof(int1)], &Obj->P2PriRef[1], &Obj->P2PriRef[Obj->NbrP2Pri]);

    GmfSetKwd(OutMsh, GmfPrismsP2Ordering, 18);
    GmfSetBlock(OutMsh, GmfPrismsP2Ordering, 1, 18, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P2PrismsOrdering[0], &P2PrismsOrdering[17]);
  }

  if (Obj->NbrP3Pri) {
    GmfSetKwd(OutMsh, GmfPrismsP3, Obj->NbrP3Pri);
    GmfSetBlock(OutMsh, GmfPrismsP3, 1, Obj->NbrP3Pri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 40, &Obj->P3Pri[1], &Obj->P3Pri[Obj->NbrP3Pri],
        typeOf[sizeof(int1)], &Obj->P3PriRef[1], &Obj->P3PriRef[Obj->NbrP3Pri]);

    GmfSetKwd(OutMsh, GmfPrismsP3Ordering, 40);
    GmfSetBlock(OutMsh, GmfPrismsP3Ordering, 1, 40, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P3PrismsOrdering[0], &P3PrismsOrdering[39]);
  }

  if (Obj->NbrP4Pri) {
    GmfSetKwd(OutMsh, GmfPrismsP4, Obj->NbrP4Pri);
    GmfSetBlock(OutMsh, GmfPrismsP4, 1, Obj->NbrP4Pri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 75, &Obj->P4Pri[1], &Obj->P4Pri[Obj->NbrP4Pri],
        typeOf[sizeof(int1)], &Obj->P4PriRef[1], &Obj->P4PriRef[Obj->NbrP4Pri]);

    GmfSetKwd(OutMsh, GmfPrismsP4Ordering, 75);
    GmfSetBlock(OutMsh, GmfPrismsP4Ordering, 1, 75, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P4PrismsOrdering[0], &P4PrismsOrdering[74]);
  }

  // set hexes
  if (Obj->NbrHex) {
    GmfSetKwd(OutMsh, GmfHexahedra, Obj->NbrHex);
    GmfSetBlock(OutMsh, GmfHexahedra, 1, Obj->NbrHex, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 8, &Obj->Hex[1], &Obj->Hex[Obj->NbrHex],
        typeOf[sizeof(int1)], &Obj->HexRef[1], &Obj->HexRef[Obj->NbrHex]);

    GmfSetKwd(OutMsh, GmfHexahedraQ1Ordering, 8);
    GmfSetBlock(OutMsh, GmfHexahedraQ1Ordering, 1, 8, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Q1HexahedraOrdering[0], &Q1HexahedraOrdering[7]);
  }

  if (Obj->NbrQ2Hex) {
    GmfSetKwd(OutMsh, GmfHexahedraQ2, Obj->NbrQ2Hex);
    GmfSetBlock(OutMsh, GmfHexahedraQ2, 1, Obj->NbrQ2Hex, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 27, &Obj->Q2Hex[1], &Obj->Q2Hex[Obj->NbrQ2Hex],
        typeOf[sizeof(int1)], &Obj->Q2HexRef[1], &Obj->Q2HexRef[Obj->NbrQ2Hex]);

    GmfSetKwd(OutMsh, GmfHexahedraQ2Ordering, 27);
    GmfSetBlock(OutMsh, GmfHexahedraQ2Ordering, 1, 27, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Q2HexahedraOrdering[0], &Q2HexahedraOrdering[26]);
  }

  if (Obj->NbrQ3Hex) {
    GmfSetKwd(OutMsh, GmfHexahedraQ3, Obj->NbrQ3Hex);
    GmfSetBlock(OutMsh, GmfHexahedraQ3, 1, Obj->NbrQ3Hex, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 64, &Obj->Q3Hex[1], &Obj->Q3Hex[Obj->NbrQ3Hex],
        typeOf[sizeof(int1)], &Obj->Q3HexRef[1], &Obj->Q3HexRef[Obj->NbrQ3Hex]);

    GmfSetKwd(OutMsh, GmfHexahedraQ3Ordering, 64);
    GmfSetBlock(OutMsh, GmfHexahedraQ3Ordering, 1, 64, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Q3HexahedraOrdering[0], &Q3HexahedraOrdering[63]);
  }

  if (Obj->NbrQ4Hex) {
    GmfSetKwd(OutMsh, GmfHexahedraQ4, Obj->NbrQ4Hex);
    GmfSetBlock(OutMsh, GmfHexahedraQ4, 1, Obj->NbrQ4Hex, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 125, &Obj->Q4Hex[1], &Obj->Q4Hex[Obj->NbrQ4Hex],
        typeOf[sizeof(int1)], &Obj->Q4HexRef[1], &Obj->Q4HexRef[Obj->NbrQ4Hex]);

    GmfSetKwd(OutMsh, GmfHexahedraQ4Ordering, 125);
    GmfSetBlock(OutMsh, GmfHexahedraQ4Ordering, 1, 125, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Q4HexahedraOrdering[0], &Q4HexahedraOrdering[124]);
  }

  /* Close current mesh */
  GmfCloseMesh(OutMsh);

  return VIZINT_SUCCESS;
}

double GetWallClock()
{
#ifdef WIN32
  struct __timeb64 tb;
  _ftime64(&tb);
  return ((double)tb.time + (double)tb.millitm / 1000.);
#else
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return (tp.tv_sec + tp.tv_usec / 1000000.);
#endif
}

#define NODE_INDEX3D(I, NI, J, NJ, K, NK) ((I) + NI * (((J)-1) + NJ * ((K)-1)))
#define NODE_INDEX2D(I, NI, J, NJ) ((I) + NI * ((J)-1))

int viz_OpenMeshCGNS(VizObject* iObj, const char* MshFile, VizIntStatus* status)
{
  if (viz_setMeshArraysFromCGNSFile(0, MshFile, &(iObj->Dim), &(iObj->NbrVer), &(iObj->NbrCrn), &(iObj->NbrEdg), &(iObj->NbrTri), &(iObj->NbrQua), &(iObj->NbrTet), &(iObj->NbrPyr), &(iObj->NbrPri), &(iObj->NbrHex),
          &(iObj->NbrP2Edg), &(iObj->NbrP2Tri), &(iObj->NbrQ2Qua), &(iObj->NbrP2Tet), &(iObj->NbrP2Pyr), &(iObj->NbrP2Pri), &(iObj->NbrQ2Hex),
          &(iObj->NbrP3Edg), &(iObj->NbrP3Tri), &(iObj->NbrQ3Qua), &(iObj->NbrP3Tet), &(iObj->NbrP3Pyr), &(iObj->NbrP3Pri), &(iObj->NbrQ3Hex),
          &(iObj->NbrP4Edg), &(iObj->NbrP4Tri), &(iObj->NbrQ4Qua), &(iObj->NbrP4Tet), &(iObj->NbrP4Pyr), &(iObj->NbrP4Pri), &(iObj->NbrQ4Hex),
          NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          status)
      == VIZINT_SUCCESS) {

    iObj->Crd = viz_reallocate(sizeof(double3) * (iObj->NbrVer + 1), iObj->Crd);
    if (!iObj->Crd) {
      viz_writestatus(status, "Allocation failed for vertex-arrays, size %lg Mb\n", sizeof(double3) * (iObj->NbrVer + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->CrdRef = viz_reallocate(sizeof(int1) * (iObj->NbrVer + 1), iObj->CrdRef);
    memset(iObj->CrdRef, 0, sizeof(int1) * (iObj->NbrVer + 1));
    if (!iObj->CrdRef) {
      viz_writestatus(status, "Allocation failed for vertex-arrays, size %lg Mb\n", sizeof(double3) * (iObj->NbrVer + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrVerMax = iObj->NbrVer;
    iObj->Edg       = viz_reallocate(sizeof(int2) * (iObj->NbrEdg + 1), iObj->Edg);
    iObj->EdgRef    = viz_reallocate(sizeof(int1) * (iObj->NbrEdg + 1), iObj->EdgRef);
    if (!iObj->Edg || !iObj->EdgRef) {
      viz_writestatus(status, "Allocation failed for Edge-arrays, size %lg Mb\n", sizeof(int2) * (iObj->NbrEdg + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrEdgMax = iObj->NbrEdg;
    memset(iObj->EdgRef, 0, sizeof(int1) * (iObj->NbrEdg + 1));
    iObj->Crn    = viz_reallocate(sizeof(int1) * (iObj->NbrCrn + 1), iObj->Crn);
    iObj->CrnRef = viz_reallocate(sizeof(int1) * (iObj->NbrCrn + 1), iObj->CrnRef);
    if (!iObj->Crn) {
      viz_writestatus(status, "Allocation failed for corners-arrays, size %lg Mb\n", sizeof(int) * (iObj->NbrCrn + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrCrnMax = iObj->NbrCrn;
    memset(iObj->CrnRef, 0, sizeof(int1) * (iObj->NbrCrn + 1));
    iObj->Tri    = viz_reallocate(sizeof(int3) * (iObj->NbrTri + 1), iObj->Tri);
    iObj->TriRef = viz_reallocate(sizeof(int1) * (iObj->NbrTri + 1), iObj->TriRef);
    if (!iObj->Tri || !iObj->TriRef) {
      viz_writestatus(status, "Allocation failed for triangle-arrays, size %lg Mb\n", sizeof(int3) * (iObj->NbrTri + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrTriMax = iObj->NbrTri;
    iObj->Qua       = viz_reallocate(sizeof(int4) * (iObj->NbrQua + 1), iObj->Qua);
    iObj->QuaRef    = viz_reallocate(sizeof(int1) * (iObj->NbrQua + 1), iObj->QuaRef);
    if (!iObj->Qua || !iObj->QuaRef) {
      viz_writestatus(status, "Allocation failed for Quad-arrays, size %lg Mb\n", sizeof(int4) * (iObj->NbrQua + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrQuaMax = iObj->NbrQua;
    iObj->Tet       = viz_reallocate(sizeof(int4) * (iObj->NbrTet + 1), iObj->Tet);
    iObj->TetRef    = viz_reallocate(sizeof(int1) * (iObj->NbrTet + 1), iObj->TetRef);
    if (!iObj->Tet || !iObj->TetRef) {
      viz_writestatus(status, "Allocation failed for Tet-arrays, size %lg Mb\n", sizeof(int4) * (iObj->NbrTet + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrTetMax = iObj->NbrTet;
    memset(iObj->TetRef, 0, sizeof(int1) * (iObj->NbrTet + 1));
    iObj->Pyr    = viz_reallocate(sizeof(int5) * (iObj->NbrPyr + 1), iObj->Pyr);
    iObj->PyrRef = viz_reallocate(sizeof(int1) * (iObj->NbrPyr + 1), iObj->PyrRef);
    if (!iObj->Pyr || !iObj->PyrRef) {
      viz_writestatus(status, "Allocation failed for Pyramid-arrays, size %lg Mb\n", sizeof(int5) * (iObj->NbrPyr + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrPyrMax = iObj->NbrPyr;
    memset(iObj->PyrRef, 0, sizeof(int1) * (iObj->NbrPyr + 1));
    iObj->Pri    = viz_reallocate(sizeof(int6) * (iObj->NbrPri + 1), iObj->Pri);
    iObj->PriRef = viz_reallocate(sizeof(int1) * (iObj->NbrPri + 1), iObj->PriRef);
    if (!iObj->Pri || !iObj->PriRef) {
      viz_writestatus(status, "Allocation failed for Prism-arrays, size %lg Mb\n", sizeof(int6) * (iObj->NbrPri + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrPriMax = iObj->NbrPri;
    memset(iObj->PriRef, 0, sizeof(int1) * (iObj->NbrPri + 1));
    iObj->Hex    = viz_reallocate(sizeof(int8) * (iObj->NbrHex + 1), iObj->Hex);
    iObj->HexRef = viz_reallocate(sizeof(int1) * (iObj->NbrHex + 1), iObj->HexRef);
    if (!iObj->Hex || !iObj->HexRef) {
      viz_writestatus(status, "Allocation failed for Hex-arrays, size %lg Mb\n", sizeof(int8) * (iObj->NbrHex + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrHexMax = iObj->NbrHex;
    memset(iObj->HexRef, 0, sizeof(int1) * (iObj->NbrHex + 1));

    iObj->P2Edg    = viz_reallocate(sizeof(int3) * (iObj->NbrP2Edg + 1), iObj->P2Edg);
    iObj->P2EdgRef = viz_reallocate(sizeof(int1) * (iObj->NbrP2Edg + 1), iObj->P2EdgRef);
    if (!iObj->P2Edg || !iObj->P2EdgRef) {
      viz_writestatus(status, "Allocation failed for P2Edge-arrays, size %lg Mb\n", sizeof(int2) * (iObj->NbrP2Edg + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP2EdgMax = iObj->NbrP2Edg;
    memset(iObj->P2EdgRef, 0, sizeof(int1) * (iObj->NbrP2Edg + 1));
    iObj->P2Tri    = viz_reallocate(sizeof(int6) * (iObj->NbrP2Tri + 1), iObj->P2Tri);
    iObj->P2TriRef = viz_reallocate(sizeof(int1) * (iObj->NbrP2Tri + 1), iObj->P2TriRef);
    if (!iObj->P2Tri || !iObj->P2TriRef) {
      viz_writestatus(status, "Allocation failed for P2-triangle-arrays, size %lg Mb\n", sizeof(int6) * (iObj->NbrP2Tri + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP2TriMax = iObj->NbrP2Tri;
    iObj->Q2Qua       = viz_reallocate(sizeof(int9) * (iObj->NbrQ2Qua + 1), iObj->Q2Qua);
    iObj->Q2QuaRef    = viz_reallocate(sizeof(int1) * (iObj->NbrQ2Qua + 1), iObj->Q2QuaRef);
    if (!iObj->Q2Qua || !iObj->Q2QuaRef) {
      viz_writestatus(status, "Allocation failed for Q2Quad-arrays, size %lg Mb\n", sizeof(int9) * (iObj->NbrQ2Qua + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrQ2QuaMax = iObj->NbrQ2Qua;
    iObj->P2Tet       = viz_reallocate(sizeof(int10) * (iObj->NbrP2Tet + 1), iObj->P2Tet);
    iObj->P2TetRef    = viz_reallocate(sizeof(int1) * (iObj->NbrP2Tet + 1), iObj->P2TetRef);
    if (!iObj->P2Tet || !iObj->P2TetRef) {
      viz_writestatus(status, "Allocation failed for P2Tet-arrays, size %lg Mb\n", sizeof(int10) * (iObj->NbrP2Tet + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP2TetMax = iObj->NbrP2Tet;
    memset(iObj->P2TetRef, 0, sizeof(int1) * (iObj->NbrP2Tet + 1));
    iObj->P2Pyr    = viz_reallocate(sizeof(int14) * (iObj->NbrP2Pyr + 1), iObj->P2Pyr);
    iObj->P2PyrRef = viz_reallocate(sizeof(int1) * (iObj->NbrP2Pyr + 1), iObj->P2PyrRef);
    if (!iObj->P2Pyr || !iObj->P2PyrRef) {
      viz_writestatus(status, "Allocation failed for P2Pyr-arrays, size %lg Mb\n", sizeof(int14) * (iObj->NbrP2Pyr + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP2PyrMax = iObj->NbrP2Pyr;
    memset(iObj->P2PyrRef, 0, sizeof(int1) * (iObj->NbrP2Pyr + 1));
    iObj->P2Pri    = viz_reallocate(sizeof(int18) * (iObj->NbrP2Pri + 1), iObj->P2Pri);
    iObj->P2PriRef = viz_reallocate(sizeof(int1) * (iObj->NbrP2Pri + 1), iObj->P2PriRef);
    if (!iObj->P2Pri || !iObj->P2PriRef) {
      viz_writestatus(status, "Allocation failed for P2Pri-arrays, size %lg Mb\n", sizeof(int18) * (iObj->NbrP2Pri + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP2PriMax = iObj->NbrP2Pri;
    memset(iObj->P2PriRef, 0, sizeof(int1) * (iObj->NbrP2Pri + 1));
    iObj->Q2Hex    = viz_reallocate(sizeof(int27) * (iObj->NbrQ2Hex + 1), iObj->Q2Hex);
    iObj->Q2HexRef = viz_reallocate(sizeof(int1) * (iObj->NbrQ2Hex + 1), iObj->Q2HexRef);
    if (!iObj->Q2Hex || !iObj->Q2HexRef) {
      viz_writestatus(status, "Allocation failed for Q2Hex-arrays, size %lg Mb\n", sizeof(int27) * (iObj->NbrQ2Hex + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrQ2HexMax = iObj->NbrQ2Hex;
    memset(iObj->Q2HexRef, 0, sizeof(int1) * (iObj->NbrQ2Hex + 1));

    iObj->P3Edg    = viz_reallocate(sizeof(int4) * (iObj->NbrP3Edg + 1), iObj->P3Edg);
    iObj->P3EdgRef = viz_reallocate(sizeof(int1) * (iObj->NbrP3Edg + 1), iObj->P3EdgRef);
    if (!iObj->P3Edg || !iObj->P3EdgRef) {
      viz_writestatus(status, "Allocation failed for P3Edge-arrays, size %lg Mb\n", sizeof(int2) * (iObj->NbrP3Edg + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP3EdgMax = iObj->NbrP3Edg;
    memset(iObj->P3EdgRef, 0, sizeof(int1) * (iObj->NbrP3Edg + 1));
    iObj->P3Tri    = viz_reallocate(sizeof(int10) * (iObj->NbrP3Tri + 1), iObj->P3Tri);
    iObj->P3TriRef = viz_reallocate(sizeof(int1) * (iObj->NbrP3Tri + 1), iObj->P3TriRef);
    if (!iObj->P3Tri || !iObj->P3TriRef) {
      viz_writestatus(status, "Allocation failed for P3-triangle-arrays, size %lg Mb\n", sizeof(int6) * (iObj->NbrP3Tri + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP3TriMax = iObj->NbrP3Tri;
    iObj->Q3Qua       = viz_reallocate(sizeof(int16) * (iObj->NbrQ3Qua + 1), iObj->Q3Qua);
    iObj->Q3QuaRef    = viz_reallocate(sizeof(int1) * (iObj->NbrQ3Qua + 1), iObj->Q3QuaRef);
    if (!iObj->Q3Qua || !iObj->Q3QuaRef) {
      viz_writestatus(status, "Allocation failed for Q3Quad-arrays, size %lg Mb\n", sizeof(int9) * (iObj->NbrQ3Qua + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrQ3QuaMax = iObj->NbrQ3Qua;
    iObj->P3Tet       = viz_reallocate(sizeof(int20) * (iObj->NbrP3Tet + 1), iObj->P3Tet);
    iObj->P3TetRef    = viz_reallocate(sizeof(int1) * (iObj->NbrP3Tet + 1), iObj->P3TetRef);
    if (!iObj->P3Tet || !iObj->P3TetRef) {
      viz_writestatus(status, "Allocation failed for P3Tet-arrays, size %lg Mb\n", sizeof(int10) * (iObj->NbrP3Tet + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP3TetMax = iObj->NbrP3Tet;
    memset(iObj->P3TetRef, 0, sizeof(int1) * (iObj->NbrP3Tet + 1));
    iObj->P3Pyr    = viz_reallocate(sizeof(int30) * (iObj->NbrP3Pyr + 1), iObj->P3Pyr);
    iObj->P3PyrRef = viz_reallocate(sizeof(int1) * (iObj->NbrP3Pyr + 1), iObj->P3PyrRef);
    if (!iObj->P3Pyr || !iObj->P3PyrRef) {
      viz_writestatus(status, "Allocation failed for P3Pyr-arrays, size %lg Mb\n", sizeof(int14) * (iObj->NbrP3Pyr + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP3PyrMax = iObj->NbrP3Pyr;
    memset(iObj->P3PyrRef, 0, sizeof(int1) * (iObj->NbrP3Pyr + 1));
    iObj->P3Pri    = viz_reallocate(sizeof(int40) * (iObj->NbrP3Pri + 1), iObj->P3Pri);
    iObj->P3PriRef = viz_reallocate(sizeof(int1) * (iObj->NbrP3Pri + 1), iObj->P3PriRef);
    if (!iObj->P3Pri || !iObj->P3PriRef) {
      viz_writestatus(status, "Allocation failed for P3Pri-arrays, size %lg Mb\n", sizeof(int18) * (iObj->NbrP3Pri + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP3PriMax = iObj->NbrP3Pri;
    memset(iObj->P3PriRef, 0, sizeof(int1) * (iObj->NbrP3Pri + 1));
    iObj->Q3Hex    = viz_reallocate(sizeof(int64) * (iObj->NbrQ3Hex + 1), iObj->Q3Hex);
    iObj->Q3HexRef = viz_reallocate(sizeof(int1) * (iObj->NbrQ3Hex + 1), iObj->Q3HexRef);
    if (!iObj->Q3Hex || !iObj->Q3HexRef) {
      viz_writestatus(status, "Allocation failed for Q3Hex-arrays, size %lg Mb\n", sizeof(int27) * (iObj->NbrQ3Hex + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrQ3HexMax = iObj->NbrQ3Hex;
    memset(iObj->Q3HexRef, 0, sizeof(int1) * (iObj->NbrQ3Hex + 1));

    iObj->P4Edg    = viz_reallocate(sizeof(int5) * (iObj->NbrP4Edg + 1), iObj->P4Edg);
    iObj->P4EdgRef = viz_reallocate(sizeof(int1) * (iObj->NbrP4Edg + 1), iObj->P4EdgRef);
    if (!iObj->P4Edg || !iObj->P4EdgRef) {
      viz_writestatus(status, "Allocation failed for P4Edge-arrays, size %lg Mb\n", sizeof(int2) * (iObj->NbrP4Edg + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP4EdgMax = iObj->NbrP4Edg;
    memset(iObj->P4EdgRef, 0, sizeof(int1) * (iObj->NbrP4Edg + 1));
    iObj->P4Tri    = viz_reallocate(sizeof(int15) * (iObj->NbrP4Tri + 1), iObj->P4Tri);
    iObj->P4TriRef = viz_reallocate(sizeof(int1) * (iObj->NbrP4Tri + 1), iObj->P4TriRef);
    if (!iObj->P4Tri || !iObj->P4TriRef) {
      viz_writestatus(status, "Allocation failed for P4-triangle-arrays, size %lg Mb\n", sizeof(int6) * (iObj->NbrP4Tri + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP4TriMax = iObj->NbrP4Tri;
    iObj->Q4Qua       = viz_reallocate(sizeof(int25) * (iObj->NbrQ4Qua + 1), iObj->Q4Qua);
    iObj->Q4QuaRef    = viz_reallocate(sizeof(int1) * (iObj->NbrQ4Qua + 1), iObj->Q4QuaRef);
    if (!iObj->Q4Qua || !iObj->Q4QuaRef) {
      viz_writestatus(status, "Allocation failed for Q4Quad-arrays, size %lg Mb\n", sizeof(int9) * (iObj->NbrQ4Qua + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrQ4QuaMax = iObj->NbrQ4Qua;
    iObj->P4Tet       = viz_reallocate(sizeof(int35) * (iObj->NbrP4Tet + 1), iObj->P4Tet);
    iObj->P4TetRef    = viz_reallocate(sizeof(int1) * (iObj->NbrP4Tet + 1), iObj->P4TetRef);
    if (!iObj->P4Tet || !iObj->P4TetRef) {
      viz_writestatus(status, "Allocation failed for P4Tet-arrays, size %lg Mb\n", sizeof(int10) * (iObj->NbrP4Tet + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP4TetMax = iObj->NbrP4Tet;
    memset(iObj->P4TetRef, 0, sizeof(int1) * (iObj->NbrP4Tet + 1));
    iObj->P4Pyr    = viz_reallocate(sizeof(int55) * (iObj->NbrP4Pyr + 1), iObj->P4Pyr);
    iObj->P4PyrRef = viz_reallocate(sizeof(int1) * (iObj->NbrP4Pyr + 1), iObj->P4PyrRef);
    if (!iObj->P4Pyr || !iObj->P4PyrRef) {
      viz_writestatus(status, "Allocation failed for P4Pyr-arrays, size %lg Mb\n", sizeof(int14) * (iObj->NbrP4Pyr + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP4PyrMax = iObj->NbrP4Pyr;
    memset(iObj->P4PyrRef, 0, sizeof(int1) * (iObj->NbrP4Pyr + 1));
    iObj->P4Pri    = viz_reallocate(sizeof(int75) * (iObj->NbrP4Pri + 1), iObj->P4Pri);
    iObj->P4PriRef = viz_reallocate(sizeof(int1) * (iObj->NbrP4Pri + 1), iObj->P4PriRef);
    if (!iObj->P4Pri || !iObj->P4PriRef) {
      viz_writestatus(status, "Allocation failed for P4Pri-arrays, size %lg Mb\n", sizeof(int18) * (iObj->NbrP4Pri + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrP4PriMax = iObj->NbrP4Pri;
    memset(iObj->P4PriRef, 0, sizeof(int1) * (iObj->NbrP4Pri + 1));
    iObj->Q4Hex    = viz_reallocate(sizeof(int125) * (iObj->NbrQ4Hex + 1), iObj->Q4Hex);
    iObj->Q4HexRef = viz_reallocate(sizeof(int1) * (iObj->NbrQ4Hex + 1), iObj->Q4HexRef);
    if (!iObj->Q4Hex || !iObj->Q4HexRef) {
      viz_writestatus(status, "Allocation failed for Q4Hex-arrays, size %lg Mb\n", sizeof(int27) * (iObj->NbrQ4Hex + 1) / (1024. * 1024.));
      return VIZINT_ERROR;
    }
    iObj->NbrQ2HexMax = iObj->NbrQ2Hex;
    memset(iObj->Q2HexRef, 0, sizeof(int1) * (iObj->NbrQ2Hex + 1));

    int ierr = viz_setMeshArraysFromCGNSFile(1, MshFile, &(iObj->Dim), &(iObj->NbrVer), &(iObj->NbrCor), &(iObj->NbrEdg), &(iObj->NbrTri), &(iObj->NbrQua), &(iObj->NbrTet), &(iObj->NbrPyr), &(iObj->NbrPri), &(iObj->NbrHex),
        &(iObj->NbrP2Edg), &(iObj->NbrP2Tri), &(iObj->NbrQ2Qua), &(iObj->NbrP2Tet), &(iObj->NbrP2Pyr), &(iObj->NbrP2Pri), &(iObj->NbrQ2Hex),
        &(iObj->NbrP3Edg), &(iObj->NbrP3Tri), &(iObj->NbrQ3Qua), &(iObj->NbrP3Tet), &(iObj->NbrP3Pyr), &(iObj->NbrP3Pri), &(iObj->NbrQ3Hex),
        &(iObj->NbrP4Edg), &(iObj->NbrP4Tri), &(iObj->NbrQ4Qua), &(iObj->NbrP4Tet), &(iObj->NbrP4Pyr), &(iObj->NbrP4Pri), &(iObj->NbrQ4Hex),
        iObj->Crd, iObj->Crn, iObj->Edg, iObj->EdgRef, iObj->Qua, iObj->QuaRef, iObj->Tri, iObj->TriRef, iObj->Tet, iObj->TetRef, iObj->Pyr, iObj->PyrRef, iObj->Pri, iObj->PriRef, iObj->Hex, iObj->HexRef,
        iObj->P2Edg, iObj->P2EdgRef, iObj->P2Tri, iObj->P2TriRef, iObj->Q2Qua, iObj->Q2QuaRef, iObj->P2Tet, iObj->P2TetRef,
        iObj->P2Pyr, iObj->P2PyrRef, iObj->P2Pri, iObj->P2PriRef, iObj->Q2Hex, iObj->Q2HexRef,
        iObj->P3Edg, iObj->P3EdgRef, iObj->P3Tri, iObj->P3TriRef, iObj->Q3Qua, iObj->Q3QuaRef, iObj->P3Tet, iObj->P3TetRef,
        iObj->P3Pyr, iObj->P3PyrRef, iObj->P3Pri, iObj->P3PriRef, iObj->Q3Hex, iObj->Q3HexRef,
        iObj->P4Edg, iObj->P4EdgRef, iObj->P4Tri, iObj->P4TriRef, iObj->Q4Qua, iObj->Q4QuaRef, iObj->P4Tet, iObj->P4TetRef,
        iObj->P4Pyr, iObj->P4PyrRef, iObj->P4Pri, iObj->P4PriRef, iObj->Q4Hex, iObj->Q4HexRef,
        status);

    if (ierr != VIZINT_SUCCESS) {
      printf("%s\n", status->ErrMsg);
      viz_writestatus(status, "Cannot open mesh file %s\n", MshFile);
      return ierr;
    }
    else
      viz_writeinfo(status, "%s", MshFile);
  }
  else {
    printf("%s\n", status->ErrMsg);
    viz_writestatus(status, "Cannot open mesh file %s\n", MshFile);
    return VIZINT_ERROR;
  }
  return VIZINT_SUCCESS;
}

/*
  Mode 0 => set size of the arrays. It is needed to allocate them
  Mode 1 => fill in arrays. We assume arrays are already allocated in this case
*/
int viz_setMeshArraysFromCGNSFile(int Mode, const char* MshNam, int1* Dim, int1* NbrVer, int1* NbrCor, int1* NbrEdg, int1* NbrTri, int1* NbrQua, int1* NbrTet, int1* NbrPyr, int1* NbrPri, int1* NbrHex,
    int1* NbrP2Edg, int1* NbrP2Tri, int1* NbrQ2Qua, int1* NbrP2Tet, int1* NbrP2Pyr, int1* NbrP2Pri, int1* NbrQ2Hex,
    int1* NbrP3Edg, int1* NbrP3Tri, int1* NbrQ3Qua, int1* NbrP3Tet, int1* NbrP3Pyr, int1* NbrP3Pri, int1* NbrQ3Hex,
    int1* NbrP4Edg, int1* NbrP4Tri, int1* NbrQ4Qua, int1* NbrP4Tet, int1* NbrP4Pyr, int1* NbrP4Pri, int1* NbrQ4Hex,
    double3* Crd, int1* Cor, int2* Edg, int1* EdgRef, int4* Qua, int1* QuaRef, int3* Tri, int1* TriRef, int4* Tet, int1* TetRef, int5* Pyr, int1* PyrRef, int6* Pri, int1* PriRef, int8* Hex, int1* HexRef,
    int3* P2Edg, int1* P2EdgRef, int6* P2Tri, int1* P2TriRef, int9* Q2Qua, int1* Q2QuaRef, int10* P2Tet, int1* P2TetRef, int14* P2Pyr, int1* P2PyrRef, int18* P2Pri, int1* P2PriRef, int27* Q2Hex, int1* Q2HexRef,
    int4* P3Edg, int1* P3EdgRef, int10* P3Tri, int1* P3TriRef, int16* Q3Qua, int1* Q3QuaRef, int20* P3Tet, int1* P3TetRef, int30* P3Pyr, int1* P3PyrRef, int40* P3Pri, int1* P3PriRef, int64* Q3Hex, int1* Q3HexRef,
    int5* P4Edg, int1* P4EdgRef, int15* P4Tri, int1* P4TriRef, int25* Q4Qua, int1* Q4QuaRef, int35* P4Tet, int1* P4TetRef, int55* P4Pyr, int1* P4PyrRef, int75* P4Pri, int1* P4PriRef, int125* Q4Hex, int1* Q4HexRef,
    VizIntStatus* status)
{

  char Label[33];

  cgsize_t Index, Index_1, Index_2, NbrCor0, NbrEdg0,
      NbrP2Edg0, NbrP3Edg0, NbrP4Edg0,
      NbrQua0, NbrVer0, NbrVerZone, NbrElt,
      NbrTri0, NbrHex0, NbrPyr0, NbrPri0, i, j, k, n, ii,
      NbrTet0, Number_of_Elems_Faces, Number_of_Zone_Elems_Faces,
      NbrP2Tri0, NbrQ2Qua0, NbrP2Tet0, NbrP2Pyr0, NbrP2Pri0, NbrQ2Hex0,
      NbrP3Tri0, NbrQ3Qua0, NbrP3Tet0, NbrP3Pyr0, NbrP3Pri0, NbrQ3Hex0,
      NbrP4Tri0, NbrQ4Qua0, NbrP4Tet0, NbrP4Pyr0, NbrP4Pri0, NbrQ4Hex0,
      SurfTri_ID, SurfQua_ID, SurfEdg_ID, SurfP2Edg_ID, SurfP3Edg_ID, SurfP4Edg_ID,
      SurfP2Tri_ID, SurfQ2Qua_ID, SurfP2Tet_ID, SurfP2Pyr_ID, SurfP2Pri_ID, SurfQ2Hex_ID,
      SurfP3Tri_ID, SurfQ3Qua_ID, SurfP3Tet_ID, SurfP3Pyr_ID, SurfP3Pri_ID, SurfQ3Hex_ID,
      SurfP4Tri_ID, SurfQ4Qua_ID, SurfP4Tet_ID, SurfP4Pyr_ID, SurfP4Pri_ID, SurfQ4Hex_ID;

  int27 Q2HexTmp;

  cgsize_t CG_Err = 0, Array_Size, nbrElt, sizElt, Start_Index, End_Index, size;

  cgsize_t *Connectivity = NULL, *Parent_Data = NULL, *ConnectOffset = NULL;

  cgsize_t Zone_Size[3][3], max_Index[3], min_Index[3];

  int File_Index, Cell_Dim, Phys_Dim, Base_Index, Number_of_Bases, NbrNod,
      Number_of_Boundary_Items, Number_of_Sections,
      Number_of_Zones, Parent_Flag, Section_Index, Zone_Index;

  ElementType_t Element_Type, SubElement_Type;

  ZoneType_t Zone_Type;

  double* Coordinate = NULL;

  CG_Err = cg_open(MshNam, CG_MODE_READ, &File_Index);

  if (File_Index < 0) {
    viz_writestatus(status, "  unable to open CGNS mesh file");
    return VIZINT_ERROR;
  }

  if (CG_Err != CG_OK && Mode == 0)
    printf("  %%%% Warning in CGNS File Reader (cg_open function): %s\n", cg_get_error());

  CG_Err = cg_nbases(File_Index, &Number_of_Bases);

  if (CG_Err != CG_OK && Mode == 0)
    printf("  %%%% Warning in CGNS File Reader: cg_nbases %s\n", cg_get_error());

  if (Number_of_Bases != 1 && Mode == 0)
    printf("  %%%% Warning in CGNS File Reader: only the first base is considered\n");

  Base_Index = 1;

  CG_Err = cg_base_read(File_Index, Base_Index,
      Label, &Cell_Dim, &Phys_Dim);

  if (CG_Err != CG_OK && Mode == 0)
    printf("  %%%% Warning in CGNS File Reader: cg_base_read %s\n", cg_get_error());

  if ((Phys_Dim != 3 && Phys_Dim != 2)) {
    cg_close(File_Index);
    viz_writestatus(status, "  only 2D/3D physical dimensions are allowed in CGNS file");
    return VIZINT_ERROR;
  }
  *Dim = Phys_Dim;

  if ((Cell_Dim != 3 && Cell_Dim != 2)) {
    cg_close(File_Index);
    viz_writestatus(status, "  only 2D/3D cell dimensions are allowed in CGNS file");
    return VIZINT_ERROR;
  }

  CG_Err = cg_nzones(File_Index, Base_Index, &Number_of_Zones);

  if (CG_Err != CG_OK && Mode == 0)
    printf("  %%%% Warning in CGNS File Reader in cg_nzones: %s\n", cg_get_error());

  if (Number_of_Zones > 1 && Mode == 0)
    printf("  %%%% CGNS File Reader: %d Zones are Detected\n", Number_of_Zones);

  //--- Count the number of entities here => we count it for all zones.

  *NbrVer      = 0;
  *NbrCor      = 0;
  *NbrEdg      = 0;
  *NbrTri      = 0;
  *NbrQua      = 0;
  *NbrTet      = 0;
  *NbrPyr      = 0;
  *NbrPri      = 0;
  *NbrHex      = 0;
  *NbrP2Edg    = 0;
  *NbrP2Tri    = 0;
  *NbrQ2Qua    = 0;
  *NbrP2Tet    = 0;
  *NbrP2Pyr    = 0;
  *NbrP2Pri    = 0;
  *NbrQ2Hex    = 0;
  *NbrP3Edg    = 0;
  *NbrP3Tri    = 0;
  *NbrQ3Qua    = 0;
  *NbrP3Tet    = 0;
  *NbrP3Pyr    = 0;
  *NbrP3Pri    = 0;
  *NbrQ3Hex    = 0;
  *NbrP4Edg    = 0;
  *NbrP4Tri    = 0;
  *NbrQ4Qua    = 0;
  *NbrP4Tet    = 0;
  *NbrP4Pyr    = 0;
  *NbrP4Pri    = 0;
  *NbrQ4Hex    = 0;
  SurfEdg_ID   = 0;
  SurfTri_ID   = 0;
  SurfQua_ID   = 0;
  SurfP2Edg_ID = 0;
  SurfP2Tri_ID = 0;
  SurfQ2Qua_ID = 0;
  SurfP2Tet_ID = 0;
  SurfP2Pyr_ID = 0;
  SurfP2Pri_ID = 0;
  SurfQ2Hex_ID = 0;
  SurfP3Edg_ID = 0;
  SurfP3Tri_ID = 0;
  SurfQ3Qua_ID = 0;
  SurfP3Tet_ID = 0;
  SurfP3Pyr_ID = 0;
  SurfP3Pri_ID = 0;
  SurfQ3Hex_ID = 0;
  SurfP4Edg_ID = 0;
  SurfP4Tri_ID = 0;
  SurfQ4Qua_ID = 0;
  SurfP4Tet_ID = 0;
  SurfP4Pyr_ID = 0;
  SurfP4Pri_ID = 0;
  SurfQ4Hex_ID = 0;

  for (Zone_Index = 1; Zone_Index <= Number_of_Zones; Zone_Index++) {

    NbrVer0   = *NbrVer;
    NbrCor0   = *NbrCor;
    NbrEdg0   = *NbrEdg;
    NbrTri0   = *NbrTri;
    NbrQua0   = *NbrQua;
    NbrTet0   = *NbrTet;
    NbrPyr0   = *NbrPyr;
    NbrPri0   = *NbrPri;
    NbrHex0   = *NbrHex;
    NbrP2Edg0 = *NbrP2Edg;
    NbrP2Tri0 = *NbrP2Tri;
    NbrQ2Qua0 = *NbrQ2Qua;
    NbrP2Tet0 = *NbrP2Tet;
    NbrP2Pyr0 = *NbrP2Pyr;
    NbrP2Pri0 = *NbrP2Pri;
    NbrQ2Hex0 = *NbrQ2Hex;
    NbrP3Edg0 = *NbrP3Edg;
    NbrP3Tri0 = *NbrP3Tri;
    NbrQ3Qua0 = *NbrQ3Qua;
    NbrP3Tet0 = *NbrP3Tet;
    NbrP3Pyr0 = *NbrP3Pyr;
    NbrP3Pri0 = *NbrP3Pri;
    NbrQ3Hex0 = *NbrQ3Hex;
    NbrP4Edg0 = *NbrP4Edg;
    NbrP4Tri0 = *NbrP4Tri;
    NbrQ4Qua0 = *NbrQ4Qua;
    NbrP4Tet0 = *NbrP4Tet;
    NbrP4Pyr0 = *NbrP4Pyr;
    NbrP4Pri0 = *NbrP4Pri;
    NbrQ4Hex0 = *NbrQ4Hex;

    CG_Err = cg_zone_type(File_Index, Base_Index, Zone_Index, &Zone_Type);

    if (CG_Err != CG_OK && Mode == 0)
      printf("  %%%% Warning in CGNS File Reader in cg_zone_type: %s\n", cg_get_error());

    CG_Err = cg_zone_read(File_Index, Base_Index, Zone_Index, Label,
        *Zone_Size);

    // printf("Zone_Size %ld %ld %ld \n", Zone_Size[0][0], Zone_Size[0][1], Zone_Size[0][2]);
    // printf("Zone_Size %ld %ld %ld \n", Zone_Size[1][0], Zone_Size[1][1], Zone_Size[1][2]);
    // printf("Zone_Size %ld %ld %ld \n", Zone_Size[2][0], Zone_Size[2][1], Zone_Size[2][2]);

    if (CG_Err != CG_OK && Mode == 0)
      printf("  %%%% Warning in CGNS File Reader in cg_zone_read: %s\n", cg_get_error());

    if (Zone_Type == Unstructured) {
      NbrVerZone = Zone_Size[0][0];
      *NbrVer += NbrVerZone;
    }
    else {
      if (Phys_Dim == 3)
        NbrVerZone = (Zone_Size[0][0]) * (Zone_Size[0][1]) * (Zone_Size[0][2]);
      else
        NbrVerZone = (Zone_Size[0][0]) * (Zone_Size[0][1]);
      *NbrVer += NbrVerZone;
    }

    if (NbrVerZone < 1) {
      cg_close(File_Index);
      viz_writestatus(status, "  no vertices are specified in CGNS mesh file");
      return VIZINT_ERROR;
    }

    //-- create a tensorial mesh from the structured entities
    if (Zone_Type == Structured) {
      // printf("WARNING: create a tensorial mesh from the structured entities (Structured may be not implemented) \n");
      if (Phys_Dim == 3) {
        *NbrQua += 2 * (Zone_Size[0][0] - 1) * (Zone_Size[0][1] - 1) + 2 * (Zone_Size[0][0] - 1) * (Zone_Size[0][2] - 1) + 2 * (Zone_Size[0][1] - 1) * (Zone_Size[0][2] - 1);
        *NbrHex += (Zone_Size[0][0] - 1) * (Zone_Size[0][1] - 1) * (Zone_Size[0][2] - 1);
      }
      else {
        *NbrQua += (Zone_Size[0][0] - 1) * (Zone_Size[0][1] - 1);
        *NbrEdg += 2 * (Zone_Size[0][0] - 1) + 2 * (Zone_Size[0][1] - 1);
      }
    }
    else {

      CG_Err = cg_nsections(File_Index, Base_Index, Zone_Index,
          &Number_of_Sections);

      if (CG_Err != CG_OK && Mode == 0)
        printf("  %%%% Warning in CGNS File Reader in cg_nsections: %s\n", cg_get_error());

      if (Number_of_Sections <= 0) {
        cg_close(File_Index);
        viz_writestatus(status, " no element sections are specified in CGNS mesh file");
        return VIZINT_ERROR;
      }
      //-- count number of entities per section => handle the case of mixed elements as well.
      for (Section_Index = 1; Section_Index <= Number_of_Sections; ++Section_Index) {
        CG_Err = cg_section_read(File_Index, Base_Index, Zone_Index,
            Section_Index, Label, &Element_Type,
            &Start_Index, &End_Index,
            &Number_of_Boundary_Items, &Parent_Flag);

        if (CG_Err != CG_OK && Mode == 0)
          printf("  %%%% Warning in CGNS File Reader in cg_section_read: %s\n", cg_get_error());

        if (Element_Type == NODE)
          *NbrCor = *NbrCor + End_Index - Start_Index + 1;

        else if (Element_Type == BAR_2)
          *NbrEdg = *NbrEdg + End_Index - Start_Index + 1;

        else if (Element_Type == TRI_3)
          *NbrTri = *NbrTri + End_Index - Start_Index + 1;

        else if (Element_Type == QUAD_4)
          *NbrQua = *NbrQua + End_Index - Start_Index + 1;

        else if (Element_Type == TETRA_4)
          *NbrTet = *NbrTet + End_Index - Start_Index + 1;

        else if (Element_Type == PYRA_5)
          *NbrPyr = *NbrPyr + End_Index - Start_Index + 1;

        else if (Element_Type == PENTA_6)
          *NbrPri = *NbrPri + End_Index - Start_Index + 1;

        else if (Element_Type == HEXA_8)
          *NbrHex = *NbrHex + End_Index - Start_Index + 1;

        else if (Element_Type == BAR_3)
          *NbrP2Edg = *NbrP2Edg + End_Index - Start_Index + 1;

        else if (Element_Type == TRI_6)
          *NbrP2Tri = *NbrP2Tri + End_Index - Start_Index + 1;

        else if (Element_Type == QUAD_9)
          *NbrQ2Qua = *NbrQ2Qua + End_Index - Start_Index + 1;

        else if (Element_Type == TETRA_10)
          *NbrP2Tet = *NbrP2Tet + End_Index - Start_Index + 1;

        else if (Element_Type == PYRA_14)
          *NbrP2Pyr = *NbrP2Pyr + End_Index - Start_Index + 1;

        else if (Element_Type == PENTA_18)
          *NbrP2Pri = *NbrP2Pri + End_Index - Start_Index + 1;

        else if (Element_Type == HEXA_27)
          *NbrQ2Hex = *NbrQ2Hex + End_Index - Start_Index + 1;

        else if (Element_Type == BAR_4)
          *NbrP3Edg = *NbrP3Edg + End_Index - Start_Index + 1;

        else if (Element_Type == TRI_10)
          *NbrP3Tri = *NbrP3Tri + End_Index - Start_Index + 1;

        else if (Element_Type == QUAD_16)
          *NbrQ3Qua = *NbrQ3Qua + End_Index - Start_Index + 1;

        else if (Element_Type == TETRA_20)
          *NbrP3Tet = *NbrP3Tet + End_Index - Start_Index + 1;

        else if (Element_Type == PYRA_30)
          *NbrP3Pyr = *NbrP3Pyr + End_Index - Start_Index + 1;

        else if (Element_Type == PENTA_40)
          *NbrP3Pri = *NbrP3Pri + End_Index - Start_Index + 1;

        else if (Element_Type == HEXA_64)
          *NbrQ3Hex = *NbrQ3Hex + End_Index - Start_Index + 1;

        else if (Element_Type == BAR_5)
          *NbrP4Edg = *NbrP4Edg + End_Index - Start_Index + 1;

        else if (Element_Type == TRI_15)
          *NbrP4Tri = *NbrP4Tri + End_Index - Start_Index + 1;

        else if (Element_Type == QUAD_25)
          *NbrQ4Qua = *NbrQ4Qua + End_Index - Start_Index + 1;

        else if (Element_Type == TETRA_35)
          *NbrP4Tet = *NbrP4Tet + End_Index - Start_Index + 1;

        else if (Element_Type == PYRA_55)
          *NbrP4Pyr = *NbrP4Pyr + End_Index - Start_Index + 1;

        else if (Element_Type == PENTA_75)
          *NbrP4Pri = *NbrP4Pri + End_Index - Start_Index + 1;

        else if (Element_Type == HEXA_125)
          *NbrQ4Hex = *NbrQ4Hex + End_Index - Start_Index + 1;

        else if (Element_Type == MIXED) {
          NbrElt = End_Index - Start_Index + 1;
          CG_Err = cg_ElementDataSize(File_Index, Base_Index, Zone_Index, Section_Index, &size);

          if (CG_Err != CG_OK && Mode == 0)
            printf("  %%%% Warning in CGNS File Reader in cg_ElementDataSize: %s\n", cg_get_error());

          Connectivity = (cgsize_t*)malloc((size_t)size * sizeof(cgsize_t));

          ConnectOffset = (cgsize_t*)malloc((size_t)(NbrElt + 1) * sizeof(cgsize_t));

          CG_Err = cg_poly_elements_read(File_Index, Base_Index, Zone_Index, Section_Index, Connectivity, ConnectOffset, Parent_Data);

          if (CG_Err != CG_OK && Mode == 0)
            printf("  %%%% Warning in CGNS File Reader in cg_poly_elements_read: %s\n", cg_get_error());

          for (i = 0, n = 0; n < NbrElt; n++) {
            SubElement_Type = Connectivity[i++];
            switch (SubElement_Type) {
              case TRI_3:
                (*NbrTri)++;
                break;
              case QUAD_4:
                (*NbrQua)++;
                break;
              case TETRA_4:
                (*NbrTet)++;
                break;
              case PYRA_5:
                (*NbrPyr)++;
                break;
              case PENTA_6:
                (*NbrPri)++;
                break;
              case HEXA_8:
                (*NbrHex)++;
                break;
              case BAR_2:
                (*NbrEdg)++;
                break;
              case BAR_3:
                (*NbrP2Edg)++;
                break;
              case TRI_6:
                (*NbrP2Tri)++;
                break;
              case QUAD_9:
                (*NbrQ2Qua)++;
                break;
              case TETRA_10:
                (*NbrP2Tet)++;
                break;
              case PYRA_14:
                (*NbrP2Pyr)++;
                break;
              case PENTA_18:
                (*NbrP2Pri)++;
                break;
              case HEXA_27:
                (*NbrQ2Hex)++;
                break;
              case BAR_4:
                (*NbrP3Edg)++;
                break;
              case TRI_10:
                (*NbrP3Tri)++;
                break;
              case QUAD_16:
                (*NbrQ3Qua)++;
                break;
              case TETRA_20:
                (*NbrP3Tet)++;
                break;
              case PYRA_30:
                (*NbrP3Pyr)++;
                break;
              case PENTA_40:
                (*NbrP3Pri)++;
                break;
              case HEXA_64:
                (*NbrQ3Hex)++;
                break;
              case BAR_5:
                (*NbrP4Edg)++;
                break;
              case TRI_15:
                (*NbrP4Tri)++;
                break;
              case QUAD_25:
                (*NbrQ4Qua)++;
                break;
              case TETRA_35:
                (*NbrP4Tet)++;
                break;
              case PYRA_55:
                (*NbrP4Pyr)++;
                break;
              case PENTA_75:
                (*NbrP4Pri)++;
                break;
              case HEXA_125:
                (*NbrQ4Hex)++;
                break;
              case NODE:
                (*NbrCor)++;
                break;
              default:
                break;
            }
            CG_Err = cg_npe(SubElement_Type, &NbrNod);
            if (NbrNod <= 0) {
              printf("  %%%% Warning in CGNS File Reader: Number of Nodes invalid for element %s\n", ElementTypeName[SubElement_Type]);
              break;
            }
            i += NbrNod;
          }

          if (Connectivity) {
            free(Connectivity);
            Connectivity = NULL;
          }
          if (ConnectOffset) {
            free(ConnectOffset);
            ConnectOffset = NULL;
          }
        }
      }
      Number_of_Elems_Faces = *NbrTet - NbrTet0 + *NbrPyr - NbrPyr0 + *NbrPri - NbrPri0 + *NbrHex - NbrHex0 + *NbrP2Tet - NbrP2Tet0 + *NbrP2Pyr - NbrP2Pyr0 + *NbrP2Pri - NbrP2Pri0 + *NbrQ2Hex - NbrQ2Hex0 + *NbrP3Tet - NbrP3Tet0 + *NbrP3Pyr - NbrP3Pyr0 + *NbrP3Pri - NbrP3Pri0 + *NbrQ3Hex - NbrQ3Hex0 + *NbrP4Tet - NbrP4Tet0 + *NbrP4Pyr - NbrP4Pyr0 + *NbrP4Pri - NbrP4Pri0 + *NbrQ4Hex - NbrQ4Hex0;

      Number_of_Zone_Elems_Faces = Zone_Size[0][1];

      if (Number_of_Elems_Faces != Number_of_Zone_Elems_Faces && Mode == 0) {
        printf("  %%%% Warning in CGNS File Reader: number of elements in zone %d not equal to number in CGNS mesh file, %ld != %ld \n", Zone_Index, Number_of_Elems_Faces, Number_of_Zone_Elems_Faces);
      }
    }
    //-- skip the fill in of the arrays in the case of the count
    if (Mode == 0)
      continue;

    //-- here fill in arrays that are assumed to be allocated.

    CG_Err = 0;

    //--- coordinates
    Array_Size = (NbrVerZone) * (int)sizeof(double);

    Coordinate = (double*)malloc(Array_Size);

    if (Zone_Type == Unstructured) {
      min_Index[0] = 1;
      max_Index[0] = NbrVerZone;
    }
    else {
      for (i = 0; i < *Dim; i++) {
        min_Index[i] = 1;
        max_Index[i] = Zone_Size[0][i];
      }
    }
    CG_Err = cg_coord_read(File_Index, Base_Index, Zone_Index,
        "CoordinateX", RealDouble,
        min_Index, max_Index, Coordinate);

    // printf("File_Index %d \n", File_Index);
    // printf("Base_Index %d \n", Base_Index);
    // printf("Zone_Index %d \n", Zone_Index);

    if (CG_Err != CG_OK)
      printf("  %%%% Warning in CGNS File Reader in cg_coord_read: %s\n", cg_get_error());

    for (Index = 1; Index <= NbrVerZone; ++Index) {
      Crd[NbrVer0 + Index][0] = Coordinate[Index - 1];
    }

    if (Zone_Type == Unstructured) {
      min_Index[0] = 1;
      max_Index[0] = NbrVerZone;
    }
    else {
      for (i = 0; i < *Dim; i++) {
        min_Index[i] = 1;
        max_Index[i] = Zone_Size[0][i];
      }
    }
    CG_Err = cg_coord_read(File_Index, Base_Index, Zone_Index,
        "CoordinateY", RealDouble,
        min_Index, max_Index, Coordinate);

    if (CG_Err != CG_OK)
      printf("  %%%% Warning in CGNS File Reader in cg_coord_read: %s\n", cg_get_error());

    for (Index = 1; Index <= NbrVerZone; ++Index) {
      Crd[NbrVer0 + Index][1] = Coordinate[Index - 1];
    }

    if (*Dim == 3) {
      if (Zone_Type == Unstructured) {
        min_Index[0] = 1;
        max_Index[0] = NbrVerZone;
      }
      else {
        for (i = 0; i < *Dim; i++) {
          min_Index[i] = 1;
          max_Index[i] = Zone_Size[0][i];
        }
      }

      CG_Err = cg_coord_read(File_Index, Base_Index, Zone_Index,
          "CoordinateZ", RealDouble,
          min_Index, max_Index, Coordinate);

      if (CG_Err != CG_OK)
        printf("  %%%% Warning in CGNS File Reader in cg_coord_read: %s\n", cg_get_error());

      for (Index = 1; Index <= NbrVerZone; ++Index) {
        Crd[NbrVer0 + Index][2] = Coordinate[Index - 1];
      }
    }
    else {
      for (Index = 1; Index <= NbrVerZone; ++Index) {
        Crd[NbrVer0 + Index][2] = 0.;
      }
    }

    if (Coordinate) {
      free(Coordinate);
      Coordinate = NULL;
    }

    //--- elements => distinguish structured and unstructured.
    if (Zone_Type == Unstructured) {
      for (Section_Index = 1; Section_Index <= Number_of_Sections; ++Section_Index) {
        CG_Err = cg_section_read(File_Index, Base_Index, Zone_Index,
            Section_Index, Label, &Element_Type,
            &Start_Index, &End_Index,
            &Number_of_Boundary_Items, &Parent_Flag);

        nbrElt = End_Index - Start_Index + 1;

        if (CG_Err != CG_OK)
          printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

        if (Element_Type == NODE) {
          Connectivity = (cgsize_t*)malloc(nbrElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: Unstructured %s\n", cg_get_error());

          Index_1 = NbrCor0 + 1;
          Index_2 = NbrCor0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index)
            Cor[Index] = Connectivity[Index - NbrCor0 - 1] + NbrVer0;

          NbrCor0 = Index_2;
        }
        else if (Element_Type == BAR_2) {
          sizElt = 2;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfEdg_ID;

          Index_1 = NbrEdg0 + 1;
          Index_2 = NbrEdg0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            EdgRef[Index] = SurfEdg_ID;
            for (int j = 0; j <= sizElt; ++j) {
              Edg[Index][j] = Connectivity[(Index - NbrEdg0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrEdg0 = Index_2;
        }
        else if (Element_Type == TRI_3) {
          sizElt = 3;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: TRI_3 %s\n", cg_get_error());

          ++SurfTri_ID;

          Index_1 = NbrTri0 + 1;
          Index_2 = NbrTri0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            TriRef[Index] = SurfTri_ID;
            for (int j = 0; j <= sizElt; ++j) {
              Tri[Index][j] = Connectivity[(Index - NbrTri0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrTri0 = Index_2;
        }

        else if (Element_Type == QUAD_4) {
          sizElt = 4;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfQua_ID;

          Index_1 = NbrQua0 + 1;
          Index_2 = NbrQua0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            QuaRef[Index] = SurfQua_ID;
            for (int j = 0; j <= sizElt; ++j) {
              Qua[Index][j] = Connectivity[(Index - NbrQua0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrQua0 = Index_2;
        }

        else if (Element_Type == TETRA_4) {
          if (*NbrTet > 0) {
            sizElt = 4;

            Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

            CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
                Section_Index, Connectivity, Parent_Data);

            if (CG_Err != CG_OK)
              printf("  %%%% Warning in CGNS File Reader: TETRA_4 %s\n", cg_get_error());

            Index_1 = NbrTet0 + 1;
            Index_2 = NbrTet0 + End_Index - Start_Index + 1;

            for (Index = Index_1; Index <= Index_2; ++Index) {
              TetRef[Index] = Zone_Index;
              for (int j = 0; j <= sizElt; ++j) {
                Tet[Index][j] = Connectivity[(Index - NbrTet0 - 1) * sizElt + j] + NbrVer0;
              }
            }

            NbrTet0 = Index_2;
          }
        }

        else if (Element_Type == PYRA_5) {
          if (*NbrPyr > 0) {
            sizElt = 5;

            Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

            CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
                Section_Index, Connectivity, Parent_Data);

            if (CG_Err != CG_OK)
              printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

            Index_1 = NbrPyr0 + 1;
            Index_2 = NbrPyr0 + End_Index - Start_Index + 1;

            for (Index = Index_1; Index <= Index_2; ++Index) {
              PyrRef[Index] = Zone_Index;
              for (int j = 0; j <= sizElt; ++j) {
                Pyr[Index][j] = Connectivity[(Index - NbrPyr0 - 1) * sizElt + j] + NbrVer0;
              }
            }

            NbrPyr0 = Index_2;
          }
        }

        else if (Element_Type == PENTA_6) {
          if (*NbrPri > 0) {
            sizElt = 6;

            Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

            CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
                Section_Index, Connectivity, Parent_Data);

            if (CG_Err != CG_OK)
              printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

            Index_1 = NbrPri0 + 1;
            Index_2 = NbrPri0 + End_Index - Start_Index + 1;

            for (Index = Index_1; Index <= Index_2; ++Index) {
              PriRef[Index] = Zone_Index;
              for (int j = 0; j <= sizElt; ++j) {
                Pri[Index][j] = Connectivity[(Index - NbrPri0 - 1) * sizElt + j] + NbrVer0;
              }
            }

            NbrPri0 = Index_2;
          }
        }

        else if (Element_Type == HEXA_8) {
          if (*NbrHex > 0) {
            sizElt = 8;

            Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

            CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
                Section_Index, Connectivity, Parent_Data);

            if (CG_Err != CG_OK)
              printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

            Index_1 = NbrHex0 + 1;
            Index_2 = NbrHex0 + End_Index - Start_Index + 1;

            for (Index = Index_1; Index <= Index_2; ++Index) {
              HexRef[Index] = Zone_Index;
              for (int j = 0; j <= sizElt; ++j) {
                Hex[Index][j] = Connectivity[(Index - NbrHex0 - 1) * sizElt + j] + NbrVer0;
              }
            }

            NbrHex0 = Index_2;
          }
        }

        else if (Element_Type == TRI_6) {
          sizElt = 6;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP2Tri_ID;

          Index_1 = NbrP2Tri0 + 1;
          Index_2 = NbrP2Tri0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P2TriRef[Index] = SurfP2Tri_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P2Tri[Index][j] = Connectivity[(Index - NbrP2Tri0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP2Tri0 = Index_2;
        }

        else if (Element_Type == BAR_3) {
          sizElt = 3;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP2Edg_ID;

          Index_1 = NbrP2Edg0 + 1;
          Index_2 = NbrP2Edg0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P2EdgRef[Index] = SurfP2Edg_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P2Edg[Index][j] = Connectivity[(Index - NbrP2Edg0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP2Edg0 = Index_2;
        }

        else if (Element_Type == QUAD_9) {
          sizElt = 9;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfQ2Qua_ID;

          Index_1 = NbrQ2Qua0 + 1;
          Index_2 = NbrQ2Qua0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            Q2QuaRef[Index] = SurfQ2Qua_ID;
            for (int j = 0; j <= sizElt; ++j) {
              Q2Qua[Index][j] = Connectivity[(Index - NbrQ2Qua0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrQ2Qua0 = Index_2;
        }

        else if (Element_Type == TETRA_10) {
          sizElt = 10;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP2Tet_ID;

          Index_1 = NbrP2Tet0 + 1;
          Index_2 = NbrP2Tet0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P2TetRef[Index] = SurfP2Tet_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P2Tet[Index][j] = Connectivity[(Index - NbrP2Tet0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP2Tet0 = Index_2;
        }

        else if (Element_Type == PYRA_14) {
          sizElt = 14;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP2Pyr_ID;

          Index_1 = NbrP2Pyr0 + 1;
          Index_2 = NbrP2Pyr0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P2PyrRef[Index] = SurfP2Pyr_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P2Pyr[Index][j] = Connectivity[(Index - NbrP2Pyr0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP2Pyr0 = Index_2;
        }

        else if (Element_Type == PENTA_18) {
          sizElt = 18;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP2Pri_ID;

          Index_1 = NbrP2Pri0 + 1;
          Index_2 = NbrP2Pri0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P2PriRef[Index] = SurfP2Pri_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P2Pri[Index][j] = Connectivity[(Index - NbrP2Pri0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP2Pri0 = Index_2;
        }

        else if (Element_Type == HEXA_27) {
          sizElt = 27;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfQ2Hex_ID;

          Index_1 = NbrQ2Hex0 + 1;
          Index_2 = NbrQ2Hex0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            Q2HexRef[Index] = SurfQ2Hex_ID;
            for (int j = 0; j <= sizElt; ++j) {
              Q2HexTmp[j] = Connectivity[(Index - NbrQ2Hex0 - 1) * sizElt + j] + NbrVer0;
            }
            //-- renumbering
            for (ii = 0; ii < 12; ++ii)
              Q2Hex[Index][ii] = Q2HexTmp[ii];
            Q2Hex[Index][12] = Q2HexTmp[16];
            Q2Hex[Index][13] = Q2HexTmp[17];
            Q2Hex[Index][14] = Q2HexTmp[18];
            Q2Hex[Index][15] = Q2HexTmp[19];
            Q2Hex[Index][16] = Q2HexTmp[12];
            Q2Hex[Index][17] = Q2HexTmp[13];
            Q2Hex[Index][18] = Q2HexTmp[14];
            Q2Hex[Index][19] = Q2HexTmp[15];
            Q2Hex[Index][20] = Q2HexTmp[20];
            Q2Hex[Index][21] = Q2HexTmp[25];
            Q2Hex[Index][22] = Q2HexTmp[21];
            Q2Hex[Index][23] = Q2HexTmp[22];
            Q2Hex[Index][24] = Q2HexTmp[23];
            Q2Hex[Index][25] = Q2HexTmp[24];
            Q2Hex[Index][26] = Q2HexTmp[26];
          }

          NbrQ2Hex0 = Index_2;
        }

        else if (Element_Type == BAR_4) {
          sizElt = 4;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP3Edg_ID;

          Index_1 = NbrP3Edg0 + 1;
          Index_2 = NbrP3Edg0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P3EdgRef[Index] = SurfP3Edg_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P3Edg[Index][j] = Connectivity[(Index - NbrP3Edg0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP3Edg0 = Index_2;
        }

        else if (Element_Type == TRI_10) {
          sizElt = 10;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP3Tri_ID;

          Index_1 = NbrP3Tri0 + 1;
          Index_2 = NbrP3Tri0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P3TriRef[Index] = SurfP3Tri_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P3Tri[Index][j] = Connectivity[(Index - NbrP3Tri0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP3Tri0 = Index_2;
        }

        else if (Element_Type == QUAD_16) {
          sizElt = 16;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfQ3Qua_ID;

          Index_1 = NbrQ3Qua0 + 1;
          Index_2 = NbrQ3Qua0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            Q3QuaRef[Index] = SurfQ3Qua_ID;
            for (int j = 0; j <= sizElt; ++j) {
              Q3Qua[Index][j] = Connectivity[(Index - NbrQ3Qua0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrQ3Qua0 = Index_2;
        }

        else if (Element_Type == TETRA_20) {
          sizElt = 20;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP3Tet_ID;

          Index_1 = NbrP3Tet0 + 1;
          Index_2 = NbrP3Tet0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P3TetRef[Index] = SurfP3Tet_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P3Tet[Index][j] = Connectivity[(Index - NbrP3Tet0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP3Tet0 = Index_2;
        }

        else if (Element_Type == PYRA_30) {
          sizElt = 30;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP3Pyr_ID;

          Index_1 = NbrP3Pyr0 + 1;
          Index_2 = NbrP3Pyr0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P3PyrRef[Index] = SurfP3Pyr_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P3Pyr[Index][j] = Connectivity[(Index - NbrP3Pyr0 - 1) * sizElt + j] + NbrVer0;
            }
          }
          printf("Warning: numbering for PYRA_30 has not been checked yet \n");

          NbrP3Pyr0 = Index_2;
        }

        else if (Element_Type == PENTA_40) {
          sizElt = 40;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP3Pri_ID;

          Index_1 = NbrP3Pri0 + 1;
          Index_2 = NbrP3Pri0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P3PriRef[Index] = SurfP3Pri_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P3Pri[Index][j] = Connectivity[(Index - NbrP3Pri0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          printf("Warning: numbering for PENTA_40 has not been checked yet \n");

          NbrP3Pri0 = Index_2;
        }

        else if (Element_Type == HEXA_64) {
          sizElt = 64;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfQ3Hex_ID;

          Index_1 = NbrQ3Hex0 + 1;
          Index_2 = NbrQ3Hex0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            Q3HexRef[Index] = SurfQ3Hex_ID;
            for (int j = 0; j <= sizElt; ++j) {
              Q3Hex[Index][j] = Connectivity[(Index - NbrQ3Hex0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          printf("Warning: numbering for HEXA_64 has not been checked yet \n");

          NbrQ3Hex0 = Index_2;
        }

        else if (Element_Type == BAR_5) {
          sizElt = 5;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP4Edg_ID;

          Index_1 = NbrP4Edg0 + 1;
          Index_2 = NbrP4Edg0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P4EdgRef[Index] = SurfP4Edg_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P4Edg[Index][j] = Connectivity[(Index - NbrP4Edg0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP4Edg0 = Index_2;
        }

        else if (Element_Type == TRI_15) {
          sizElt = 15;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP4Tri_ID;

          Index_1 = NbrP4Tri0 + 1;
          Index_2 = NbrP4Tri0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P4TriRef[Index] = SurfP4Tri_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P4Tri[Index][j] = Connectivity[(Index - NbrP4Tri0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP4Tri0 = Index_2;
        }

        else if (Element_Type == QUAD_25) {
          sizElt = 25;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfQ4Qua_ID;

          Index_1 = NbrQ4Qua0 + 1;
          Index_2 = NbrQ4Qua0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            Q4QuaRef[Index] = SurfQ4Qua_ID;
            for (int j = 0; j <= sizElt; ++j) {
              Q4Qua[Index][j] = Connectivity[(Index - NbrQ4Qua0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          printf("Warning: numbering for QUAD_25 has not been checked yet \n");

          NbrQ4Qua0 = Index_2;
        }

        else if (Element_Type == TETRA_35) {
          sizElt = 35;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP4Tet_ID;

          Index_1 = NbrP4Tet0 + 1;
          Index_2 = NbrP4Tet0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P4TetRef[Index] = SurfP4Tet_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P4Tet[Index][j] = Connectivity[(Index - NbrP4Tet0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          NbrP4Tet0 = Index_2;
        }

        else if (Element_Type == PYRA_55) {
          sizElt = 55;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP4Pyr_ID;

          Index_1 = NbrP4Pyr0 + 1;
          Index_2 = NbrP4Pyr0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P4PyrRef[Index] = SurfP4Pyr_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P4Pyr[Index][j] = Connectivity[(Index - NbrP4Pyr0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          printf("Warning: numbering for PYRA_55 has not been checked yet \n");

          NbrP4Pyr0 = Index_2;
        }

        else if (Element_Type == PENTA_75) {
          sizElt = 75;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfP4Pri_ID;

          Index_1 = NbrP4Pri0 + 1;
          Index_2 = NbrP4Pri0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            P4PriRef[Index] = SurfP4Pri_ID;
            for (int j = 0; j <= sizElt; ++j) {
              P4Pri[Index][j] = Connectivity[(Index - NbrP4Pri0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          printf("Warning: numbering for PENTA_75 has not been checked yet \n");

          NbrP4Pri0 = Index_2;
        }

        else if (Element_Type == HEXA_125) {
          sizElt = 125;

          Connectivity = (cgsize_t*)malloc((nbrElt + 1) * sizElt * sizeof(cgsize_t));

          CG_Err = cg_elements_read(File_Index, Base_Index, Zone_Index,
              Section_Index, Connectivity, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          ++SurfQ4Hex_ID;

          Index_1 = NbrQ4Hex0 + 1;
          Index_2 = NbrQ4Hex0 + End_Index - Start_Index + 1;

          for (Index = Index_1; Index <= Index_2; ++Index) {
            Q4HexRef[Index] = SurfQ4Hex_ID;
            for (int j = 0; j <= sizElt; ++j) {
              Q4Hex[Index][j] = Connectivity[(Index - NbrQ4Hex0 - 1) * sizElt + j] + NbrVer0;
            }
          }

          printf("Warning: numbering for HEXA_125 has not been checked yet \n");

          NbrQ4Hex0 = Index_2;
        }

        else if (Element_Type == MIXED) {
          NbrElt = End_Index - Start_Index + 1;
          CG_Err = cg_ElementDataSize(File_Index, Base_Index, Zone_Index, Section_Index, &size);

          if (CG_Err != CG_OK && Mode == 0)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          Connectivity = (cgsize_t*)malloc((size_t)size * sizeof(cgsize_t));

          ConnectOffset = (cgsize_t*)malloc((size_t)(NbrElt + 1) * sizeof(cgsize_t));

          CG_Err = cg_poly_elements_read(File_Index, Base_Index, Zone_Index, Section_Index, Connectivity, ConnectOffset, Parent_Data);

          if (CG_Err != CG_OK && Mode == 0)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          for (i = 0, n = 0; n < NbrElt; n++) {
            SubElement_Type = Connectivity[i++];
            CG_Err          = cg_npe(SubElement_Type, &NbrNod);
            switch (SubElement_Type) {
              case NODE:
                NbrCor0++;
                Cor[NbrCor0] = NbrVer0 + Connectivity[i];
                break;
              case BAR_2:
                NbrEdg0++;
                Edg[NbrEdg0][0] = NbrVer0 + Connectivity[i];
                Edg[NbrEdg0][1] = NbrVer0 + Connectivity[i + 1];
                EdgRef[NbrEdg0] = -Zone_Index;
                break;
              case TRI_3:
                NbrTri0++;
                Tri[NbrTri0][0] = NbrVer0 + Connectivity[i];
                Tri[NbrTri0][1] = NbrVer0 + Connectivity[i + 1];
                Tri[NbrTri0][2] = NbrVer0 + Connectivity[i + 2];
                TriRef[NbrTri0] = -Zone_Index;
                break;
              case QUAD_4:
                NbrQua0++;
                Qua[NbrQua0][0] = NbrVer0 + Connectivity[i];
                Qua[NbrQua0][1] = NbrVer0 + Connectivity[i + 1];
                Qua[NbrQua0][2] = NbrVer0 + Connectivity[i + 2];
                Qua[NbrQua0][3] = NbrVer0 + Connectivity[i + 3];
                QuaRef[NbrQua0] = -Zone_Index;
                break;
              case TETRA_4:
                NbrTet0++;
                Tet[NbrTet0][0] = NbrVer0 + Connectivity[i];
                Tet[NbrTet0][1] = NbrVer0 + Connectivity[i + 1];
                Tet[NbrTet0][2] = NbrVer0 + Connectivity[i + 2];
                Tet[NbrTet0][3] = NbrVer0 + Connectivity[i + 3];
                TetRef[NbrTet0] = Zone_Index;
                break;
              case PYRA_5:
                NbrPyr0++;
                Pyr[NbrPyr0][0] = NbrVer0 + Connectivity[i];
                Pyr[NbrPyr0][1] = NbrVer0 + Connectivity[i + 1];
                Pyr[NbrPyr0][2] = NbrVer0 + Connectivity[i + 2];
                Pyr[NbrPyr0][3] = NbrVer0 + Connectivity[i + 3];
                Pyr[NbrPyr0][4] = NbrVer0 + Connectivity[i + 4];
                PyrRef[NbrPyr0] = Zone_Index;
                break;
              case PENTA_6:
                NbrPri0++;
                Pri[NbrPri0][0] = NbrVer0 + Connectivity[i];
                Pri[NbrPri0][1] = NbrVer0 + Connectivity[i + 1];
                Pri[NbrPri0][2] = NbrVer0 + Connectivity[i + 2];
                Pri[NbrPri0][3] = NbrVer0 + Connectivity[i + 3];
                Pri[NbrPri0][4] = NbrVer0 + Connectivity[i + 4];
                Pri[NbrPri0][5] = NbrVer0 + Connectivity[i + 5];
                PriRef[NbrPri0] = Zone_Index;
                break;
              case HEXA_8:
                NbrHex0++;
                Hex[NbrHex0][0] = NbrVer0 + Connectivity[i];
                Hex[NbrHex0][1] = NbrVer0 + Connectivity[i + 1];
                Hex[NbrHex0][2] = NbrVer0 + Connectivity[i + 2];
                Hex[NbrHex0][3] = NbrVer0 + Connectivity[i + 3];
                Hex[NbrHex0][4] = NbrVer0 + Connectivity[i + 4];
                Hex[NbrHex0][5] = NbrVer0 + Connectivity[i + 5];
                Hex[NbrHex0][6] = NbrVer0 + Connectivity[i + 6];
                Hex[NbrHex0][7] = NbrVer0 + Connectivity[i + 7];
                HexRef[NbrHex0] = Zone_Index;
                break;
              case BAR_3:
                NbrP2Edg0++;
                for (ii = 0; ii < 3; ++ii)
                  P2Edg[NbrP2Edg0][ii] += NbrVer0 + Connectivity[i + ii];
                P2EdgRef[NbrP2Edg0] = -Zone_Index;
                break;
              case TRI_6:
                NbrP2Tri0++;
                for (ii = 0; ii < 6; ++ii)
                  P2Tri[NbrP2Tri0][ii] += NbrVer0 + Connectivity[i + ii];
                P2TriRef[NbrP2Tri0] = -Zone_Index;
                break;
              case QUAD_9:
                NbrQ2Qua0++;
                for (ii = 0; ii < 9; ++ii)
                  Q2Qua[NbrQ2Qua0][ii] += NbrVer0 + Connectivity[i + ii];
                Q2QuaRef[NbrQ2Qua0] = -Zone_Index;
                break;
              case TETRA_10:
                NbrP2Tet0++;
                for (ii = 0; ii < 10; ++ii)
                  P2Tet[NbrP2Tet0][ii] += NbrVer0 + Connectivity[i + ii];
                P2TetRef[NbrP2Tet0] = Zone_Index;
                break;
              case PYRA_14:
                NbrP2Pyr0++;
                for (ii = 0; ii < 14; ++ii)
                  P2Pyr[NbrP2Pyr0][ii] += NbrVer0 + Connectivity[i + ii];
                P2PyrRef[NbrP2Pyr0] = Zone_Index;
                break;
              case PENTA_18:
                NbrP2Pri0++;
                for (ii = 0; ii < 18; ++ii)
                  P2Pri[NbrP2Pri0][ii] += NbrVer0 + Connectivity[i + ii];
                P2PriRef[NbrP2Pri0] = Zone_Index;
                break;
              case HEXA_27:
                NbrQ2Hex0++;
                for (ii = 0; ii < 12; ++ii)
                  Q2Hex[NbrQ2Hex0][ii] += NbrVer0 + Connectivity[i + ii];
                Q2Hex[NbrQ2Hex0][12] += NbrVer0 + Connectivity[i + 16];
                Q2Hex[NbrQ2Hex0][13] += NbrVer0 + Connectivity[i + 17];
                Q2Hex[NbrQ2Hex0][14] += NbrVer0 + Connectivity[i + 18];
                Q2Hex[NbrQ2Hex0][15] += NbrVer0 + Connectivity[i + 19];
                Q2Hex[NbrQ2Hex0][16] += NbrVer0 + Connectivity[i + 12];
                Q2Hex[NbrQ2Hex0][17] += NbrVer0 + Connectivity[i + 13];
                Q2Hex[NbrQ2Hex0][18] += NbrVer0 + Connectivity[i + 14];
                Q2Hex[NbrQ2Hex0][19] += NbrVer0 + Connectivity[i + 15];
                Q2Hex[NbrQ2Hex0][20] += NbrVer0 + Connectivity[i + 20];
                Q2Hex[NbrQ2Hex0][21] += NbrVer0 + Connectivity[i + 25];
                Q2Hex[NbrQ2Hex0][22] += NbrVer0 + Connectivity[i + 21];
                Q2Hex[NbrQ2Hex0][23] += NbrVer0 + Connectivity[i + 22];
                Q2Hex[NbrQ2Hex0][24] += NbrVer0 + Connectivity[i + 23];
                Q2Hex[NbrQ2Hex0][25] += NbrVer0 + Connectivity[i + 24];
                Q2Hex[NbrQ2Hex0][26] += NbrVer0 + Connectivity[i + 26];

                Q2HexRef[NbrQ2Hex0] = Zone_Index;
                break;
              case BAR_4:
                NbrP3Edg0++;
                for (ii = 0; ii < 4; ++ii)
                  P3Edg[NbrP3Edg0][ii] += NbrVer0 + Connectivity[i + ii];
                P3EdgRef[NbrP3Edg0] = -Zone_Index;
                break;
              case TRI_10:
                NbrP3Tri0++;
                for (ii = 0; ii < 10; ++ii)
                  P3Tri[NbrP3Tri0][ii] += NbrVer0 + Connectivity[i + ii];
                P3TriRef[NbrP3Tri0] = -Zone_Index;
                break;
              case QUAD_16:
                NbrQ3Qua0++;
                for (ii = 0; ii < 16; ++ii)
                  Q3Qua[NbrQ3Qua0][ii] += NbrVer0 + Connectivity[i + ii];
                Q3QuaRef[NbrQ3Qua0] = -Zone_Index;
                break;
              case TETRA_20:
                NbrP3Tet0++;
                for (ii = 0; ii < 20; ++ii)
                  P3Tet[NbrP3Tet0][ii] += NbrVer0 + Connectivity[i + ii];
                P3TetRef[NbrP3Tet0] = Zone_Index;
                break;
              case PYRA_30:
                NbrP3Pyr0++;
                for (ii = 0; ii < 30; ++ii)
                  P3Pyr[NbrP3Pyr0][ii] += NbrVer0 + Connectivity[i + ii];
                P3PyrRef[NbrP3Pyr0] = Zone_Index;
                break;
              case PENTA_40:
                NbrP3Pri0++;
                for (ii = 0; ii < 40; ++ii)
                  P3Pri[NbrP3Pri0][ii] += NbrVer0 + Connectivity[i + ii];
                P3PriRef[NbrP3Pri0] = Zone_Index;
                break;
              case HEXA_64:
                NbrQ3Hex0++;
                for (ii = 0; ii < 64; ++ii)
                  Q3Hex[NbrQ3Hex0][ii] += NbrVer0 + Connectivity[i + ii];
                Q3HexRef[NbrQ3Hex0] = Zone_Index;
                break;
              case BAR_5:
                NbrP4Edg0++;
                for (ii = 0; ii < 5; ++ii)
                  P4Edg[NbrP4Edg0][ii] += NbrVer0 + Connectivity[i + ii];
                P4EdgRef[NbrP4Edg0] = -Zone_Index;
                break;
              case TRI_15:
                NbrP4Tri0++;
                for (ii = 0; ii < 15; ++ii)
                  P4Tri[NbrP4Tri0][ii] += NbrVer0 + Connectivity[i + ii];
                P4TriRef[NbrP4Tri0] = -Zone_Index;
                break;
              case QUAD_25:
                NbrQ4Qua0++;
                for (ii = 0; ii < 25; ++ii)
                  Q4Qua[NbrQ4Qua0][ii] += NbrVer0 + Connectivity[i + ii];
                Q4QuaRef[NbrQ4Qua0] = -Zone_Index;
                break;
              case TETRA_35:
                NbrP4Tet0++;
                for (ii = 0; ii < 35; ++ii)
                  P4Tet[NbrP4Tet0][ii] += NbrVer0 + Connectivity[i + ii];
                P4TetRef[NbrP4Tet0] = Zone_Index;
                break;
              case PYRA_55:
                NbrP4Pyr0++;
                for (ii = 0; ii < 55; ++ii)
                  P4Pyr[NbrP4Pyr0][ii] += NbrVer0 + Connectivity[i + ii];
                P4PyrRef[NbrP4Pyr0] = Zone_Index;
                break;
              case PENTA_75:
                NbrP4Pri0++;
                for (ii = 0; ii < 75; ++ii)
                  P4Pri[NbrP4Pri0][ii] += NbrVer0 + Connectivity[i + ii];
                P4PriRef[NbrP4Pri0] = Zone_Index;
                break;
              case HEXA_125:
                NbrQ4Hex0++;
                for (ii = 0; ii < 125; ++ii)
                  Q4Hex[NbrQ4Hex0][ii] += NbrVer0 + Connectivity[i + ii];
                Q4HexRef[NbrQ4Hex0] = Zone_Index;
                break;
              default:
                break;
            }
            if (NbrNod <= 0) {
              printf("  %%%% Warning in CGNS File Reader: Number of Nodes invalid for element %s\n", ElementTypeName[SubElement_Type]);
            }
            i += NbrNod;
          }

          if (Connectivity) {
            free(Connectivity);
            Connectivity = NULL;
          }
          if (ConnectOffset) {
            free(ConnectOffset);
            ConnectOffset = NULL;
          }
        }
        else
          printf("  %%%% Warning in CGNS File Reader: Zone %d -> Ignoring Section %s Containing Elements of Type %s.\n", Zone_Index, Label, ElementTypeName[Element_Type]);
      }
    }
    else {
      //-- Structured case
      if (*Dim == 2) {
        //-- Quad first
        for (j = 1; j < Zone_Size[0][1]; j++) {
          for (i = 1; i < Zone_Size[0][0]; i++) {
            NbrQua0++;
            Qua[NbrQua0][0] = NbrVer0 + NODE_INDEX2D(i, Zone_Size[0][0], j, Zone_Size[0][1]);
            Qua[NbrQua0][1] = NbrVer0 + NODE_INDEX2D(i + 1, Zone_Size[0][0], j, Zone_Size[0][1]);
            Qua[NbrQua0][2] = NbrVer0 + NODE_INDEX2D(i + 1, Zone_Size[0][0], j + 1, Zone_Size[0][1]);
            Qua[NbrQua0][3] = NbrVer0 + NODE_INDEX2D(i, Zone_Size[0][0], j + 1, Zone_Size[0][1]);
            QuaRef[NbrQua0] = Zone_Index;
          }
        }
        //-- Edges after
        SurfEdg_ID++;
        for (j = 1; j < Zone_Size[0][1]; j++) {
          NbrEdg0++;
          Edg[NbrEdg0][0] = NbrVer0 + NODE_INDEX2D(1, Zone_Size[0][0], j, Zone_Size[0][1]);
          Edg[NbrEdg0][1] = NbrVer0 + NODE_INDEX2D(1, Zone_Size[0][0], j + 1, Zone_Size[0][1]);
          EdgRef[NbrEdg0] = SurfEdg_ID;
        }
        SurfEdg_ID++;
        for (j = 1; j < Zone_Size[0][1]; j++) {
          NbrEdg0++;
          Edg[NbrEdg0][0] = NbrVer0 + NODE_INDEX2D(Zone_Size[0][0], Zone_Size[0][0], j + 1, Zone_Size[0][1]);
          Edg[NbrEdg0][1] = NbrVer0 + NODE_INDEX2D(Zone_Size[0][0], Zone_Size[0][0], j, Zone_Size[0][1]);
          EdgRef[NbrEdg0] = SurfEdg_ID;
        }
        SurfEdg_ID++;
        for (i = 1; i < Zone_Size[0][0]; i++) {
          NbrEdg0++;
          Edg[NbrEdg0][0] = NbrVer0 + NODE_INDEX2D(i, Zone_Size[0][0], 1, Zone_Size[0][1]);
          Edg[NbrEdg0][1] = NbrVer0 + NODE_INDEX2D(i + 1, Zone_Size[0][0], 1, Zone_Size[0][1]);
          EdgRef[NbrEdg0] = SurfEdg_ID;
        }
        SurfEdg_ID++;
        for (i = 1; i < Zone_Size[0][0]; i++) {
          NbrEdg0++;
          Edg[NbrEdg0][0] = NbrVer0 + NODE_INDEX2D(i + 1, Zone_Size[0][0], Zone_Size[0][1], Zone_Size[0][1]);
          Edg[NbrEdg0][1] = NbrVer0 + NODE_INDEX2D(i, Zone_Size[0][0], Zone_Size[0][1], Zone_Size[0][1]);
          EdgRef[NbrEdg0] = SurfEdg_ID;
        }
      }
      else if (*Dim == 3) {
        int ni = Zone_Size[0][0];
        int nj = Zone_Size[0][1];
        int nk = Zone_Size[0][2];
        //-- Hex first
        for (k = 1; k < nk; k++) {
          for (j = 1; j < nj; j++) {
            for (i = 1; i < ni; i++) {
              NbrHex0++;
              Hex[NbrHex0][0] = NbrVer0 + NODE_INDEX3D(i, ni, j, nj, k, nk);
              Hex[NbrHex0][1] = NbrVer0 + NODE_INDEX3D(i + 1, ni, j, nj, k, nk);
              Hex[NbrHex0][2] = NbrVer0 + NODE_INDEX3D(i + 1, ni, j + 1, nj, k, nk);
              Hex[NbrHex0][3] = NbrVer0 + NODE_INDEX3D(i, ni, j + 1, nj, k, nk);
              Hex[NbrHex0][4] = NbrVer0 + NODE_INDEX3D(i, ni, j, nj, k + 1, nk);
              Hex[NbrHex0][5] = NbrVer0 + NODE_INDEX3D(i + 1, ni, j, nj, k + 1, nk);
              Hex[NbrHex0][6] = NbrVer0 + NODE_INDEX3D(i + 1, ni, j + 1, nj, k + 1, nk);
              Hex[NbrHex0][7] = NbrVer0 + NODE_INDEX3D(i, ni, j + 1, nj, k + 1, nk);
              HexRef[NbrHex0] = Zone_Index;
            }
          }
        }
        //-- Quads after
        SurfQua_ID++;
        for (k = 1; k < nk; k++) {
          for (j = 1; j < nj; j++) {
            NbrQua0++;
            Qua[NbrQua0][0] = NbrVer0 + NODE_INDEX3D(1, ni, j, nj, k, nk);
            Qua[NbrQua0][1] = NbrVer0 + NODE_INDEX3D(1, ni, j, nj, k + 1, nk);
            Qua[NbrQua0][2] = NbrVer0 + NODE_INDEX3D(1, ni, j + 1, nj, k + 1, nk);
            Qua[NbrQua0][3] = NbrVer0 + NODE_INDEX3D(1, ni, j + 1, nj, k, nk);
            QuaRef[NbrQua0] = SurfQua_ID;
          }
        }
        SurfQua_ID++;
        for (k = 1; k < nk; k++) {
          for (j = 1; j < nj; j++) {
            NbrQua0++;
            Qua[NbrQua0][0] = NbrVer0 + NODE_INDEX3D(ni, ni, j, nj, k, nk);
            Qua[NbrQua0][1] = NbrVer0 + NODE_INDEX3D(ni, ni, j + 1, nj, k, nk);
            Qua[NbrQua0][2] = NbrVer0 + NODE_INDEX3D(ni, ni, j + 1, nj, k + 1, nk);
            Qua[NbrQua0][3] = NbrVer0 + NODE_INDEX3D(ni, ni, j, nj, k + 1, nk);
            QuaRef[NbrQua0] = SurfQua_ID;
          }
        }
        SurfQua_ID++;
        for (k = 1; k < nk; k++) {
          for (i = 1; i < ni; i++) {
            NbrQua0++;
            Qua[NbrQua0][0] = NbrVer0 + NODE_INDEX3D(i, ni, 1, nj, k, nk);
            Qua[NbrQua0][1] = NbrVer0 + NODE_INDEX3D(i + 1, ni, 1, nj, k, nk);
            Qua[NbrQua0][2] = NbrVer0 + NODE_INDEX3D(i + 1, ni, 1, nj, k + 1, nk);
            Qua[NbrQua0][3] = NbrVer0 + NODE_INDEX3D(i, ni, 1, nj, k + 1, nk);
            QuaRef[NbrQua0] = SurfQua_ID;
          }
        }
        SurfQua_ID++;
        for (k = 1; k < nk; k++) {
          for (i = 1; i < ni; i++) {
            NbrQua0++;
            Qua[NbrQua0][0] = NbrVer0 + NODE_INDEX3D(i, ni, nj, nj, k, nk);
            Qua[NbrQua0][1] = NbrVer0 + NODE_INDEX3D(i, ni, nj, nj, k + 1, nk);
            Qua[NbrQua0][2] = NbrVer0 + NODE_INDEX3D(i + 1, ni, nj, nj, k + 1, nk);
            Qua[NbrQua0][3] = NbrVer0 + NODE_INDEX3D(i + 1, ni, nj, nj, k, nk);
            QuaRef[NbrQua0] = SurfQua_ID;
          }
        }
        SurfQua_ID++;
        for (j = 1; j < nj; j++) {
          for (i = 1; i < ni; i++) {
            NbrQua0++;
            Qua[NbrQua0][0] = NbrVer0 + NODE_INDEX3D(i, ni, j, nj, 1, nk);
            Qua[NbrQua0][1] = NbrVer0 + NODE_INDEX3D(i, ni, j + 1, nj, 1, nk);
            Qua[NbrQua0][2] = NbrVer0 + NODE_INDEX3D(i + 1, ni, j + 1, nj, 1, nk);
            Qua[NbrQua0][3] = NbrVer0 + NODE_INDEX3D(i + 1, ni, j, nj, 1, nk);
            QuaRef[NbrQua0] = SurfQua_ID;
          }
        }
        SurfQua_ID++;
        for (j = 1; j < nj; j++) {
          for (i = 1; i < ni; i++) {
            NbrQua0++;
            Qua[NbrQua0][0] = NbrVer0 + NODE_INDEX3D(i, ni, j, nj, nk, nk);
            Qua[NbrQua0][1] = NbrVer0 + NODE_INDEX3D(i + 1, ni, j, nj, nk, nk);
            Qua[NbrQua0][2] = NbrVer0 + NODE_INDEX3D(i + 1, ni, j + 1, nj, nk, nk);
            Qua[NbrQua0][3] = NbrVer0 + NODE_INDEX3D(i, ni, j + 1, nj, nk, nk);
            QuaRef[NbrQua0] = SurfQua_ID;
          }
        }
      }
    }
  }
  CG_Err = cg_close(File_Index);

  if (CG_Err != CG_OK)
    printf("  %%%% Warning in CGNS File Reader in cg_close: %s\n", cg_get_error());

  return VIZINT_SUCCESS;
}

//-- open solution at elements and solution at vertices
int viz_OpenSolutionCGNS(VizObject* iObj, const char* SolNam, VizIntStatus* status)
{
  char Label[1024], LabelElt[1024], LabelNam[1024];

  char(**Field_Name2)[1024];

  char(*FldVerNam)[1024], (*FldEltNam)[1024];

  Field_Name2 = NULL;

  int CG_Err = 0;

  int NbrQua0, NbrVer0, NbrVerZone, NbrTri0, NbrTet0, NbrHex0, NbrPyr0, NbrPri0,
      NbrQua, NbrVer, NbrTri, NbrTet, NbrHex, NbrPyr, NbrPri, i, n, NbrNod, idxElt0, NbrElt0,
      NbrQua00, NbrTri00, NbrTet00, NbrHex00, NbrPyr00, NbrPri00;

  int NbrP2Tri, NbrP2Tri0, NbrP2Tri00, NbrQ2Qua, NbrQ2Qua0, NbrQ2Qua00,
      NbrP2Tet, NbrP2Tet0, NbrP2Tet00, NbrP2Pyr, NbrP2Pyr0, NbrP2Pyr00,
      NbrP2Pri, NbrP2Pri0, NbrP2Pri00, NbrQ2Hex, NbrQ2Hex0, NbrQ2Hex00;

  int NbrP3Tri, NbrP3Tri0, NbrP3Tri00, NbrQ3Qua, NbrQ3Qua0, NbrQ3Qua00,
      NbrP3Tet, NbrP3Tet0, NbrP3Tet00, NbrP3Pyr, NbrP3Pyr0, NbrP3Pyr00,
      NbrP3Pri, NbrP3Pri0, NbrP3Pri00, NbrQ3Hex, NbrQ3Hex0, NbrQ3Hex00;

  int NbrP4Tri, NbrP4Tri0, NbrP4Tri00, NbrQ4Qua, NbrQ4Qua0, NbrQ4Qua00,
      NbrP4Tet, NbrP4Tet0, NbrP4Tet00, NbrP4Pyr, NbrP4Pyr0, NbrP4Pyr00,
      NbrP4Pri, NbrP4Pri0, NbrP4Pri00, NbrQ4Hex, NbrQ4Hex0, NbrQ4Hex00;

  cgsize_t *Connectivity = NULL, *Parent_Data = NULL, *ConnectOffset = NULL;

  double **SolVer = NULL, **SolTri = NULL, **SolQua = NULL, **SolTet = NULL, **SolPri = NULL, **SolPyr = NULL, **SolHex = NULL, **SolElt = NULL, *SolMix = NULL,
         **SolP2Tri = NULL, **SolQ2Qua = NULL, **SolP2Tet = NULL, **SolP2Pyr = NULL, **SolP2Pri = NULL, **SolQ2Hex = NULL,
         **SolP3Tri = NULL, **SolQ3Qua = NULL, **SolP3Tet = NULL, **SolP3Pyr = NULL, **SolP3Pri = NULL, **SolQ3Hex = NULL,
         **SolP4Tri = NULL, **SolQ4Qua = NULL, **SolP4Tet = NULL, **SolP4Pyr = NULL, **SolP4Pri = NULL, **SolQ4Hex = NULL;

  int NbrFldVer, NbrFldElt, idxVer, idxElt, iFld;

  cgsize_t Zone_Size[3][3];
  int *    ZoneSolFldNbr = NULL, *ZoneSolFldNbrVal = NULL;

  int Base_Index, Cell_Dim, File_Index, Number_of_Sections,
      Number_of_Bases, Number_of_Boundary_Items, Number_of_Solutions, Number_of_Fields,
      Number_of_Zones, Parent_Flag, Phys_Dim, Solution_Index, Field_Index,
      Zone_Index, Section_Index, NbrElt, NbrFld, NbrSol0;

  cgsize_t Start_Index, End_Index, size;

  cgsize_t max_Index[3], min_Index[3];

  ElementType_t Element_Type, SubElement_Type;

  ZoneType_t Zone_Type;

  GridLocation_t Grid_Location, *ZoneSolLoc = NULL;

  DataType_t Data_Type;

  solution **SolObj = NULL, **cur_sol = NULL;

  File_Index = -1;
  CG_Err     = cg_open(SolNam, CG_MODE_READ, &File_Index);

  if (File_Index < 0) {
    viz_writestatus(status, "  unable to open CGNS solution file");
    return VIZINT_ERROR;
  }

  if (CG_Err != CG_OK)
    printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

  CG_Err = cg_nbases(File_Index, &Number_of_Bases);

  if (CG_Err != CG_OK)
    printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

  if (Number_of_Bases != 1)
    printf("  %%%% Warning in CGNS File Reader: only the first base is considered\n");

  Base_Index = 1;

  CG_Err = cg_base_read(File_Index, Base_Index,
      Label, &Cell_Dim, &Phys_Dim);

  if (CG_Err != CG_OK)
    printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

  if ((Phys_Dim != 3 && Phys_Dim != 2)) {
    cg_close(File_Index);
    viz_writestatus(status, "  only 2D/3D physical dimensions are allowed in CGNS file");
    return VIZINT_ERROR;
  }

  if ((Cell_Dim != 3 && Cell_Dim != 2)) {
    cg_close(File_Index);
    viz_writestatus(status, "  only 2D/3D cell dimensions are allowed in CGNS file");
    return VIZINT_ERROR;
  }

  CG_Err = cg_nzones(File_Index, Base_Index, &Number_of_Zones);
  if (CG_Err != CG_OK)
    printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

  //-- check compatiblity of the solutions as they are defined zone-wise where vizir only defines a global one.
  //-- count the number of fields per solution type (e.g. solution at vertices and solution at elements)
  NbrFldVer = NbrFldElt = 0;
  int err               = 0;
  Zone_Index            = 1;
  CG_Err                = cg_nsols(File_Index, Base_Index, Zone_Index, &Number_of_Solutions);
  if (CG_Err != CG_OK)
    printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
  NbrSol0          = Number_of_Solutions;
  ZoneSolFldNbr    = malloc(Number_of_Solutions * sizeof(int));
  ZoneSolFldNbrVal = malloc(Number_of_Solutions * sizeof(int));
  ZoneSolLoc       = malloc(Number_of_Solutions * sizeof(GridLocation_t));
  Field_Name2      = malloc(Number_of_Solutions * sizeof(char(*)[1024]));
  for (Solution_Index = 1; Solution_Index <= Number_of_Solutions; ++Solution_Index) {
    CG_Err = cg_sol_info(File_Index, Base_Index, Zone_Index, Solution_Index, Label, &Grid_Location);
    if (CG_Err != CG_OK)
      printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
    CG_Err = cg_nfields(File_Index, Base_Index, Zone_Index, Solution_Index, &Number_of_Fields);
    if (CG_Err != CG_OK)
      printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

    ZoneSolFldNbr[Solution_Index - 1] = Number_of_Fields;
    ZoneSolLoc[Solution_Index - 1]    = Grid_Location;
    NbrFld                            = 0;
    if (Number_of_Fields > 0)
      Field_Name2[Solution_Index - 1] = malloc(Number_of_Fields * 1024 * sizeof(char));
    else
      Field_Name2[Solution_Index - 1] = NULL;

    //-- Check admissible fields
    for (Field_Index = 1; Field_Index <= Number_of_Fields; ++Field_Index) {
      CG_Err = cg_field_info(File_Index, Base_Index, Zone_Index, Solution_Index, Field_Index, &Data_Type, Field_Name2[Solution_Index - 1][Field_Index - 1]);
      if (CG_Err != CG_OK)
        printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
      if (Data_Type == RealDouble || Data_Type == RealSingle) {
        NbrFld++;
        if (Grid_Location == Vertex)
          NbrFldVer++;
        else if (Grid_Location == CellCenter)
          NbrFldElt++;
        else if (Field_Index == 1) { //--these data are attached to the solution, regardless of the field and is the same in every zone
          printf("  %%%% Warning in CGNS File Reader: Solution %s with type %s is not supported\n", Label, GridLocationName[Grid_Location]);
        }
      }
      else {
        printf("  %%%% Warning in CGNS File Reader: Skipping Field %s of %s. RealDouble or RealSingle Fields are required.\n", Field_Name2[Solution_Index - 1][Field_Index - 1], Label);
      }
    }
    ZoneSolFldNbrVal[Solution_Index - 1] = NbrFld;
  }

  for (Zone_Index = 2; Zone_Index <= Number_of_Zones; Zone_Index++) {
    CG_Err = cg_nsols(File_Index, Base_Index, Zone_Index, &Number_of_Solutions);
    if (CG_Err != CG_OK)
      printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
    if (Number_of_Solutions != NbrSol0) {
      viz_writestatus(status, "  the number of flow solutions needs to be the same in every zone: %d (Zone 1) and %d (Zone %d)", NbrSol0, Number_of_Solutions, Zone_Index);
      err = 1;
    }
    else {
      for (Solution_Index = 1; Solution_Index <= Number_of_Solutions; ++Solution_Index) {
        CG_Err = cg_sol_info(File_Index, Base_Index, Zone_Index, Solution_Index, Label, &Grid_Location);
        if (CG_Err != CG_OK)
          printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
        CG_Err = cg_nfields(File_Index, Base_Index, Zone_Index, Solution_Index, &Number_of_Fields);
        if (CG_Err != CG_OK)
          printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
        if (ZoneSolFldNbr[Solution_Index - 1] != Number_of_Fields) {
          viz_writestatus(status, "  For each flow solution the number of fields needs to stay the same from one zone to another");
          err = 1;
        }
        else if (ZoneSolLoc[Solution_Index - 1] != Grid_Location) {
          viz_writestatus(status, "  For each flow solution the grid location of the fields needs to stay the same from one zone to another");
          err = 1;
        }
        else {
          NbrFld = 0;
          //-- Check admissible fields names
          for (Field_Index = 1; Field_Index <= Number_of_Fields; ++Field_Index) {
            CG_Err = cg_field_info(File_Index, Base_Index, Zone_Index, Solution_Index, Field_Index, &Data_Type, LabelNam);
            if (CG_Err != CG_OK)
              printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
            if (Field_Name2[Solution_Index - 1][Field_Index - 1] != LabelNam) {
              viz_writestatus(status, "  For each flow solution the name of the fields needs to stay the same from one zone to another");
              err = 1;
            }
            if (Data_Type == RealDouble || Data_Type == RealSingle) {
              NbrFld++;
            }
          }
          if (ZoneSolFldNbrVal[Solution_Index - 1] != NbrFld) {
            viz_writestatus(status, "  For each flow solution the data type of the fields of the fields needs to stay the same from one zone to another");
            err = 1;
          }
        }
      }
    }
  }

  if (ZoneSolFldNbr) {
    free(ZoneSolFldNbr);
    ZoneSolFldNbr = NULL;
  }
  if (ZoneSolFldNbrVal) {
    free(ZoneSolFldNbrVal);
    ZoneSolFldNbrVal = NULL;
  }
  if (ZoneSolLoc) {
    free(ZoneSolLoc);
    ZoneSolLoc = NULL;
  }

  if (err == 1) {
    cg_close(File_Index);
    return VIZINT_ERROR;
  }
  if (NbrFldVer + NbrFldElt > 0)
    printf("  %%%% INFO CGNS File Reader: %d Fields at vertices and %d Fields at elements are detected\n", NbrFldVer, NbrFldElt);
  else {
    viz_writestatus(status, "  No Solution field is given in CGNS file\n");
    cg_close(File_Index);
    return VIZINT_ERROR;
  }

  //-- Now as we know that the multi field is admissible, we can set the arrays properly, with the names as well
  //-- First step, get field names
  idxVer = idxElt = 0;
  FldVerNam       = malloc(NbrFldVer * 1024 * sizeof(char));
  FldEltNam       = malloc(NbrFldElt * 1024 * sizeof(char));
  for (Zone_Index = 1; Zone_Index <= Number_of_Zones; Zone_Index++) {
    CG_Err = cg_nsols(File_Index, Base_Index, Zone_Index, &Number_of_Solutions);
    if (CG_Err != CG_OK)
      printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
    for (Solution_Index = 1; Solution_Index <= Number_of_Solutions; ++Solution_Index) {
      CG_Err = cg_sol_info(File_Index, Base_Index, Zone_Index, Solution_Index, Label, &Grid_Location);
      CG_Err = cg_nfields(File_Index, Base_Index, Zone_Index, Solution_Index, &Number_of_Fields);
      //-- Check admissible fields names
      if (Label[0] != '\0' && Grid_Location != GridLocationNull) {
        for (Field_Index = 1; Field_Index <= Number_of_Fields; ++Field_Index) {
          CG_Err = cg_field_info(File_Index, Base_Index, Zone_Index, Solution_Index, Field_Index, &Data_Type, LabelNam);
          if (Data_Type == RealDouble || Data_Type == RealSingle) {
            //-- check if there is multiple flow solutions (in the latter, a # is put after FlowSolution in Label and then a description is given to make a difference)
            if (Grid_Location == Vertex) {
              char* res = strstr(Label, "#");
              if (res != NULL && res[1] != '\0')
                sprintf(FldVerNam[idxVer], "%s/%s", &(res[1]), LabelNam);
              else
                sprintf(FldVerNam[idxVer], "%s", LabelNam);
              idxVer++;
            }
            else if (Grid_Location == CellCenter) {
              char* res = strstr(Label, "#");
              if (res != NULL && res[1] != '\0')
                sprintf(FldEltNam[idxElt], "%s/%s", &(res[1]), LabelNam);
              else
                sprintf(FldEltNam[idxElt], "%s", LabelNam);
              idxElt++;
            }
          }
        }
      }
    }
  }

  //-- second step, allocate global solutions arrays and attach it to global solution arrays.
  //-- Loop over fields ( do it in decreasing order so that fields appear in increasing order in the structure)
  if (NbrFldVer > 0 && iObj->NbrVer > 0) {
    SolObj = malloc(sizeof(solution*) * NbrFldVer);
    for (iFld = 0; iFld < NbrFldVer; iFld++) {
      SolObj[iFld] = NULL;
    }
    cur_sol = &(iObj->CrdSol);
    SolVer  = malloc(NbrFldVer * sizeof(double*));
    for (iFld = 0; iFld < NbrFldVer; iFld++) {
      SolVer[iFld] = malloc((iObj->NbrVer + 1) * sizeof(double));
      //-- for each field, alloc one solution
      SolObj[iFld]         = viz_addsol(*cur_sol);
      SolObj[iFld]->Dim    = iObj->Dim;
      SolObj[iFld]->NbrSol = iObj->NbrVer;
      SolObj[iFld]->SolTyp = GmfSca;
      sprintf(SolObj[iFld]->Nam, "%s", FldVerNam[iFld]);
      SolObj[iFld]->Sol      = SolVer[iFld];
      SolObj[iFld]->Ite      = 0;
      SolObj[iFld]->Tim      = 0.;
      SolObj[iFld]->Deg      = 0;
      SolObj[iFld]->NbrNod   = 1;
      SolObj[iFld]->vizAlloc = 1;
      *cur_sol               = SolObj[iFld];
    }
    // for (iFld = 0; iFld < NbrFldVer; iFld++)
    //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
    *cur_sol = SolObj[0];

    if (SolObj) {
      free(SolObj);
      SolObj = NULL;
    }
  }

  if (NbrFldElt > 0) {
    if (Cell_Dim == 2) {
      if (iObj->NbrQua > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol = &(iObj->QuaSol);
        SolQua  = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolQua[iFld] = malloc((iObj->NbrQua + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrQua;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolQua[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];

        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }
      if (iObj->NbrTri > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol = &(iObj->TriSol);
        SolTri  = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolTri[iFld] = malloc((iObj->NbrTri + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrTri;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolTri[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];

        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP2Tri > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P2TriSol);
        SolP2Tri = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP2Tri[iFld] = malloc((iObj->NbrP2Tri + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP2Tri;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP2Tri[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrQ2Qua > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->Q2QuaSol);
        SolQ2Qua = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolQ2Qua[iFld] = malloc((iObj->NbrQ2Qua + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrQ2Qua;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolQ2Qua[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP3Tri > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P3TriSol);
        SolP3Tri = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP3Tri[iFld] = malloc((iObj->NbrP3Tri + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP3Tri;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP3Tri[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrQ3Qua > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->Q3QuaSol);
        SolQ3Qua = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolQ3Qua[iFld] = malloc((iObj->NbrQ3Qua + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrQ3Qua;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolQ3Qua[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP4Tri > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P4TriSol);
        SolP4Tri = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP4Tri[iFld] = malloc((iObj->NbrP4Tri + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP4Tri;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP4Tri[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrQ4Qua > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->Q4QuaSol);
        SolQ4Qua = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolQ4Qua[iFld] = malloc((iObj->NbrQ4Qua + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrQ4Qua;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolQ4Qua[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }
    }
    else if (Cell_Dim == 3) {
      if (iObj->NbrTet > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol = &(iObj->TetSol);
        SolTet  = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolTet[iFld] = malloc((iObj->NbrTet + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrTet;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolTet[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }
      if (iObj->NbrPyr > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol = &(iObj->PyrSol);
        SolPyr  = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolPyr[iFld] = malloc((iObj->NbrPyr + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrPyr;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolPyr[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }
      if (iObj->NbrPri > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol = &(iObj->PriSol);
        SolPri  = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolPri[iFld] = malloc((iObj->NbrPri + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrPri;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolPri[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }
      if (iObj->NbrHex > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol = &(iObj->HexSol);
        SolHex  = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolHex[iFld] = malloc((iObj->NbrHex + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrHex;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolHex[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP2Tet > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P2TetSol);
        SolP2Tet = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP2Tet[iFld] = malloc((iObj->NbrP2Tet + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP2Tet;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP2Tet[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP2Pyr > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P2PyrSol);
        SolP2Pyr = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP2Pyr[iFld] = malloc((iObj->NbrP2Pyr + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP2Pyr;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP2Pyr[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP2Pri > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P2PriSol);
        SolP2Pri = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP2Pri[iFld] = malloc((iObj->NbrP2Pri + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP2Pri;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP2Pri[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrQ2Hex > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->Q2HexSol);
        SolQ2Hex = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolQ2Hex[iFld] = malloc((iObj->NbrQ2Hex + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrQ2Hex;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolQ2Hex[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP3Tet > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P3TetSol);
        SolP3Tet = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP3Tet[iFld] = malloc((iObj->NbrP3Tet + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP3Tet;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP3Tet[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP3Pyr > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P3PyrSol);
        SolP3Pyr = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP3Pyr[iFld] = malloc((iObj->NbrP3Pyr + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP3Pyr;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP3Pyr[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP3Pri > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P3PriSol);
        SolP3Pri = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP3Pri[iFld] = malloc((iObj->NbrP3Pri + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP3Pri;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP3Pri[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrQ3Hex > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->Q3HexSol);
        SolQ3Hex = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolQ3Hex[iFld] = malloc((iObj->NbrQ3Hex + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrQ3Hex;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolQ3Hex[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP4Tet > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P4TetSol);
        SolP4Tet = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP4Tet[iFld] = malloc((iObj->NbrP4Tet + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP4Tet;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP4Tet[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP4Pyr > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P4PyrSol);
        SolP4Pyr = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP4Pyr[iFld] = malloc((iObj->NbrP4Pyr + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP4Pyr;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP4Pyr[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrP4Pri > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->P4PriSol);
        SolP4Pri = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolP4Pri[iFld] = malloc((iObj->NbrP4Pri + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrP4Pri;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolP4Pri[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }

      if (iObj->NbrQ4Hex > 0) {
        SolObj = malloc(sizeof(solution*) * NbrFldElt);
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolObj[iFld] = NULL;
        }
        cur_sol  = &(iObj->Q4HexSol);
        SolQ4Hex = malloc(NbrFldElt * sizeof(double*));
        for (iFld = 0; iFld < NbrFldElt; iFld++) {
          SolQ4Hex[iFld] = malloc((iObj->NbrQ4Hex + 1) * sizeof(double));
          //-- for each field, alloc one solution
          SolObj[iFld]         = viz_addsol(*cur_sol);
          SolObj[iFld]->Dim    = iObj->Dim;
          SolObj[iFld]->NbrSol = iObj->NbrQ4Hex;
          SolObj[iFld]->SolTyp = GmfSca;
          sprintf(SolObj[iFld]->Nam, "%s", FldEltNam[iFld]);
          SolObj[iFld]->Sol      = SolQ4Hex[iFld];
          SolObj[iFld]->Ite      = 0;
          SolObj[iFld]->Tim      = 0.;
          SolObj[iFld]->Deg      = 0;
          SolObj[iFld]->NbrNod   = 1;
          SolObj[iFld]->vizAlloc = 1;
          *cur_sol               = SolObj[iFld];
        }
        // for (iFld = 0; iFld < NbrFldElt; iFld++)
        //   printf("  Field %d : %s\n", iFld + 1, SolObj[iFld]->Nam);
        *cur_sol = SolObj[0];
        if (SolObj) {
          free(SolObj);
          SolObj = NULL;
        }
      }
    }
  }
  if (FldEltNam) {
    free(FldEltNam);
    FldEltNam = NULL;
  }
  if (FldVerNam) {
    free(FldVerNam);
    FldVerNam = NULL;
  }
  //-- third step => fill in arrays
  NbrVer   = 0;
  NbrTri   = 0;
  NbrQua   = 0;
  NbrTet   = 0;
  NbrPyr   = 0;
  NbrPri   = 0;
  NbrHex   = 0;
  NbrP2Tri = 0;
  NbrQ2Qua = 0;
  NbrP2Tet = 0;
  NbrP2Pyr = 0;
  NbrP2Pri = 0;
  NbrQ2Hex = 0;
  NbrP3Tri = 0;
  NbrQ3Qua = 0;
  NbrP3Tet = 0;
  NbrP3Pyr = 0;
  NbrP3Pri = 0;
  NbrQ3Hex = 0;
  NbrP4Tri = 0;
  NbrQ4Qua = 0;
  NbrP4Tet = 0;
  NbrP4Pyr = 0;
  NbrP4Pri = 0;
  NbrQ4Hex = 0;

  for (Zone_Index = 1; Zone_Index <= Number_of_Zones; Zone_Index++) {

    NbrVer0   = NbrVer;
    NbrTri0   = NbrTri;
    NbrQua0   = NbrQua;
    NbrTet0   = NbrTet;
    NbrPyr0   = NbrPyr;
    NbrPri0   = NbrPri;
    NbrHex0   = NbrHex;
    NbrP2Tri0 = NbrP2Tri;
    NbrQ2Qua0 = NbrQ2Qua;
    NbrP2Tet0 = NbrP2Tet;
    NbrP2Pyr0 = NbrP2Pyr;
    NbrP2Pri0 = NbrP2Pri;
    NbrQ2Hex0 = NbrQ2Hex;
    NbrP3Tri0 = NbrP3Tri;
    NbrQ3Qua0 = NbrQ3Qua;
    NbrP3Tet0 = NbrP3Tet;
    NbrP3Pyr0 = NbrP3Pyr;
    NbrP3Pri0 = NbrP3Pri;
    NbrQ3Hex0 = NbrQ3Hex;
    NbrP4Tri0 = NbrP4Tri;
    NbrQ4Qua0 = NbrQ4Qua;
    NbrP4Tet0 = NbrP4Tet;
    NbrP4Pyr0 = NbrP4Pyr;
    NbrP4Pri0 = NbrP4Pri;
    NbrQ4Hex0 = NbrQ4Hex;

    CG_Err = cg_zone_type(File_Index, Base_Index, Zone_Index, &Zone_Type);

    if (CG_Err != CG_OK)
      printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

    CG_Err = cg_zone_read(File_Index, Base_Index, Zone_Index, Label,
        *Zone_Size);

    if (CG_Err != CG_OK)
      printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

    if (Zone_Size[0][0] < 1) {
      cg_close(File_Index);
      viz_writestatus(status, "  no vertices are specified in Zone %d of CGNS solution file %s", Zone_Index, SolNam);
      continue;
    }

    //-- count number of entities in this zone

    if (Zone_Type == Unstructured) {
      NbrVerZone = Zone_Size[0][0];
      NbrVer += NbrVerZone;
    }
    else {
      if (Phys_Dim == 3)
        NbrVerZone = (Zone_Size[0][0]) * (Zone_Size[0][1]) * (Zone_Size[0][2]);
      else
        NbrVerZone = (Zone_Size[0][0]) * (Zone_Size[0][1]);
      NbrVer += NbrVerZone;
    }

    if (NbrVerZone < 1) {
      cg_close(File_Index);
      viz_writestatus(status, "  no vertices are specified in CGNS mesh file");
      return VIZINT_ERROR;
    }

    //-- create a tensorial mesh from the structured entities
    if (Zone_Type == Structured) {
      if (Phys_Dim == 3)
        NbrHex += (Zone_Size[0][0] - 1) * (Zone_Size[0][1] - 1) * (Zone_Size[0][2] - 1);
      else
        NbrQua += (Zone_Size[0][0] - 1) * (Zone_Size[0][1] - 1);
    }
    else {

      CG_Err = cg_nsections(File_Index, Base_Index, Zone_Index,
          &Number_of_Sections);

      if (CG_Err != CG_OK)
        printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

      if (Number_of_Sections <= 0) {
        cg_close(File_Index);
        viz_writestatus(status, " no element sections are specified in CGNS mesh file");
        return VIZINT_ERROR;
      }
      //-- count number of entities per section => handle the case of mixed elements as well.
      for (Section_Index = 1; Section_Index <= Number_of_Sections; ++Section_Index) {
        CG_Err = cg_section_read(File_Index, Base_Index, Zone_Index,
            Section_Index, Label, &Element_Type,
            &Start_Index, &End_Index,
            &Number_of_Boundary_Items, &Parent_Flag);

        if (CG_Err != CG_OK)
          printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

        if (Element_Type == TRI_3)
          NbrTri = NbrTri + End_Index - Start_Index + 1;

        else if (Element_Type == QUAD_4)
          NbrQua = NbrQua + End_Index - Start_Index + 1;

        else if (Element_Type == TETRA_4)
          NbrTet = NbrTet + End_Index - Start_Index + 1;

        else if (Element_Type == PYRA_5)
          NbrPyr = NbrPyr + End_Index - Start_Index + 1;

        else if (Element_Type == PENTA_6)
          NbrPri = NbrPri + End_Index - Start_Index + 1;

        else if (Element_Type == HEXA_8)
          NbrHex = NbrHex + End_Index - Start_Index + 1;

        else if (Element_Type == TRI_6)
          NbrP2Tri = NbrP2Tri + End_Index - Start_Index + 1;

        else if (Element_Type == QUAD_9)
          NbrQ2Qua = NbrQ2Qua + End_Index - Start_Index + 1;

        else if (Element_Type == TETRA_10)
          NbrP2Tet = NbrP2Tet + End_Index - Start_Index + 1;

        else if (Element_Type == PYRA_14)
          NbrP2Pyr = NbrP2Pyr + End_Index - Start_Index + 1;

        else if (Element_Type == PENTA_18)
          NbrP2Pri = NbrP2Pri + End_Index - Start_Index + 1;

        else if (Element_Type == HEXA_27)
          NbrQ2Hex = NbrQ2Hex + End_Index - Start_Index + 1;

        else if (Element_Type == TRI_10)
          NbrP3Tri = NbrP3Tri + End_Index - Start_Index + 1;

        else if (Element_Type == QUAD_16)
          NbrQ3Qua = NbrQ3Qua + End_Index - Start_Index + 1;

        else if (Element_Type == TETRA_20)
          NbrP3Tet = NbrP3Tet + End_Index - Start_Index + 1;

        else if (Element_Type == PYRA_30)
          NbrP3Pyr = NbrP3Pyr + End_Index - Start_Index + 1;

        else if (Element_Type == PENTA_40)
          NbrP3Pri = NbrP3Pri + End_Index - Start_Index + 1;

        else if (Element_Type == HEXA_64)
          NbrQ3Hex = NbrQ3Hex + End_Index - Start_Index + 1;

        else if (Element_Type == TRI_15)
          NbrP4Tri = NbrP4Tri + End_Index - Start_Index + 1;

        else if (Element_Type == QUAD_25)
          NbrQ4Qua = NbrQ4Qua + End_Index - Start_Index + 1;

        else if (Element_Type == TETRA_35)
          NbrP4Tet = NbrP4Tet + End_Index - Start_Index + 1;

        else if (Element_Type == PYRA_55)
          NbrP4Pyr = NbrP4Pyr + End_Index - Start_Index + 1;

        else if (Element_Type == PENTA_75)
          NbrP4Pri = NbrP4Pri + End_Index - Start_Index + 1;

        else if (Element_Type == HEXA_125)
          NbrQ4Hex = NbrQ4Hex + End_Index - Start_Index + 1;

        else if (Element_Type == MIXED) {
          NbrElt = End_Index - Start_Index + 1;
          CG_Err = cg_ElementDataSize(File_Index, Base_Index, Zone_Index, Section_Index, &size);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          Connectivity = (cgsize_t*)malloc((size_t)size * sizeof(cgsize_t));

          ConnectOffset = (cgsize_t*)malloc((size_t)(NbrElt + 1) * sizeof(cgsize_t));

          CG_Err = cg_poly_elements_read(File_Index, Base_Index, Zone_Index, Section_Index, Connectivity, ConnectOffset, Parent_Data);

          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

          for (i = 0, n = 0; n < NbrElt; n++) {
            SubElement_Type = Connectivity[i++];
            switch (SubElement_Type) {
              case TRI_3:
                (NbrTri)++;
                break;
              case QUAD_4:
                (NbrQua)++;
                break;
              case TETRA_4:
                (NbrTet)++;
                break;
              case PYRA_5:
                (NbrPyr)++;
                break;
              case PENTA_6:
                (NbrPri)++;
                break;
              case HEXA_8:
                (NbrHex)++;
                break;
              case TRI_6:
                (NbrP2Tri)++;
                break;
              case QUAD_9:
                (NbrQ2Qua)++;
                break;
              case TETRA_10:
                (NbrP2Tet)++;
                break;
              case PYRA_14:
                (NbrP2Pyr)++;
                break;
              case PENTA_18:
                (NbrP2Pri)++;
                break;
              case HEXA_27:
                (NbrQ2Hex)++;
                break;
              case TRI_10:
                (NbrP3Tri)++;
                break;
              case QUAD_16:
                (NbrQ3Qua)++;
                break;
              case TETRA_20:
                (NbrP3Tet)++;
                break;
              case PYRA_30:
                (NbrP3Pyr)++;
                break;
              case PENTA_40:
                (NbrP3Pri)++;
                break;
              case HEXA_64:
                (NbrQ3Hex)++;
                break;
              case TRI_15:
                (NbrP4Tri)++;
                break;
              case QUAD_25:
                (NbrQ4Qua)++;
                break;
              case TETRA_35:
                (NbrP4Tet)++;
                break;
              case PYRA_55:
                (NbrP4Pyr)++;
                break;
              case PENTA_75:
                (NbrP4Pri)++;
                break;
              case HEXA_125:
                (NbrQ4Hex)++;
                break;
              default:
                break;
            }
            CG_Err = cg_npe(SubElement_Type, &NbrNod);
            if (NbrNod <= 0) {
              printf("  %%%% Warning in CGNS File Reader: Number of Nodes invalid for element %s\n", ElementTypeName[SubElement_Type]);
              break;
            }
            i += NbrNod;
          }
          if (Connectivity) {
            free(Connectivity);
            Connectivity = NULL;
          }
          if (ConnectOffset) {
            free(ConnectOffset);
            ConnectOffset = NULL;
          }
        }
      }
    }

    //-- get number of solutions
    CG_Err = cg_nsols(File_Index, Base_Index, Zone_Index, &Number_of_Solutions);

    if (CG_Err != CG_OK)
      printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

    if (Number_of_Solutions <= 0) {
      cg_close(File_Index);
      viz_writestatus(status, " no solutions are specified in Zone %d of CGNS solution file %s", Zone_Index, SolNam);
      continue;
    }

    //-- count number of elements per zone

    //-- INFO : Solution at elements are gathered in CGNS into a single structure called Cell Centered Soltuion
    //--        Therefore, to get the solution per entities, it is mandatory to do a loop over them in order to get the sub-indices of each of them.
    idxVer = idxElt = 0;
    for (Solution_Index = 1; Solution_Index <= Number_of_Solutions; ++Solution_Index) {
      CG_Err = cg_sol_info(File_Index, Base_Index, Zone_Index, Solution_Index, Label, &Grid_Location);
      if (CG_Err != CG_OK)
        printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
      if (Label[0] != '\0' && Grid_Location != GridLocationNull) {
        if (Grid_Location == Vertex) {
          if (Zone_Type == Unstructured) {
            min_Index[0] = 1;
            max_Index[0] = iObj->NbrVer;
          }
          else {
            for (i = 0; i < iObj->Dim; i++) {
              min_Index[i] = 1;
              max_Index[i] = Zone_Size[0][i];
            }
          }
          CG_Err = cg_nfields(File_Index, Base_Index, Zone_Index, Solution_Index, &Number_of_Fields);
          if (CG_Err != CG_OK)
            printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
          if (Number_of_Fields > 0) {
            //-- Check admissible fields
            for (Field_Index = 1; Field_Index <= Number_of_Fields; ++Field_Index) {
              CG_Err = cg_field_info(File_Index, Base_Index, Zone_Index, Solution_Index, Field_Index, &Data_Type, LabelNam);
              if (CG_Err != CG_OK)
                printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
              if (Data_Type == RealDouble || Data_Type == RealSingle) {
                CG_Err = cg_field_read(File_Index, Base_Index, Zone_Index, Solution_Index, LabelNam,
                    RealDouble, min_Index, max_Index, (void*)(&SolVer[idxVer][NbrVer0 + 1]));
                idxVer++;
              }
            }
          }
        }
        else if (Grid_Location == CellCenter) {
          if (Zone_Type == Unstructured) {
            CG_Err = cg_nsections(File_Index, Base_Index, Zone_Index,
                &Number_of_Sections);

            if (CG_Err != CG_OK)
              printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

            if (Number_of_Sections <= 0) {
              cg_close(File_Index);
              viz_writestatus(status, " no element sections are specified in CGNS mesh file");
              return VIZINT_ERROR;
            }
            //-- fill in volume arrays first => reset idxElt offset each time
            idxElt0 = idxElt;
            for (Section_Index = 1; Section_Index <= Number_of_Sections; ++Section_Index) {
              CG_Err       = cg_section_read(File_Index, Base_Index, Zone_Index,
                        Section_Index, LabelElt, &Element_Type,
                        &Start_Index, &End_Index,
                        &Number_of_Boundary_Items, &Parent_Flag);
              min_Index[0] = Start_Index;
              max_Index[0] = End_Index;

              idxElt = idxElt0;
              if (Element_Type != MIXED) {
                if (Cell_Dim == 2) {
                  if (Element_Type == TRI_3) {
                    SolElt  = SolTri;
                    NbrElt0 = NbrTri0;
                  }
                  else if (Element_Type == QUAD_4) {
                    SolElt  = SolQua;
                    NbrElt0 = NbrQua0;
                  }
                  else if (Element_Type == TRI_6) {
                    SolElt  = SolP2Tri;
                    NbrElt0 = NbrP2Tri0;
                  }
                  else if (Element_Type == QUAD_9) {
                    SolElt  = SolQ2Qua;
                    NbrElt0 = NbrQ2Qua0;
                  }
                  else if (Element_Type == TRI_10) {
                    SolElt  = SolP3Tri;
                    NbrElt0 = NbrP3Tri0;
                  }
                  else if (Element_Type == QUAD_16) {
                    SolElt  = SolQ3Qua;
                    NbrElt0 = NbrQ3Qua0;
                  }
                  else if (Element_Type == TRI_15) {
                    SolElt  = SolP4Tri;
                    NbrElt0 = NbrP4Tri0;
                  }
                  else if (Element_Type == QUAD_25) {
                    SolElt  = SolQ4Qua;
                    NbrElt0 = NbrQ4Qua0;
                  }
                  else
                    continue;
                }
                else if (Cell_Dim == 3) {
                  if (Element_Type == TETRA_4) {
                    SolElt  = SolTet;
                    NbrElt0 = NbrTet0;
                  }
                  else if (Element_Type == PYRA_5) {
                    SolElt  = SolPyr;
                    NbrElt0 = NbrPyr0;
                  }
                  else if (Element_Type == PENTA_6) {
                    SolElt  = SolPri;
                    NbrElt0 = NbrPri0;
                  }
                  else if (Element_Type == HEXA_8) {
                    SolElt  = SolHex;
                    NbrElt0 = NbrHex0;
                  }
                  else if (Element_Type == TETRA_10) {
                    SolElt  = SolP2Tet;
                    NbrElt0 = NbrP2Tet0;
                  }
                  else if (Element_Type == PYRA_14) {
                    SolElt  = SolP2Pyr;
                    NbrElt0 = NbrP2Pyr0;
                  }
                  else if (Element_Type == PENTA_18) {
                    SolElt  = SolP2Pri;
                    NbrElt0 = NbrP2Pri0;
                  }
                  else if (Element_Type == HEXA_27) {
                    SolElt  = SolQ2Hex;
                    NbrElt0 = NbrQ2Hex0;
                  }
                  else if (Element_Type == TETRA_20) {
                    SolElt  = SolP3Tet;
                    NbrElt0 = NbrP3Tet0;
                  }
                  else if (Element_Type == PYRA_30) {
                    SolElt  = SolP3Pyr;
                    NbrElt0 = NbrP3Pyr0;
                  }
                  else if (Element_Type == PENTA_40) {
                    SolElt  = SolP3Pri;
                    NbrElt0 = NbrP3Pri0;
                  }
                  else if (Element_Type == HEXA_64) {
                    SolElt  = SolQ3Hex;
                    NbrElt0 = NbrQ3Hex0;
                  }
                  else if (Element_Type == TETRA_35) {
                    SolElt  = SolP4Tet;
                    NbrElt0 = NbrP4Tet0;
                  }
                  else if (Element_Type == PYRA_55) {
                    SolElt  = SolP4Pyr;
                    NbrElt0 = NbrP4Pyr0;
                  }
                  else if (Element_Type == PENTA_75) {
                    SolElt  = SolP4Pri;
                    NbrElt0 = NbrP4Pri0;
                  }
                  else if (Element_Type == HEXA_125) {
                    SolElt  = SolQ4Hex;
                    NbrElt0 = NbrQ4Hex0;
                  }
                  else
                    continue;
                }

                CG_Err = cg_nfields(File_Index, Base_Index, Zone_Index, Solution_Index, &Number_of_Fields);
                if (CG_Err != CG_OK)
                  printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
                if (Number_of_Fields > 0) {
                  //-- Check admissible fields
                  for (Field_Index = 1; Field_Index <= Number_of_Fields; ++Field_Index) {
                    CG_Err = cg_field_info(File_Index, Base_Index, Zone_Index, Solution_Index, Field_Index, &Data_Type, LabelNam);
                    if (CG_Err != CG_OK)
                      printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
                    if (Data_Type == RealDouble || Data_Type == RealSingle) {
                      CG_Err = cg_field_read(File_Index, Base_Index, Zone_Index, Solution_Index, LabelNam,
                          RealDouble, min_Index, max_Index, (void*)(&SolElt[idxElt][NbrElt0 + 1]));
                      idxElt++;
                    }
                  }
                }
              }
              else {
                //-- handle case of mixed elements
                NbrElt = End_Index - Start_Index + 1;
                SolMix = malloc(NbrElt * sizeof(double));
                CG_Err = cg_ElementDataSize(File_Index, Base_Index, Zone_Index, Section_Index, &size);

                if (CG_Err != CG_OK)
                  printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

                Connectivity = (cgsize_t*)malloc((size_t)size * sizeof(cgsize_t));

                ConnectOffset = (cgsize_t*)malloc((size_t)(NbrElt + 1) * sizeof(cgsize_t));

                CG_Err = cg_poly_elements_read(File_Index, Base_Index, Zone_Index, Section_Index, Connectivity, ConnectOffset, Parent_Data);

                if (CG_Err != CG_OK)
                  printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());

                CG_Err = cg_nfields(File_Index, Base_Index, Zone_Index, Solution_Index, &Number_of_Fields);
                if (CG_Err != CG_OK)
                  printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
                if (Number_of_Fields > 0) {
                  //-- Check admissible fields
                  NbrQua00   = NbrQua0;
                  NbrTri00   = NbrTri0;
                  NbrTet00   = NbrTet0;
                  NbrHex00   = NbrHex0;
                  NbrPyr00   = NbrPyr0;
                  NbrPri00   = NbrPri0;
                  NbrP2Tri00 = NbrP2Tri0;
                  NbrQ2Qua00 = NbrQ2Qua0;
                  NbrP2Tet00 = NbrP2Tet0;
                  NbrP2Pyr00 = NbrP2Pyr0;
                  NbrP2Pri00 = NbrP2Pri0;
                  NbrQ2Hex00 = NbrQ2Hex0;
                  NbrP3Tri00 = NbrP3Tri0;
                  NbrQ3Qua00 = NbrQ3Qua0;
                  NbrP3Tet00 = NbrP3Tet0;
                  NbrP3Pyr00 = NbrP3Pyr0;
                  NbrP3Pri00 = NbrP3Pri0;
                  NbrQ3Hex00 = NbrQ3Hex0;
                  NbrP4Tri00 = NbrP4Tri0;
                  NbrQ4Qua00 = NbrQ4Qua0;
                  NbrP4Tet00 = NbrP4Tet0;
                  NbrP4Pyr00 = NbrP4Pyr0;
                  NbrP4Pri00 = NbrP4Pri0;
                  NbrQ4Hex00 = NbrQ4Hex0;
                  for (Field_Index = 1; Field_Index <= Number_of_Fields; ++Field_Index) {
                    CG_Err = cg_field_info(File_Index, Base_Index, Zone_Index, Solution_Index, Field_Index, &Data_Type, LabelNam);
                    if (CG_Err != CG_OK)
                      printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
                    if (Data_Type == RealDouble || Data_Type == RealSingle) {
                      CG_Err = cg_field_read(File_Index, Base_Index, Zone_Index, Solution_Index, LabelNam,
                          RealDouble, min_Index, max_Index, SolMix);

                      NbrQua0   = NbrQua00;
                      NbrTri0   = NbrTri00;
                      NbrTet0   = NbrTet00;
                      NbrHex0   = NbrHex00;
                      NbrPyr0   = NbrPyr00;
                      NbrPri0   = NbrPri00;
                      NbrP2Tri0 = NbrP2Tri00;
                      NbrQ2Qua0 = NbrQ2Qua00;
                      NbrP2Tet0 = NbrP2Tet00;
                      NbrP2Pyr0 = NbrP2Pyr00;
                      NbrP2Pri0 = NbrP2Pri00;
                      NbrQ2Hex0 = NbrQ2Hex00;
                      NbrP3Tri0 = NbrP3Tri00;
                      NbrQ3Qua0 = NbrQ3Qua00;
                      NbrP3Tet0 = NbrP3Tet00;
                      NbrP3Pyr0 = NbrP3Pyr00;
                      NbrP3Pri0 = NbrP3Pri00;
                      NbrQ3Hex0 = NbrQ3Hex00;
                      NbrP4Tri0 = NbrP4Tri00;
                      NbrQ4Qua0 = NbrQ4Qua00;
                      NbrP4Tet0 = NbrP4Tet00;
                      NbrP4Pyr0 = NbrP4Pyr00;
                      NbrP4Pri0 = NbrP4Pri00;
                      NbrQ4Hex0 = NbrQ4Hex00;
                      for (i = 0, n = 0; n < NbrElt; n++) {
                        SubElement_Type = Connectivity[i++];
                        CG_Err          = cg_npe(SubElement_Type, &NbrNod);
                        switch (SubElement_Type) {
                          case TRI_3:
                            if (Cell_Dim == 2) {
                              NbrTri0++;
                              SolTri[idxElt][NbrTri0] = SolMix[n];
                            }
                            break;
                          case QUAD_4:
                            if (Cell_Dim == 2) {
                              NbrQua0++;
                              SolQua[idxElt][NbrQua0] = SolMix[n];
                            }
                            break;
                          case TETRA_4:
                            if (Cell_Dim == 3) {
                              NbrTet0++;
                              SolTet[idxElt][NbrTet0] = SolMix[n];
                            }
                            break;
                          case PYRA_5:
                            if (Cell_Dim == 3) {
                              NbrPyr0++;
                              SolPyr[idxElt][NbrPyr0] = SolMix[n];
                            }
                            break;
                          case PENTA_6:
                            if (Cell_Dim == 3) {
                              NbrPri0++;
                              SolPri[idxElt][NbrPri0] = SolMix[n];
                            }
                            break;
                          case HEXA_8:
                            if (Cell_Dim == 3) {
                              NbrHex0++;
                              SolHex[idxElt][NbrHex0] = SolMix[n];
                            }
                            break;
                          case TRI_6:
                            if (Cell_Dim == 2) {
                              NbrP2Tri0++;
                              SolP2Tri[idxElt][NbrP2Tri0] = SolMix[n];
                            }
                            break;
                          case QUAD_9:
                            if (Cell_Dim == 2) {
                              NbrQ2Qua0++;
                              SolQ2Qua[idxElt][NbrQ2Qua0] = SolMix[n];
                            }
                            break;
                          case TETRA_10:
                            if (Cell_Dim == 3) {
                              NbrP2Tet0++;
                              SolP2Tet[idxElt][NbrP2Tet0] = SolMix[n];
                            }
                            break;
                          case PYRA_14:
                            if (Cell_Dim == 3) {
                              NbrP2Pyr0++;
                              SolP2Pyr[idxElt][NbrP2Pyr0] = SolMix[n];
                            }
                            break;
                          case PENTA_18:
                            if (Cell_Dim == 3) {
                              NbrP2Pri0++;
                              SolP2Pri[idxElt][NbrP2Pri0] = SolMix[n];
                            }
                            break;
                          case HEXA_27:
                            if (Cell_Dim == 3) {
                              NbrQ2Hex0++;
                              SolQ2Hex[idxElt][NbrQ2Hex0] = SolMix[n];
                            }
                            break;

                          case TRI_10:
                            if (Cell_Dim == 2) {
                              NbrP3Tri0++;
                              SolP3Tri[idxElt][NbrP3Tri0] = SolMix[n];
                            }
                            break;
                          case QUAD_16:
                            if (Cell_Dim == 2) {
                              NbrQ3Qua0++;
                              SolQ3Qua[idxElt][NbrQ3Qua0] = SolMix[n];
                            }
                            break;
                          case TETRA_20:
                            if (Cell_Dim == 3) {
                              NbrP3Tet0++;
                              SolP3Tet[idxElt][NbrP3Tet0] = SolMix[n];
                            }
                            break;
                          case PYRA_30:
                            if (Cell_Dim == 3) {
                              NbrP3Pyr0++;
                              SolP3Pyr[idxElt][NbrP3Pyr0] = SolMix[n];
                            }
                            break;
                          case PENTA_40:
                            if (Cell_Dim == 3) {
                              NbrP3Pri0++;
                              SolP3Pri[idxElt][NbrP3Pri0] = SolMix[n];
                            }
                            break;
                          case HEXA_64:
                            if (Cell_Dim == 3) {
                              NbrQ3Hex0++;
                              SolQ3Hex[idxElt][NbrQ3Hex0] = SolMix[n];
                            }
                            break;

                          case TRI_15:
                            if (Cell_Dim == 2) {
                              NbrP4Tri0++;
                              SolP4Tri[idxElt][NbrP4Tri0] = SolMix[n];
                            }
                            break;
                          case QUAD_25:
                            if (Cell_Dim == 2) {
                              NbrQ4Qua0++;
                              SolQ4Qua[idxElt][NbrQ4Qua0] = SolMix[n];
                            }
                            break;
                          case TETRA_35:
                            if (Cell_Dim == 3) {
                              NbrP4Tet0++;
                              SolP4Tet[idxElt][NbrP4Tet0] = SolMix[n];
                            }
                            break;
                          case PYRA_55:
                            if (Cell_Dim == 3) {
                              NbrP4Pyr0++;
                              SolP4Pyr[idxElt][NbrP4Pyr0] = SolMix[n];
                            }
                            break;
                          case PENTA_75:
                            if (Cell_Dim == 3) {
                              NbrP4Pri0++;
                              SolP4Pri[idxElt][NbrP4Pri0] = SolMix[n];
                            }
                            break;
                          case HEXA_125:
                            if (Cell_Dim == 3) {
                              NbrQ4Hex0++;
                              SolQ4Hex[idxElt][NbrQ4Hex0] = SolMix[n];
                            }
                            break;
                          default:
                            break;
                        }
                        if (NbrNod <= 0) {
                          printf("  %%%% Warning in CGNS File Reader: Number of Nodes invalid for element %s\n", ElementTypeName[SubElement_Type]);
                        }
                        i += NbrNod;
                      }
                      idxElt++;
                    }
                  }
                }
              }
              if (Connectivity) {
                free(Connectivity);
                Connectivity = NULL;
              }
              if (ConnectOffset) {
                free(ConnectOffset);
                ConnectOffset = NULL;
              }
              if (SolMix) {
                free(SolMix);
                SolMix = NULL;
              }
              idxElt++;
            }
          }
          else {
            for (i = 0; i < iObj->Dim; i++) {
              min_Index[i] = 1;
              max_Index[i] = Zone_Size[1][i];
            }
            if (Cell_Dim == 2) {
              SolElt  = SolQua;
              NbrElt0 = NbrQua0;
            }
            else if (Cell_Dim == 3) {
              SolElt  = SolHex;
              NbrElt0 = NbrHex0;
            }
            CG_Err = cg_nfields(File_Index, Base_Index, Zone_Index, Solution_Index, &Number_of_Fields);
            if (CG_Err != CG_OK)
              printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
            if (Number_of_Fields > 0) {
              //-- Check admissible fields
              for (Field_Index = 1; Field_Index <= Number_of_Fields; ++Field_Index) {
                CG_Err = cg_field_info(File_Index, Base_Index, Zone_Index, Solution_Index, Field_Index, &Data_Type, LabelNam);
                if (CG_Err != CG_OK)
                  printf("  %%%% Warning in CGNS File Reader: %s\n", cg_get_error());
                if (Data_Type == RealDouble || Data_Type == RealSingle) {
                  CG_Err = cg_field_read(File_Index, Base_Index, Zone_Index, Solution_Index, LabelNam,
                      RealDouble, min_Index, max_Index, (void*)(&SolElt[idxElt][NbrElt0 + 1]));
                  idxElt++;
                }
              }
            }
          }
        }
      }
    }
    SolElt = NULL;
  }
  if (SolVer) {
    free(SolVer);
    SolVer = NULL;
  }
  if (SolTri) {
    free(SolTri);
    SolTri = NULL;
  }
  if (SolQua) {
    free(SolQua);
    SolQua = NULL;
  }
  if (SolTet) {
    free(SolTet);
    SolTet = NULL;
  }
  if (SolPri) {
    free(SolPri);
    SolPri = NULL;
  }
  if (SolPyr) {
    free(SolPyr);
    SolPyr = NULL;
  }
  if (SolHex) {
    free(SolHex);
    SolHex = NULL;
  }
  if (SolP2Tri) {
    free(SolP2Tri);
    SolP2Tri = NULL;
  }
  if (SolQ2Qua) {
    free(SolQ2Qua);
    SolQ2Qua = NULL;
  }
  if (SolP2Tet) {
    free(SolP2Tet);
    SolP2Tet = NULL;
  }
  if (SolP2Pyr) {
    free(SolP2Pyr);
    SolP2Pyr = NULL;
  }
  if (SolP2Pri) {
    free(SolP2Pri);
    SolP2Pri = NULL;
  }
  if (SolQ2Hex) {
    free(SolQ2Hex);
    SolQ2Hex = NULL;
  }
  if (SolP3Tri) {
    free(SolP3Tri);
    SolP3Tri = NULL;
  }
  if (SolQ3Qua) {
    free(SolQ3Qua);
    SolQ3Qua = NULL;
  }
  if (SolP3Tet) {
    free(SolP3Tet);
    SolP3Tet = NULL;
  }
  if (SolP3Pyr) {
    free(SolP3Pyr);
    SolP3Pyr = NULL;
  }
  if (SolP3Pri) {
    free(SolP3Pri);
    SolP3Pri = NULL;
  }
  if (SolQ3Hex) {
    free(SolQ3Hex);
    SolQ3Hex = NULL;
  }
  if (SolP4Tri) {
    free(SolP4Tri);
    SolP4Tri = NULL;
  }
  if (SolQ4Qua) {
    free(SolQ4Qua);
    SolQ4Qua = NULL;
  }
  if (SolP4Tet) {
    free(SolP4Tet);
    SolP4Tet = NULL;
  }
  if (SolP4Pyr) {
    free(SolP4Pyr);
    SolP4Pyr = NULL;
  }
  if (SolP4Pri) {
    free(SolP4Pri);
    SolP4Pri = NULL;
  }
  if (SolQ4Hex) {
    free(SolQ4Hex);
    SolQ4Hex = NULL;
  }
  cg_close(File_Index);
  return VIZINT_SUCCESS;
}

int viz_WriteSolutionsStrings(int64_t OutSol, VizObject* iObj)
{
  int iFld, NbrFldNam;
  char(*FldNam)[2048];
  int* FldKwd;

  NbrFldNam = viz_CountNumberOfFieldNames(iObj);

  FldKwd = malloc(sizeof(int) * NbrFldNam);
  FldNam = malloc(sizeof(char) * 2018 * NbrFldNam);
  viz_GetSolutionFieldNamesAndKwd(iObj, FldNam, FldKwd);

  GmfSetKwd(OutSol, GmfReferenceStrings, NbrFldNam);
  for (iFld = 0; iFld < NbrFldNam; iFld++)
    GmfSetLin(OutSol, GmfReferenceStrings, FldKwd[iFld], iFld + 1, FldNam[iFld]);

  free(FldKwd);
  free(FldNam);

  return VIZINT_SUCCESS;
}

void viz_GetSolutionFieldNamesAndKwd(VizObject* Obj, char (*Nam)[2048], int* Kwd)
{
  solution* sol;

  int Fmax, cpt;

  Fmax = cpt = 0;

  if (!Nam || !Kwd)
    return;

  if (Obj->CrdSol) {
    sol  = Obj->CrdSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfSolAtVertices;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->EdgSol) {
    sol  = Obj->EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtEdgesP1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P2EdgSol) {
    sol  = Obj->P2EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtEdgesP2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P3EdgSol) {
    sol  = Obj->P3EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtEdgesP3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P4EdgSol) {
    sol  = Obj->P4EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtEdgesP4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->TriSol) {
    sol  = Obj->TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTrianglesP1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P2TriSol) {
    sol  = Obj->P2TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTrianglesP2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P3TriSol) {
    sol  = Obj->P3TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTrianglesP3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P4TriSol) {
    sol  = Obj->P4TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTrianglesP4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->QuaSol) {
    sol  = Obj->QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtQuadrilateralsQ1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q2QuaSol) {
    sol  = Obj->Q2QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtQuadrilateralsQ2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q3QuaSol) {
    sol  = Obj->Q3QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtQuadrilateralsQ3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q4QuaSol) {
    sol  = Obj->Q4QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtQuadrilateralsQ4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->TetSol) {
    sol  = Obj->TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTetrahedraP1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P2TetSol) {
    sol  = Obj->P2TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTetrahedraP2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P3TetSol) {
    sol  = Obj->P3TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTetrahedraP3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P4TetSol) {
    sol  = Obj->P4TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTetrahedraP4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->PyrSol) {
    sol  = Obj->PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPyramidsP1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P2PyrSol) {
    sol  = Obj->P2PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPyramidsP2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P3PyrSol) {
    sol  = Obj->P3PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPyramidsP3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P4PyrSol) {
    sol  = Obj->P4PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPyramidsP4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->PriSol) {
    sol  = Obj->PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPrismsP1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P2PriSol) {
    sol  = Obj->P2PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPrismsP2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P3PriSol) {
    sol  = Obj->P3PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPrismsP3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P4PriSol) {
    sol  = Obj->P4PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPrismsP4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->HexSol) {
    sol  = Obj->HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtHexahedraQ1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q2HexSol) {
    sol  = Obj->Q2HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtHexahedraQ2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q3HexSol) {
    sol  = Obj->Q3HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtHexahedraQ3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q4HexSol) {
    sol  = Obj->Q4HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtHexahedraQ4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  return;
}

int viz_CountNumberOfFieldNames(VizObject* Obj)
{
  solution* sol;

  int cpt = 0;

  if (Obj->CrdSol) {
    sol = Obj->CrdSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->EdgSol) {
    sol = Obj->EdgSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P2EdgSol) {
    sol = Obj->P2EdgSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P3EdgSol) {
    sol = Obj->P3EdgSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P4EdgSol) {
    sol = Obj->P4EdgSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->TriSol) {
    sol = Obj->TriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P2TriSol) {
    sol = Obj->P2TriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P3TriSol) {
    sol = Obj->P3TriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P4TriSol) {
    sol = Obj->P4TriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->QuaSol) {
    sol = Obj->QuaSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q2QuaSol) {
    sol = Obj->Q2QuaSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q3QuaSol) {
    sol = Obj->Q3QuaSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q4QuaSol) {
    sol = Obj->Q4QuaSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->TetSol) {
    sol = Obj->TetSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P2TetSol) {
    sol = Obj->P2TetSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P3TetSol) {
    sol = Obj->P3TetSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P4TetSol) {
    sol = Obj->P4TetSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->PyrSol) {
    sol = Obj->PyrSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P2PyrSol) {
    sol = Obj->P2PyrSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P3PyrSol) {
    sol = Obj->P3PyrSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P4PyrSol) {
    sol = Obj->P4PyrSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->PriSol) {
    sol = Obj->PriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P2PriSol) {
    sol = Obj->P2PriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P3PriSol) {
    sol = Obj->P3PriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P4PriSol) {
    sol = Obj->P4PriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->HexSol) {
    sol = Obj->HexSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q2HexSol) {
    sol = Obj->Q2HexSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q3HexSol) {
    sol = Obj->Q3HexSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q4HexSol) {
    sol = Obj->Q4HexSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  return cpt;
}

int viz_WriteSolution(VizObject* Obj, const char* SolFile, int SolDeg, VizIntStatus* status)
{
  return viz_WriteSolutionLibMeshb(Obj, SolFile, SolDeg, status);
}

static int viz_TestWriteHOSol(int SolAtEnt, int SolDeg)
{
  if (((SolDeg == 0 && (SolAtEnt == GmfSolAtVertices || SolAtEnt == GmfSolAtEdges || SolAtEnt == GmfSolAtTriangles || SolAtEnt == GmfSolAtQuadrilaterals || SolAtEnt == GmfSolAtTetrahedra || SolAtEnt == GmfSolAtPyramids || SolAtEnt == GmfSolAtPrisms || SolAtEnt == GmfSolAtHexahedra))))
    return 0;
  else
    return 1;
}

static int viz_TestCompatibilitySolDegKwd(int SolDeg, int SolAtEnt)
{
  if (SolDeg == 0 && (SolAtEnt == GmfSolAtVertices || SolAtEnt == GmfSolAtEdges || SolAtEnt == GmfSolAtTriangles || SolAtEnt == GmfSolAtQuadrilaterals || SolAtEnt == GmfSolAtTetrahedra || SolAtEnt == GmfSolAtPyramids || SolAtEnt == GmfSolAtPrisms || SolAtEnt == GmfSolAtHexahedra))
    return 1;
  else if (SolDeg != 0 && (SolAtEnt == GmfHOSolAtEdgesP1 || SolAtEnt == GmfHOSolAtTrianglesP1 || SolAtEnt == GmfHOSolAtQuadrilateralsQ1 || SolAtEnt == GmfHOSolAtTetrahedraP1 || SolAtEnt == GmfHOSolAtPyramidsP1 || SolAtEnt == GmfHOSolAtPrismsP1 || SolAtEnt == GmfHOSolAtHexahedraQ1))
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP2 || SolAtEnt == GmfHOSolAtTrianglesP2 || SolAtEnt == GmfHOSolAtQuadrilateralsQ2 || SolAtEnt == GmfHOSolAtTetrahedraP2 || SolAtEnt == GmfHOSolAtPyramidsP2 || SolAtEnt == GmfHOSolAtPrismsP2 || SolAtEnt == GmfHOSolAtHexahedraQ2)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP3 || SolAtEnt == GmfHOSolAtTrianglesP3 || SolAtEnt == GmfHOSolAtQuadrilateralsQ3 || SolAtEnt == GmfHOSolAtTetrahedraP3 || SolAtEnt == GmfHOSolAtPyramidsP3 || SolAtEnt == GmfHOSolAtPrismsP3 || SolAtEnt == GmfHOSolAtHexahedraQ3)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP4 || SolAtEnt == GmfHOSolAtTrianglesP4 || SolAtEnt == GmfHOSolAtQuadrilateralsQ4 || SolAtEnt == GmfHOSolAtTetrahedraP4 || SolAtEnt == GmfHOSolAtPyramidsP4 || SolAtEnt == GmfHOSolAtPrismsP4 || SolAtEnt == GmfHOSolAtHexahedraQ4)
    return 1;

  return 0;
}

static int viz_WriteSolAtEntLibMeshb(int64_t OutSol, int Dim, solution* sol, int SolAtEnt, int SolDeg, double* Tim, int* Ite, VizIntStatus* status)
{
  if (sol == NULL)
    return VIZINT_SUCCESS;

  if (OutSol == 0) {
    viz_writestatus(status, "Wrong OutSol     parameter on input\n");
    return VIZINT_ERROR;
  }
  if (SolDeg < 0) {
    viz_writestatus(status, "Wrong SolDeg     parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!Tim) {
    viz_writestatus(status, "NULL (Tim)       parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!Ite) {
    viz_writestatus(status, "NULL (Ite)       parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  if (viz_TestCompatibilitySolDegKwd(SolDeg, SolAtEnt) == 0) {
    viz_writestatus(status, "Mismatch of a solution of degree %d with solution keyword\n", SolDeg);
    return VIZINT_ERROR;
  }

  int     idx, iSolu, NbrSol, NbrNod, NbrEnt, SolSiz;
  int *   SolTyp, *TypSiz;
  double* dblBuf;
  float*  fltBuf;

  dblBuf = NULL;
  fltBuf = NULL;

  solution *cur, **Sol;

  //-- create the array of solutions after count
  cur    = sol;
  NbrSol = 0;
  NbrNod = -1;
  NbrEnt = -1;
  while (cur) {
    if (SolDeg == cur->Deg) {
      NbrSol++;
      if (NbrNod != -1 && NbrNod != sol->NbrNod) {
        viz_writestatus(status, "ERROR : Different number of nodes for solutions of same degree at the same element\n");
        return VIZINT_ERROR;
      }
      else
        NbrNod = sol->NbrNod;
      if (NbrEnt != -1 && NbrEnt != sol->NbrSol) {
        viz_writestatus(status, "ERROR : Different number of entities for solutions of same degree at the same element\n");
        return VIZINT_ERROR;
      }
      else
        NbrEnt = sol->NbrSol;
    }
    cur = cur->nxt;
  }
  if (NbrSol == 0)
    return VIZINT_SUCCESS;

  //-- alloc arrays and fill it;
  Sol    = malloc(NbrSol * sizeof(solution*));
  SolTyp = malloc(NbrSol * sizeof(int));
  TypSiz = malloc(NbrSol * sizeof(int));
  cur    = sol;
  iSolu  = 0;
  while (cur) {
    if (SolDeg == cur->Deg) {
      Sol[iSolu]    = cur;
      SolTyp[iSolu] = cur->SolTyp;
      (*Tim)        = max(*Tim, cur->Tim);
      (*Ite)        = max(*Ite, cur->Ite);
      iSolu++;
    }
    cur = cur->nxt;
  }

  //-- get solution size
  SolSiz = 0;
  for (int iSol = 0; iSol < NbrSol; iSol++) {
    switch (SolTyp[iSol]) {
      case GmfSca:
        TypSiz[iSol] = 1;
        break;
      case GmfVec:
        TypSiz[iSol] = (Dim == 2) ? 2 : 3;
        break;
      case GmfSymMat:
        TypSiz[iSol] = (Dim == 2) ? 3 : 6;
        break;
      case GmfMat:
        TypSiz[iSol] = (Dim == 2) ? 4 : 9;
        break;
    }
    SolSiz += TypSiz[iSol];
  }
  SolSiz *= NbrNod;

  //-- alloc buffer arrays
  dblBuf = malloc(SolSiz * sizeof(double));

  //-- set keywords
  if (viz_TestWriteHOSol(SolAtEnt, SolDeg))
    GmfSetKwd(OutSol, SolAtEnt, NbrEnt, NbrSol, SolTyp, SolDeg, NbrNod);
  else
    GmfSetKwd(OutSol, SolAtEnt, NbrEnt, NbrSol, SolTyp);

  //-- fill in solution arrays
  for (int iEnt = 1; iEnt <= NbrEnt; iEnt++) {
    idx = 0;

    for (int i = 0; i < NbrNod; i++) {
      for (int iSol = 0; iSol < NbrSol; iSol++) {
        for (int j = 0; j < TypSiz[iSol]; j++) {
          dblBuf[idx++] = Sol[iSol]->Sol[iEnt * TypSiz[iSol] * NbrNod + i * TypSiz[iSol] + j];
        }
      }
    }
    GmfSetLin(OutSol, SolAtEnt, dblBuf);
  }

  //-- free working arrays
  if (fltBuf) {
    free(fltBuf);
    fltBuf = NULL;
  }
  if (dblBuf) {
    free(dblBuf);
    dblBuf = NULL;
  }
  if (SolTyp) {
    free(SolTyp);
    SolTyp = NULL;
  }
  if (TypSiz) {
    free(TypSiz);
    TypSiz = NULL;
  }
  if (Sol) {
    free(Sol);
    Sol = NULL;
  }

  return VIZINT_SUCCESS;
}

int viz_GetSolutionDegMax(VizObject* Obj)
{
  solution* sol;

  int DegMax = 0;

  if (Obj->EdgSol) {
    sol = Obj->EdgSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P2EdgSol) {
    sol = Obj->P2EdgSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P3EdgSol) {
    sol = Obj->P3EdgSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P4EdgSol) {
    sol = Obj->P4EdgSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->TriSol) {
    sol = Obj->TriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P2TriSol) {
    sol = Obj->P2TriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P3TriSol) {
    sol = Obj->P3TriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P4TriSol) {
    sol = Obj->P4TriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->QuaSol) {
    sol = Obj->QuaSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q2QuaSol) {
    sol = Obj->Q2QuaSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q3QuaSol) {
    sol = Obj->Q3QuaSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q4QuaSol) {
    sol = Obj->Q4QuaSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->TetSol) {
    sol = Obj->TetSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P2TetSol) {
    sol = Obj->P2TetSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P3TetSol) {
    sol = Obj->P3TetSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P4TetSol) {
    sol = Obj->P4TetSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->PyrSol) {
    sol = Obj->PyrSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P2PyrSol) {
    sol = Obj->P2PyrSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P3PyrSol) {
    sol = Obj->P3PyrSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P4PyrSol) {
    sol = Obj->P4PyrSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->PriSol) {
    sol = Obj->PriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P2PriSol) {
    sol = Obj->P2PriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P3PriSol) {
    sol = Obj->P3PriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P4PriSol) {
    sol = Obj->P4PriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->HexSol) {
    sol = Obj->HexSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q2HexSol) {
    sol = Obj->Q2HexSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q3HexSol) {
    sol = Obj->Q3HexSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q4HexSol) {
    sol = Obj->Q4HexSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  return DegMax;
}

/*
  Soldeg == -2  => display sol at vertices
  SolDeg == -1  => display all fields
  SolDeg >=  0  => display solution at elts at the given degree
*/

int viz_WriteSolutionLibMeshb(VizObject* Obj, const char* SolFile, int SolDeg, VizIntStatus* status)
{
  if (SolDeg < -2) {
    viz_writestatus(status, "Wrong SolDeg     parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!SolFile) {
    viz_writestatus(status, "NULL (SolFile)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  int    Ite;
  double Tim;

  Ite = 0;
  Tim = 0.;

  int FilVer = (sizeof(int1) == 8) ? 4 : 3;

  int deg, degMax, ierr;

  int64_t OutSol = GmfOpenMesh(SolFile, GmfWrite, FilVer, Obj->Dim);

  if (OutSol == 0) {
    viz_writestatus(status, "Cannot open mesh file %s\n", SolFile);
    return VIZINT_ERROR;
  }
  else
    viz_writeinfo(status, "%s", SolFile);

  //-- write for each entity

  if (SolDeg == -2) //-- write solatvertices
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->CrdSol, GmfSolAtVertices, 0, &Tim, &Ite, status);
  if (SolDeg == -1) { //-- write everything
    // if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->CrdSol, GmfSolAtVertices, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
    //   return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->EdgSol, GmfSolAtEdges, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TriSol, GmfSolAtTriangles, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->QuaSol, GmfSolAtQuadrilaterals, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TetSol, GmfSolAtTetrahedra, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PyrSol, GmfSolAtPyramids, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PriSol, GmfSolAtPrisms, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->HexSol, GmfSolAtHexahedra, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->CrdSol, GmfSolAtVertices, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;

    //-- get higher existing degree
    degMax = viz_GetSolutionDegMax(Obj);

    if (degMax >= 0) {
      for (deg = 0; deg <= degMax; deg++) {
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->EdgSol, GmfHOSolAtEdgesP1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2EdgSol, GmfHOSolAtEdgesP2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3EdgSol, GmfHOSolAtEdgesP3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4EdgSol, GmfHOSolAtEdgesP4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TriSol, GmfHOSolAtTrianglesP1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2TriSol, GmfHOSolAtTrianglesP2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3TriSol, GmfHOSolAtTrianglesP3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4TriSol, GmfHOSolAtTrianglesP4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->QuaSol, GmfHOSolAtQuadrilateralsQ1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q2QuaSol, GmfHOSolAtQuadrilateralsQ2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q3QuaSol, GmfHOSolAtQuadrilateralsQ3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q4QuaSol, GmfHOSolAtQuadrilateralsQ4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TetSol, GmfHOSolAtTetrahedraP1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2TetSol, GmfHOSolAtTetrahedraP2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3TetSol, GmfHOSolAtTetrahedraP3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4TetSol, GmfHOSolAtTetrahedraP4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PyrSol, GmfHOSolAtPyramidsP1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2PyrSol, GmfHOSolAtPyramidsP2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3PyrSol, GmfHOSolAtPyramidsP3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4PyrSol, GmfHOSolAtPyramidsP4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PriSol, GmfHOSolAtPrismsP1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2PriSol, GmfHOSolAtPrismsP2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3PriSol, GmfHOSolAtPrismsP3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4PriSol, GmfHOSolAtPrismsP4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->HexSol, GmfHOSolAtHexahedraQ1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q2HexSol, GmfHOSolAtHexahedraQ2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q3HexSol, GmfHOSolAtHexahedraQ3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q4HexSol, GmfHOSolAtHexahedraQ4, deg, &Tim, &Ite, status);
      }
    }
  }
  else {
    if (SolDeg == 0) { //-- write degree 0 aka solatelt and solatvertices (degree 0 by convention)
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->EdgSol, GmfSolAtEdges, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TriSol, GmfSolAtTriangles, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->QuaSol, GmfSolAtQuadrilaterals, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TetSol, GmfSolAtTetrahedra, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PyrSol, GmfSolAtPyramids, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PriSol, GmfSolAtPrisms, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->HexSol, GmfSolAtHexahedra, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->CrdSol, GmfSolAtVertices, SolDeg, &Tim, &Ite, status);
    }

    //--- write degree > 1
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->EdgSol, GmfHOSolAtEdgesP1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2EdgSol, GmfHOSolAtEdgesP2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3EdgSol, GmfHOSolAtEdgesP3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4EdgSol, GmfHOSolAtEdgesP4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TriSol, GmfHOSolAtTrianglesP1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2TriSol, GmfHOSolAtTrianglesP2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3TriSol, GmfHOSolAtTrianglesP3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4TriSol, GmfHOSolAtTrianglesP4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->QuaSol, GmfHOSolAtQuadrilateralsQ1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q2QuaSol, GmfHOSolAtQuadrilateralsQ2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q3QuaSol, GmfHOSolAtQuadrilateralsQ3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q4QuaSol, GmfHOSolAtQuadrilateralsQ4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TetSol, GmfHOSolAtTetrahedraP1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2TetSol, GmfHOSolAtTetrahedraP2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3TetSol, GmfHOSolAtTetrahedraP3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4TetSol, GmfHOSolAtTetrahedraP4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PyrSol, GmfHOSolAtPyramidsP1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2PyrSol, GmfHOSolAtPyramidsP2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3PyrSol, GmfHOSolAtPyramidsP3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4PyrSol, GmfHOSolAtPyramidsP4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PriSol, GmfHOSolAtPrismsP1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2PriSol, GmfHOSolAtPrismsP2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3PriSol, GmfHOSolAtPrismsP3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4PriSol, GmfHOSolAtPrismsP4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->HexSol, GmfHOSolAtHexahedraQ1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q2HexSol, GmfHOSolAtHexahedraQ2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q3HexSol, GmfHOSolAtHexahedraQ3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q4HexSol, GmfHOSolAtHexahedraQ4, SolDeg, &Tim, &Ite, status);
  }

  // Write iteration and time if there is some
  if (Ite != 0) {
    GmfSetKwd(OutSol, GmfIterations, 1);
    GmfSetLin(OutSol, GmfIterations, Ite);
  }

  if (Tim != 0.) {
    GmfSetKwd(OutSol, GmfTime, 1);
    GmfSetLin(OutSol, GmfTime, Tim);
  }

  viz_WriteSolutionsStrings(OutSol, Obj);

  // Close current mesh
  GmfCloseMesh(OutSol);

  return VIZINT_SUCCESS;
}
